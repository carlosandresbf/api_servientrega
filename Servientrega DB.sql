/*
SQLyog Ultimate v12.2.4 (64 bit)
MySQL - 5.6.34-log : Database - servientrega_db
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`servientrega_db` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `servientrega_db`;

/*Table structure for table `cities` */

DROP TABLE IF EXISTS `cities`;

CREATE TABLE `cities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `states_id` int(10) unsigned DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cities_states_id_foreign` (`states_id`),
  CONSTRAINT `cities_states_id_foreign` FOREIGN KEY (`states_id`) REFERENCES `states` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `cities` */

insert  into `cities`(`id`,`name`,`active`,`states_id`,`deleted_at`,`created_at`,`updated_at`) values 
(1,'APARTADO',1,1,NULL,NULL,NULL),
(2,'ENVIGADO',1,1,NULL,NULL,NULL),
(3,'PUERTO BERRIO',1,1,NULL,NULL,NULL),
(4,'BELLO',1,1,NULL,NULL,NULL),
(5,'CAUCASIA',1,1,NULL,NULL,NULL),
(6,'CHIGORODO',1,1,NULL,NULL,NULL),
(7,'SABANETA',1,1,NULL,NULL,NULL),
(8,'LA CEJA',1,1,NULL,NULL,NULL),
(9,'MEDELLIN',1,1,NULL,NULL,NULL),
(10,'ITAGUI',1,1,NULL,NULL,NULL),
(11,'RIONEGRO (ANT)',1,1,NULL,NULL,NULL),
(12,'TURBO',1,1,NULL,NULL,NULL),
(13,'MONTERIA',1,10,NULL,NULL,NULL),
(14,'MONTELIBANO',1,10,NULL,NULL,NULL),
(15,'FUSAGASUGA',1,11,NULL,NULL,NULL),
(16,'CHIA',1,11,NULL,NULL,NULL),
(17,'FACATATIVA',1,11,NULL,NULL,NULL),
(18,'SOACHA',1,11,NULL,NULL,NULL),
(19,'BOGOTA',1,11,NULL,NULL,NULL),
(20,'TENJO',1,11,NULL,NULL,NULL),
(21,'FUNZA',1,11,NULL,NULL,NULL),
(22,'MADRID',1,11,NULL,NULL,NULL),
(23,'GIRARDOT',1,11,NULL,NULL,NULL),
(24,'VILLETA',1,11,NULL,NULL,NULL),
(25,'NEIVA',1,12,NULL,NULL,NULL),
(26,'PITALITO',1,12,NULL,NULL,NULL),
(27,'MAICAO',1,13,NULL,NULL,NULL),
(28,'RIOHACHA',1,13,NULL,NULL,NULL),
(29,'SANTA MARTA',1,14,NULL,NULL,NULL),
(30,'VILLAVICENCIO',1,15,NULL,NULL,NULL),
(31,'IPIALES',1,16,NULL,NULL,NULL),
(32,'PASTO',1,16,NULL,NULL,NULL),
(33,'CUCUTA',1,17,NULL,NULL,NULL),
(34,'MOCOA',1,18,NULL,NULL,NULL),
(35,'ARMENIA (Q)',1,19,NULL,NULL,NULL),
(36,'SAN ANDRES',1,2,NULL,NULL,NULL),
(37,'DOSQUEBRADAS',1,20,NULL,NULL,NULL),
(38,'PEREIRA',1,20,NULL,NULL,NULL),
(39,'BARRANCABERMEJA',1,21,NULL,NULL,NULL),
(40,'BUCARAMANGA',1,21,NULL,NULL,NULL),
(41,'FLORIDABLANCA',1,21,NULL,NULL,NULL),
(42,'SINCELEJO',1,22,NULL,NULL,NULL),
(43,'MELGAR',1,23,NULL,NULL,NULL),
(44,'ESPINAL',1,23,NULL,NULL,NULL),
(45,'IBAGUE',1,23,NULL,NULL,NULL),
(46,'TULUA',1,24,NULL,NULL,NULL),
(47,'PALMIRA',1,24,NULL,NULL,NULL),
(48,'CALI',1,24,NULL,NULL,NULL),
(49,'BUENAVENTURA',1,24,NULL,NULL,NULL),
(50,'CARTAGO',1,24,NULL,NULL,NULL),
(51,'BUGA',1,24,NULL,NULL,NULL),
(52,'SOLEDAD',1,3,NULL,NULL,NULL),
(53,'BARRANQUILLA',1,3,NULL,NULL,NULL),
(54,'CARTAGENA',1,4,NULL,NULL,NULL),
(55,'DUITAMA',1,5,NULL,NULL,NULL),
(56,'SOGAMOSO',1,5,NULL,NULL,NULL),
(57,'JENESANO',1,5,NULL,NULL,NULL),
(58,'PAIPA',1,5,NULL,NULL,NULL),
(59,'CHIQUINQUIRA',1,5,NULL,NULL,NULL),
(60,'PUERTO BOYACA',1,5,NULL,NULL,NULL),
(61,'TUNJA',1,5,NULL,NULL,NULL),
(62,'MANIZALES',1,6,NULL,NULL,NULL),
(63,'LA DORADA',1,6,NULL,NULL,NULL),
(64,'YOPAL',1,7,NULL,NULL,NULL),
(65,'POPAYAN',1,8,NULL,NULL,NULL),
(66,'MINA DRUMOND PRIBBENOW',1,9,NULL,NULL,NULL),
(67,'VALLEDUPAR',1,9,NULL,NULL,NULL),
(68,'CHINCHINA',1,6,NULL,NULL,NULL),
(69,'SANTA ROSA',1,6,NULL,NULL,NULL),
(70,'VITERBO CDAS',1,6,NULL,NULL,NULL),
(71,'SANTUARIO',1,6,NULL,NULL,NULL),
(72,'ANSERMA',1,6,NULL,NULL,NULL),
(73,'MANZANARES',1,6,NULL,NULL,NULL),
(74,'FILADELFIA',1,6,NULL,NULL,NULL),
(75,'SALAMINA',1,6,NULL,NULL,NULL),
(76,'RIOSUCIO',1,6,NULL,NULL,NULL),
(77,'FRESNO',1,6,NULL,NULL,NULL),
(78,'CORINTO',1,8,NULL,NULL,NULL),
(79,'PUERTO TEJADA',1,8,NULL,NULL,NULL),
(80,'EL BORDO',1,8,NULL,NULL,NULL),
(81,'SANTANDER DE QUILICHAO',1,8,NULL,NULL,NULL),
(82,'VILLA GORGONA',1,24,NULL,NULL,NULL),
(83,'EL CERRITO',1,24,NULL,NULL,NULL),
(84,'JAMUNDI',1,24,NULL,NULL,NULL),
(85,'YUMBO',1,24,NULL,NULL,NULL),
(86,'YUMBO',1,24,NULL,NULL,NULL),
(87,'TRUJILLO',1,24,NULL,NULL,NULL),
(88,'ANDALUCIA',1,24,NULL,NULL,NULL),
(89,'BUGALAGRANDE',1,24,NULL,NULL,NULL),
(90,'CANDELARIA',1,24,NULL,NULL,NULL),
(91,'DARIEN',1,24,NULL,NULL,NULL),
(92,'FLORIDA',1,24,NULL,NULL,NULL),
(93,'GINEBRA',1,24,NULL,NULL,NULL),
(94,'GUACARI',1,24,NULL,NULL,NULL),
(95,'PRADERA',1,24,NULL,NULL,NULL),
(96,'RESTREPO',1,24,NULL,NULL,NULL),
(97,'DAGUA',1,24,NULL,NULL,NULL),
(98,'FLORENCIA',1,25,NULL,NULL,NULL);

/*Table structure for table `customers` */

DROP TABLE IF EXISTS `customers`;

CREATE TABLE `customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cities_id` int(10) unsigned DEFAULT NULL,
  `adress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `users_id` int(10) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cds_id` int(11) unsigned DEFAULT NULL,
  `lat` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lon` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `customers_mobile_unique` (`mobile`),
  UNIQUE KEY `customers_document_unique` (`document`),
  KEY `customers_cities_id_foreign` (`cities_id`),
  KEY `customers_users_id_foreign` (`users_id`),
  CONSTRAINT `customers_cities_id_foreign` FOREIGN KEY (`cities_id`) REFERENCES `municipios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `customers_users_id_foreign` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `customers` */

insert  into `customers`(`id`,`name`,`lastName`,`document`,`mobile`,`cities_id`,`adress`,`users_id`,`deleted_at`,`created_at`,`updated_at`,`cds_id`,`lat`,`lon`) values 
(1,'Carlos','Andres','111048978','+573014685403',4,'Mz 1 Cs 2',6,NULL,'2018-11-01 09:24:39','2018-11-01 09:24:39',NULL,NULL,NULL);

/*Table structure for table `departamentos` */

DROP TABLE IF EXISTS `departamentos`;

CREATE TABLE `departamentos` (
  `id` int(2) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8mb4;

/*Data for the table `departamentos` */

insert  into `departamentos`(`id`,`name`,`created_at`,`updated_at`,`deleted_at`) values 
(5,'ANTIOQUIA',NULL,NULL,NULL),
(8,'ATLÁNTICO',NULL,NULL,NULL),
(11,'BOGOTÁ, D.C.',NULL,NULL,NULL),
(13,'BOLÍVAR',NULL,NULL,NULL),
(15,'BOYACÁ',NULL,NULL,NULL),
(17,'CALDAS',NULL,NULL,NULL),
(18,'CAQUETÁ',NULL,NULL,NULL),
(19,'CAUCA',NULL,NULL,NULL),
(20,'CESAR',NULL,NULL,NULL),
(23,'CÓRDOBA',NULL,NULL,NULL),
(25,'CUNDINAMARCA',NULL,NULL,NULL),
(27,'CHOCÓ',NULL,NULL,NULL),
(41,'HUILA',NULL,NULL,NULL),
(44,'LA GUAJIRA',NULL,NULL,NULL),
(47,'MAGDALENA',NULL,NULL,NULL),
(50,'META',NULL,NULL,NULL),
(52,'NARIÑO',NULL,NULL,NULL),
(54,'NORTE DE SANTANDER',NULL,NULL,NULL),
(63,'QUINDIO',NULL,NULL,NULL),
(66,'RISARALDA',NULL,NULL,NULL),
(68,'SANTANDER',NULL,NULL,NULL),
(70,'SUCRE',NULL,NULL,NULL),
(73,'TOLIMA',NULL,NULL,NULL),
(76,'VALLE DEL CAUCA',NULL,NULL,NULL),
(81,'ARAUCA',NULL,NULL,NULL),
(85,'CASANARE',NULL,NULL,NULL),
(86,'PUTUMAYO',NULL,NULL,NULL),
(88,'ARCHIPIÉLAGO DE SAN ANDRÉS, PROVIDENCIA Y SANTA CATALINA',NULL,NULL,NULL),
(91,'AMAZONAS',NULL,NULL,NULL),
(94,'GUAINÍA',NULL,NULL,NULL),
(95,'GUAVIARE',NULL,NULL,NULL),
(97,'VAUPÉS',NULL,NULL,NULL),
(99,'VICHADA',NULL,NULL,NULL);

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

/*Table structure for table `municipios` */

DROP TABLE IF EXISTS `municipios`;

CREATE TABLE `municipios` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `state` int(1) unsigned NOT NULL,
  `departamento_id` int(2) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1101 DEFAULT CHARSET=utf8mb4;

/*Data for the table `municipios` */

insert  into `municipios`(`id`,`name`,`state`,`departamento_id`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'Abriaquí',1,5,NULL,NULL,NULL),
(2,'Acacías',1,50,NULL,NULL,NULL),
(3,'Acandí',1,27,NULL,NULL,NULL),
(4,'Acevedo',1,41,NULL,NULL,NULL),
(5,'Achí',1,13,NULL,NULL,NULL),
(6,'Agrado',1,41,NULL,NULL,NULL),
(7,'Agua de Dios',1,25,NULL,NULL,NULL),
(8,'Aguachica',1,20,NULL,NULL,NULL),
(9,'Aguada',1,68,NULL,NULL,NULL),
(10,'Aguadas',1,17,NULL,NULL,NULL),
(11,'Aguazul',1,85,NULL,NULL,NULL),
(12,'Agustín Codazzi',1,20,NULL,NULL,NULL),
(13,'Aipe',1,41,NULL,NULL,NULL),
(14,'Albania',1,18,NULL,NULL,NULL),
(15,'Albania',1,44,NULL,NULL,NULL),
(16,'Albania',1,68,NULL,NULL,NULL),
(17,'Albán',1,25,NULL,NULL,NULL),
(18,'Albán (San José)',1,52,NULL,NULL,NULL),
(19,'Alcalá',1,76,NULL,NULL,NULL),
(20,'Alejandria',1,5,NULL,NULL,NULL),
(21,'Algarrobo',1,47,NULL,NULL,NULL),
(22,'Algeciras',1,41,NULL,NULL,NULL),
(23,'Almaguer',1,19,NULL,NULL,NULL),
(24,'Almeida',1,15,NULL,NULL,NULL),
(25,'Alpujarra',1,73,NULL,NULL,NULL),
(26,'Altamira',1,41,NULL,NULL,NULL),
(27,'Alto Baudó (Pie de Pato)',1,27,NULL,NULL,NULL),
(28,'Altos del Rosario',1,13,NULL,NULL,NULL),
(29,'Alvarado',1,73,NULL,NULL,NULL),
(30,'Amagá',1,5,NULL,NULL,NULL),
(31,'Amalfi',1,5,NULL,NULL,NULL),
(32,'Ambalema',1,73,NULL,NULL,NULL),
(33,'Anapoima',1,25,NULL,NULL,NULL),
(34,'Ancuya',1,52,NULL,NULL,NULL),
(35,'Andalucía',1,76,NULL,NULL,NULL),
(36,'Andes',1,5,NULL,NULL,NULL),
(37,'Angelópolis',1,5,NULL,NULL,NULL),
(38,'Angostura',1,5,NULL,NULL,NULL),
(39,'Anolaima',1,25,NULL,NULL,NULL),
(40,'Anorí',1,5,NULL,NULL,NULL),
(41,'Anserma',1,17,NULL,NULL,NULL),
(42,'Ansermanuevo',1,76,NULL,NULL,NULL),
(43,'Anzoátegui',1,73,NULL,NULL,NULL),
(44,'Anzá',1,5,NULL,NULL,NULL),
(45,'Apartadó',1,5,NULL,NULL,NULL),
(46,'Apulo',1,25,NULL,NULL,NULL),
(47,'Apía',1,66,NULL,NULL,NULL),
(48,'Aquitania',1,15,NULL,NULL,NULL),
(49,'Aracataca',1,47,NULL,NULL,NULL),
(50,'Aranzazu',1,17,NULL,NULL,NULL),
(51,'Aratoca',1,68,NULL,NULL,NULL),
(52,'Arauca',1,81,NULL,NULL,NULL),
(53,'Arauquita',1,81,NULL,NULL,NULL),
(54,'Arbeláez',1,25,NULL,NULL,NULL),
(55,'Arboleda (Berruecos)',1,52,NULL,NULL,NULL),
(56,'Arboledas',1,54,NULL,NULL,NULL),
(57,'Arboletes',1,5,NULL,NULL,NULL),
(58,'Arcabuco',1,15,NULL,NULL,NULL),
(59,'Arenal',1,13,NULL,NULL,NULL),
(60,'Argelia',1,5,NULL,NULL,NULL),
(61,'Argelia',1,19,NULL,NULL,NULL),
(62,'Argelia',1,76,NULL,NULL,NULL),
(63,'Ariguaní (El Difícil)',1,47,NULL,NULL,NULL),
(64,'Arjona',1,13,NULL,NULL,NULL),
(65,'Armenia',1,5,NULL,NULL,NULL),
(66,'Armenia',1,63,NULL,NULL,NULL),
(67,'Armero (Guayabal)',1,73,NULL,NULL,NULL),
(68,'Arroyohondo',1,13,NULL,NULL,NULL),
(69,'Astrea',1,20,NULL,NULL,NULL),
(70,'Ataco',1,73,NULL,NULL,NULL),
(71,'Atrato (Yuto)',1,27,NULL,NULL,NULL),
(72,'Ayapel',1,23,NULL,NULL,NULL),
(73,'Bagadó',1,27,NULL,NULL,NULL),
(74,'Bahía Solano (Mútis)',1,27,NULL,NULL,NULL),
(75,'Bajo Baudó (Pizarro)',1,27,NULL,NULL,NULL),
(76,'Balboa',1,19,NULL,NULL,NULL),
(77,'Balboa',1,66,NULL,NULL,NULL),
(78,'Baranoa',1,8,NULL,NULL,NULL),
(79,'Baraya',1,41,NULL,NULL,NULL),
(80,'Barbacoas',1,52,NULL,NULL,NULL),
(81,'Barbosa',1,5,NULL,NULL,NULL),
(82,'Barbosa',1,68,NULL,NULL,NULL),
(83,'Barichara',1,68,NULL,NULL,NULL),
(84,'Barranca de Upía',1,50,NULL,NULL,NULL),
(85,'Barrancabermeja',1,68,NULL,NULL,NULL),
(86,'Barrancas',1,44,NULL,NULL,NULL),
(87,'Barranco de Loba',1,13,NULL,NULL,NULL),
(88,'Barranquilla',1,8,NULL,NULL,NULL),
(89,'Becerríl',1,20,NULL,NULL,NULL),
(90,'Belalcázar',1,17,NULL,NULL,NULL),
(91,'Bello',1,5,NULL,NULL,NULL),
(92,'Belmira',1,5,NULL,NULL,NULL),
(93,'Beltrán',1,25,NULL,NULL,NULL),
(94,'Belén',1,15,NULL,NULL,NULL),
(95,'Belén',1,52,NULL,NULL,NULL),
(96,'Belén de Bajirá',1,27,NULL,NULL,NULL),
(97,'Belén de Umbría',1,66,NULL,NULL,NULL),
(98,'Belén de los Andaquíes',1,18,NULL,NULL,NULL),
(99,'Berbeo',1,15,NULL,NULL,NULL),
(100,'Betania',1,5,NULL,NULL,NULL),
(101,'Beteitiva',1,15,NULL,NULL,NULL),
(102,'Betulia',1,5,NULL,NULL,NULL),
(103,'Betulia',1,68,NULL,NULL,NULL),
(104,'Bituima',1,25,NULL,NULL,NULL),
(105,'Boavita',1,15,NULL,NULL,NULL),
(106,'Bochalema',1,54,NULL,NULL,NULL),
(107,'Bogotá D.C.',1,11,NULL,NULL,NULL),
(108,'Bojacá',1,25,NULL,NULL,NULL),
(109,'Bojayá (Bellavista)',1,27,NULL,NULL,NULL),
(110,'Bolívar',1,5,NULL,NULL,NULL),
(111,'Bolívar',1,19,NULL,NULL,NULL),
(112,'Bolívar',1,68,NULL,NULL,NULL),
(113,'Bolívar',1,76,NULL,NULL,NULL),
(114,'Bosconia',1,20,NULL,NULL,NULL),
(115,'Boyacá',1,15,NULL,NULL,NULL),
(116,'Briceño',1,5,NULL,NULL,NULL),
(117,'Briceño',1,15,NULL,NULL,NULL),
(118,'Bucaramanga',1,68,NULL,NULL,NULL),
(119,'Bucarasica',1,54,NULL,NULL,NULL),
(120,'Buenaventura',1,76,NULL,NULL,NULL),
(121,'Buenavista',1,15,NULL,NULL,NULL),
(122,'Buenavista',1,23,NULL,NULL,NULL),
(123,'Buenavista',1,63,NULL,NULL,NULL),
(124,'Buenavista',1,70,NULL,NULL,NULL),
(125,'Buenos Aires',1,19,NULL,NULL,NULL),
(126,'Buesaco',1,52,NULL,NULL,NULL),
(127,'Buga',1,76,NULL,NULL,NULL),
(128,'Bugalagrande',1,76,NULL,NULL,NULL),
(129,'Burítica',1,5,NULL,NULL,NULL),
(130,'Busbanza',1,15,NULL,NULL,NULL),
(131,'Cabrera',1,25,NULL,NULL,NULL),
(132,'Cabrera',1,68,NULL,NULL,NULL),
(133,'Cabuyaro',1,50,NULL,NULL,NULL),
(134,'Cachipay',1,25,NULL,NULL,NULL),
(135,'Caicedo',1,5,NULL,NULL,NULL),
(136,'Caicedonia',1,76,NULL,NULL,NULL),
(137,'Caimito',1,70,NULL,NULL,NULL),
(138,'Cajamarca',1,73,NULL,NULL,NULL),
(139,'Cajibío',1,19,NULL,NULL,NULL),
(140,'Cajicá',1,25,NULL,NULL,NULL),
(141,'Calamar',1,13,NULL,NULL,NULL),
(142,'Calamar',1,95,NULL,NULL,NULL),
(143,'Calarcá',1,63,NULL,NULL,NULL),
(144,'Caldas',1,5,NULL,NULL,NULL),
(145,'Caldas',1,15,NULL,NULL,NULL),
(146,'Caldono',1,19,NULL,NULL,NULL),
(147,'California',1,68,NULL,NULL,NULL),
(148,'Calima (Darién)',1,76,NULL,NULL,NULL),
(149,'Caloto',1,19,NULL,NULL,NULL),
(150,'Calí',1,76,NULL,NULL,NULL),
(151,'Campamento',1,5,NULL,NULL,NULL),
(152,'Campo de la Cruz',1,8,NULL,NULL,NULL),
(153,'Campoalegre',1,41,NULL,NULL,NULL),
(154,'Campohermoso',1,15,NULL,NULL,NULL),
(155,'Canalete',1,23,NULL,NULL,NULL),
(156,'Candelaria',1,8,NULL,NULL,NULL),
(157,'Candelaria',1,76,NULL,NULL,NULL),
(158,'Cantagallo',1,13,NULL,NULL,NULL),
(159,'Cantón de San Pablo',1,27,NULL,NULL,NULL),
(160,'Caparrapí',1,25,NULL,NULL,NULL),
(161,'Capitanejo',1,68,NULL,NULL,NULL),
(162,'Caracolí',1,5,NULL,NULL,NULL),
(163,'Caramanta',1,5,NULL,NULL,NULL),
(164,'Carcasí',1,68,NULL,NULL,NULL),
(165,'Carepa',1,5,NULL,NULL,NULL),
(166,'Carmen de Apicalá',1,73,NULL,NULL,NULL),
(167,'Carmen de Carupa',1,25,NULL,NULL,NULL),
(168,'Carmen de Viboral',1,5,NULL,NULL,NULL),
(169,'Carmen del Darién (CURBARADÓ)',1,27,NULL,NULL,NULL),
(170,'Carolina',1,5,NULL,NULL,NULL),
(171,'Cartagena',1,13,NULL,NULL,NULL),
(172,'Cartagena del Chairá',1,18,NULL,NULL,NULL),
(173,'Cartago',1,76,NULL,NULL,NULL),
(174,'Carurú',1,97,NULL,NULL,NULL),
(175,'Casabianca',1,73,NULL,NULL,NULL),
(176,'Castilla la Nueva',1,50,NULL,NULL,NULL),
(177,'Caucasia',1,5,NULL,NULL,NULL),
(178,'Cañasgordas',1,5,NULL,NULL,NULL),
(179,'Cepita',1,68,NULL,NULL,NULL),
(180,'Cereté',1,23,NULL,NULL,NULL),
(181,'Cerinza',1,15,NULL,NULL,NULL),
(182,'Cerrito',1,68,NULL,NULL,NULL),
(183,'Cerro San Antonio',1,47,NULL,NULL,NULL),
(184,'Chachaguí',1,52,NULL,NULL,NULL),
(185,'Chaguaní',1,25,NULL,NULL,NULL),
(186,'Chalán',1,70,NULL,NULL,NULL),
(187,'Chaparral',1,73,NULL,NULL,NULL),
(188,'Charalá',1,68,NULL,NULL,NULL),
(189,'Charta',1,68,NULL,NULL,NULL),
(190,'Chigorodó',1,5,NULL,NULL,NULL),
(191,'Chima',1,68,NULL,NULL,NULL),
(192,'Chimichagua',1,20,NULL,NULL,NULL),
(193,'Chimá',1,23,NULL,NULL,NULL),
(194,'Chinavita',1,15,NULL,NULL,NULL),
(195,'Chinchiná',1,17,NULL,NULL,NULL),
(196,'Chinácota',1,54,NULL,NULL,NULL),
(197,'Chinú',1,23,NULL,NULL,NULL),
(198,'Chipaque',1,25,NULL,NULL,NULL),
(199,'Chipatá',1,68,NULL,NULL,NULL),
(200,'Chiquinquirá',1,15,NULL,NULL,NULL),
(201,'Chiriguaná',1,20,NULL,NULL,NULL),
(202,'Chiscas',1,15,NULL,NULL,NULL),
(203,'Chita',1,15,NULL,NULL,NULL),
(204,'Chitagá',1,54,NULL,NULL,NULL),
(205,'Chitaraque',1,15,NULL,NULL,NULL),
(206,'Chivatá',1,15,NULL,NULL,NULL),
(207,'Chivolo',1,47,NULL,NULL,NULL),
(208,'Choachí',1,25,NULL,NULL,NULL),
(209,'Chocontá',1,25,NULL,NULL,NULL),
(210,'Chámeza',1,85,NULL,NULL,NULL),
(211,'Chía',1,25,NULL,NULL,NULL),
(212,'Chíquiza',1,15,NULL,NULL,NULL),
(213,'Chívor',1,15,NULL,NULL,NULL),
(214,'Cicuco',1,13,NULL,NULL,NULL),
(215,'Cimitarra',1,68,NULL,NULL,NULL),
(216,'Circasia',1,63,NULL,NULL,NULL),
(217,'Cisneros',1,5,NULL,NULL,NULL),
(218,'Ciénaga',1,15,NULL,NULL,NULL),
(219,'Ciénaga',1,47,NULL,NULL,NULL),
(220,'Ciénaga de Oro',1,23,NULL,NULL,NULL),
(221,'Clemencia',1,13,NULL,NULL,NULL),
(222,'Cocorná',1,5,NULL,NULL,NULL),
(223,'Coello',1,73,NULL,NULL,NULL),
(224,'Cogua',1,25,NULL,NULL,NULL),
(225,'Colombia',1,41,NULL,NULL,NULL),
(226,'Colosó (Ricaurte)',1,70,NULL,NULL,NULL),
(227,'Colón',1,86,NULL,NULL,NULL),
(228,'Colón (Génova)',1,52,NULL,NULL,NULL),
(229,'Concepción',1,5,NULL,NULL,NULL),
(230,'Concepción',1,68,NULL,NULL,NULL),
(231,'Concordia',1,5,NULL,NULL,NULL),
(232,'Concordia',1,47,NULL,NULL,NULL),
(233,'Condoto',1,27,NULL,NULL,NULL),
(234,'Confines',1,68,NULL,NULL,NULL),
(235,'Consaca',1,52,NULL,NULL,NULL),
(236,'Contadero',1,52,NULL,NULL,NULL),
(237,'Contratación',1,68,NULL,NULL,NULL),
(238,'Convención',1,54,NULL,NULL,NULL),
(239,'Copacabana',1,5,NULL,NULL,NULL),
(240,'Coper',1,15,NULL,NULL,NULL),
(241,'Cordobá',1,63,NULL,NULL,NULL),
(242,'Corinto',1,19,NULL,NULL,NULL),
(243,'Coromoro',1,68,NULL,NULL,NULL),
(244,'Corozal',1,70,NULL,NULL,NULL),
(245,'Corrales',1,15,NULL,NULL,NULL),
(246,'Cota',1,25,NULL,NULL,NULL),
(247,'Cotorra',1,23,NULL,NULL,NULL),
(248,'Covarachía',1,15,NULL,NULL,NULL),
(249,'Coveñas',1,70,NULL,NULL,NULL),
(250,'Coyaima',1,73,NULL,NULL,NULL),
(251,'Cravo Norte',1,81,NULL,NULL,NULL),
(252,'Cuaspud (Carlosama)',1,52,NULL,NULL,NULL),
(253,'Cubarral',1,50,NULL,NULL,NULL),
(254,'Cubará',1,15,NULL,NULL,NULL),
(255,'Cucaita',1,15,NULL,NULL,NULL),
(256,'Cucunubá',1,25,NULL,NULL,NULL),
(257,'Cucutilla',1,54,NULL,NULL,NULL),
(258,'Cuitiva',1,15,NULL,NULL,NULL),
(259,'Cumaral',1,50,NULL,NULL,NULL),
(260,'Cumaribo',1,99,NULL,NULL,NULL),
(261,'Cumbal',1,52,NULL,NULL,NULL),
(262,'Cumbitara',1,52,NULL,NULL,NULL),
(263,'Cunday',1,73,NULL,NULL,NULL),
(264,'Curillo',1,18,NULL,NULL,NULL),
(265,'Curití',1,68,NULL,NULL,NULL),
(266,'Curumaní',1,20,NULL,NULL,NULL),
(267,'Cáceres',1,5,NULL,NULL,NULL),
(268,'Cáchira',1,54,NULL,NULL,NULL),
(269,'Cácota',1,54,NULL,NULL,NULL),
(270,'Cáqueza',1,25,NULL,NULL,NULL),
(271,'Cértegui',1,27,NULL,NULL,NULL),
(272,'Cómbita',1,15,NULL,NULL,NULL),
(273,'Córdoba',1,13,NULL,NULL,NULL),
(274,'Córdoba',1,52,NULL,NULL,NULL),
(275,'Cúcuta',1,54,NULL,NULL,NULL),
(276,'Dabeiba',1,5,NULL,NULL,NULL),
(277,'Dagua',1,76,NULL,NULL,NULL),
(278,'Dibulla',1,44,NULL,NULL,NULL),
(279,'Distracción',1,44,NULL,NULL,NULL),
(280,'Dolores',1,73,NULL,NULL,NULL),
(281,'Don Matías',1,5,NULL,NULL,NULL),
(282,'Dos Quebradas',1,66,NULL,NULL,NULL),
(283,'Duitama',1,15,NULL,NULL,NULL),
(284,'Durania',1,54,NULL,NULL,NULL),
(285,'Ebéjico',1,5,NULL,NULL,NULL),
(286,'El Bagre',1,5,NULL,NULL,NULL),
(287,'El Banco',1,47,NULL,NULL,NULL),
(288,'El Cairo',1,76,NULL,NULL,NULL),
(289,'El Calvario',1,50,NULL,NULL,NULL),
(290,'El Carmen',1,54,NULL,NULL,NULL),
(291,'El Carmen',1,68,NULL,NULL,NULL),
(292,'El Carmen de Atrato',1,27,NULL,NULL,NULL),
(293,'El Carmen de Bolívar',1,13,NULL,NULL,NULL),
(294,'El Castillo',1,50,NULL,NULL,NULL),
(295,'El Cerrito',1,76,NULL,NULL,NULL),
(296,'El Charco',1,52,NULL,NULL,NULL),
(297,'El Cocuy',1,15,NULL,NULL,NULL),
(298,'El Colegio',1,25,NULL,NULL,NULL),
(299,'El Copey',1,20,NULL,NULL,NULL),
(300,'El Doncello',1,18,NULL,NULL,NULL),
(301,'El Dorado',1,50,NULL,NULL,NULL),
(302,'El Dovio',1,76,NULL,NULL,NULL),
(303,'El Espino',1,15,NULL,NULL,NULL),
(304,'El Guacamayo',1,68,NULL,NULL,NULL),
(305,'El Guamo',1,13,NULL,NULL,NULL),
(306,'El Molino',1,44,NULL,NULL,NULL),
(307,'El Paso',1,20,NULL,NULL,NULL),
(308,'El Paujil',1,18,NULL,NULL,NULL),
(309,'El Peñol',1,52,NULL,NULL,NULL),
(310,'El Peñon',1,13,NULL,NULL,NULL),
(311,'El Peñon',1,68,NULL,NULL,NULL),
(312,'El Peñón',1,25,NULL,NULL,NULL),
(313,'El Piñon',1,47,NULL,NULL,NULL),
(314,'El Playón',1,68,NULL,NULL,NULL),
(315,'El Retorno',1,95,NULL,NULL,NULL),
(316,'El Retén',1,47,NULL,NULL,NULL),
(317,'El Roble',1,70,NULL,NULL,NULL),
(318,'El Rosal',1,25,NULL,NULL,NULL),
(319,'El Rosario',1,52,NULL,NULL,NULL),
(320,'El Tablón de Gómez',1,52,NULL,NULL,NULL),
(321,'El Tambo',1,19,NULL,NULL,NULL),
(322,'El Tambo',1,52,NULL,NULL,NULL),
(323,'El Tarra',1,54,NULL,NULL,NULL),
(324,'El Zulia',1,54,NULL,NULL,NULL),
(325,'El Águila',1,76,NULL,NULL,NULL),
(326,'Elías',1,41,NULL,NULL,NULL),
(327,'Encino',1,68,NULL,NULL,NULL),
(328,'Enciso',1,68,NULL,NULL,NULL),
(329,'Entrerríos',1,5,NULL,NULL,NULL),
(330,'Envigado',1,5,NULL,NULL,NULL),
(331,'Espinal',1,73,NULL,NULL,NULL),
(332,'Facatativá',1,25,NULL,NULL,NULL),
(333,'Falan',1,73,NULL,NULL,NULL),
(334,'Filadelfia',1,17,NULL,NULL,NULL),
(335,'Filandia',1,63,NULL,NULL,NULL),
(336,'Firavitoba',1,15,NULL,NULL,NULL),
(337,'Flandes',1,73,NULL,NULL,NULL),
(338,'Florencia',1,18,NULL,NULL,NULL),
(339,'Florencia',1,19,NULL,NULL,NULL),
(340,'Floresta',1,15,NULL,NULL,NULL),
(341,'Florida',1,76,NULL,NULL,NULL),
(342,'Floridablanca',1,68,NULL,NULL,NULL),
(343,'Florián',1,68,NULL,NULL,NULL),
(344,'Fonseca',1,44,NULL,NULL,NULL),
(345,'Fortúl',1,81,NULL,NULL,NULL),
(346,'Fosca',1,25,NULL,NULL,NULL),
(347,'Francisco Pizarro',1,52,NULL,NULL,NULL),
(348,'Fredonia',1,5,NULL,NULL,NULL),
(349,'Fresno',1,73,NULL,NULL,NULL),
(350,'Frontino',1,5,NULL,NULL,NULL),
(351,'Fuente de Oro',1,50,NULL,NULL,NULL),
(352,'Fundación',1,47,NULL,NULL,NULL),
(353,'Funes',1,52,NULL,NULL,NULL),
(354,'Funza',1,25,NULL,NULL,NULL),
(355,'Fusagasugá',1,25,NULL,NULL,NULL),
(356,'Fómeque',1,25,NULL,NULL,NULL),
(357,'Fúquene',1,25,NULL,NULL,NULL),
(358,'Gachalá',1,25,NULL,NULL,NULL),
(359,'Gachancipá',1,25,NULL,NULL,NULL),
(360,'Gachantivá',1,15,NULL,NULL,NULL),
(361,'Gachetá',1,25,NULL,NULL,NULL),
(362,'Galapa',1,8,NULL,NULL,NULL),
(363,'Galeras (Nueva Granada)',1,70,NULL,NULL,NULL),
(364,'Galán',1,68,NULL,NULL,NULL),
(365,'Gama',1,25,NULL,NULL,NULL),
(366,'Gamarra',1,20,NULL,NULL,NULL),
(367,'Garagoa',1,15,NULL,NULL,NULL),
(368,'Garzón',1,41,NULL,NULL,NULL),
(369,'Gigante',1,41,NULL,NULL,NULL),
(370,'Ginebra',1,76,NULL,NULL,NULL),
(371,'Giraldo',1,5,NULL,NULL,NULL),
(372,'Girardot',1,25,NULL,NULL,NULL),
(373,'Girardota',1,5,NULL,NULL,NULL),
(374,'Girón',1,68,NULL,NULL,NULL),
(375,'Gonzalez',1,20,NULL,NULL,NULL),
(376,'Gramalote',1,54,NULL,NULL,NULL),
(377,'Granada',1,5,NULL,NULL,NULL),
(378,'Granada',1,25,NULL,NULL,NULL),
(379,'Granada',1,50,NULL,NULL,NULL),
(380,'Guaca',1,68,NULL,NULL,NULL),
(381,'Guacamayas',1,15,NULL,NULL,NULL),
(382,'Guacarí',1,76,NULL,NULL,NULL),
(383,'Guachavés',1,52,NULL,NULL,NULL),
(384,'Guachené',1,19,NULL,NULL,NULL),
(385,'Guachetá',1,25,NULL,NULL,NULL),
(386,'Guachucal',1,52,NULL,NULL,NULL),
(387,'Guadalupe',1,5,NULL,NULL,NULL),
(388,'Guadalupe',1,41,NULL,NULL,NULL),
(389,'Guadalupe',1,68,NULL,NULL,NULL),
(390,'Guaduas',1,25,NULL,NULL,NULL),
(391,'Guaitarilla',1,52,NULL,NULL,NULL),
(392,'Gualmatán',1,52,NULL,NULL,NULL),
(393,'Guamal',1,47,NULL,NULL,NULL),
(394,'Guamal',1,50,NULL,NULL,NULL),
(395,'Guamo',1,73,NULL,NULL,NULL),
(396,'Guapota',1,68,NULL,NULL,NULL),
(397,'Guapí',1,19,NULL,NULL,NULL),
(398,'Guaranda',1,70,NULL,NULL,NULL),
(399,'Guarne',1,5,NULL,NULL,NULL),
(400,'Guasca',1,25,NULL,NULL,NULL),
(401,'Guatapé',1,5,NULL,NULL,NULL),
(402,'Guataquí',1,25,NULL,NULL,NULL),
(403,'Guatavita',1,25,NULL,NULL,NULL),
(404,'Guateque',1,15,NULL,NULL,NULL),
(405,'Guavatá',1,68,NULL,NULL,NULL),
(406,'Guayabal de Siquima',1,25,NULL,NULL,NULL),
(407,'Guayabetal',1,25,NULL,NULL,NULL),
(408,'Guayatá',1,15,NULL,NULL,NULL),
(409,'Guepsa',1,68,NULL,NULL,NULL),
(410,'Guicán',1,15,NULL,NULL,NULL),
(411,'Gutiérrez',1,25,NULL,NULL,NULL),
(412,'Guática',1,66,NULL,NULL,NULL),
(413,'Gámbita',1,68,NULL,NULL,NULL),
(414,'Gámeza',1,15,NULL,NULL,NULL),
(415,'Génova',1,63,NULL,NULL,NULL),
(416,'Gómez Plata',1,5,NULL,NULL,NULL),
(417,'Hacarí',1,54,NULL,NULL,NULL),
(418,'Hatillo de Loba',1,13,NULL,NULL,NULL),
(419,'Hato',1,68,NULL,NULL,NULL),
(420,'Hato Corozal',1,85,NULL,NULL,NULL),
(421,'Hatonuevo',1,44,NULL,NULL,NULL),
(422,'Heliconia',1,5,NULL,NULL,NULL),
(423,'Herrán',1,54,NULL,NULL,NULL),
(424,'Herveo',1,73,NULL,NULL,NULL),
(425,'Hispania',1,5,NULL,NULL,NULL),
(426,'Hobo',1,41,NULL,NULL,NULL),
(427,'Honda',1,73,NULL,NULL,NULL),
(428,'Ibagué',1,73,NULL,NULL,NULL),
(429,'Icononzo',1,73,NULL,NULL,NULL),
(430,'Iles',1,52,NULL,NULL,NULL),
(431,'Imúes',1,52,NULL,NULL,NULL),
(432,'Inzá',1,19,NULL,NULL,NULL),
(433,'Inírida',1,94,NULL,NULL,NULL),
(434,'Ipiales',1,52,NULL,NULL,NULL),
(435,'Isnos',1,41,NULL,NULL,NULL),
(436,'Istmina',1,27,NULL,NULL,NULL),
(437,'Itagüí',1,5,NULL,NULL,NULL),
(438,'Ituango',1,5,NULL,NULL,NULL),
(439,'Izá',1,15,NULL,NULL,NULL),
(440,'Jambaló',1,19,NULL,NULL,NULL),
(441,'Jamundí',1,76,NULL,NULL,NULL),
(442,'Jardín',1,5,NULL,NULL,NULL),
(443,'Jenesano',1,15,NULL,NULL,NULL),
(444,'Jericó',1,5,NULL,NULL,NULL),
(445,'Jericó',1,15,NULL,NULL,NULL),
(446,'Jerusalén',1,25,NULL,NULL,NULL),
(447,'Jesús María',1,68,NULL,NULL,NULL),
(448,'Jordán',1,68,NULL,NULL,NULL),
(449,'Juan de Acosta',1,8,NULL,NULL,NULL),
(450,'Junín',1,25,NULL,NULL,NULL),
(451,'Juradó',1,27,NULL,NULL,NULL),
(452,'La Apartada y La Frontera',1,23,NULL,NULL,NULL),
(453,'La Argentina',1,41,NULL,NULL,NULL),
(454,'La Belleza',1,68,NULL,NULL,NULL),
(455,'La Calera',1,25,NULL,NULL,NULL),
(456,'La Capilla',1,15,NULL,NULL,NULL),
(457,'La Ceja',1,5,NULL,NULL,NULL),
(458,'La Celia',1,66,NULL,NULL,NULL),
(459,'La Cruz',1,52,NULL,NULL,NULL),
(460,'La Cumbre',1,76,NULL,NULL,NULL),
(461,'La Dorada',1,17,NULL,NULL,NULL),
(462,'La Esperanza',1,54,NULL,NULL,NULL),
(463,'La Estrella',1,5,NULL,NULL,NULL),
(464,'La Florida',1,52,NULL,NULL,NULL),
(465,'La Gloria',1,20,NULL,NULL,NULL),
(466,'La Jagua de Ibirico',1,20,NULL,NULL,NULL),
(467,'La Jagua del Pilar',1,44,NULL,NULL,NULL),
(468,'La Llanada',1,52,NULL,NULL,NULL),
(469,'La Macarena',1,50,NULL,NULL,NULL),
(470,'La Merced',1,17,NULL,NULL,NULL),
(471,'La Mesa',1,25,NULL,NULL,NULL),
(472,'La Montañita',1,18,NULL,NULL,NULL),
(473,'La Palma',1,25,NULL,NULL,NULL),
(474,'La Paz',1,68,NULL,NULL,NULL),
(475,'La Paz (Robles)',1,20,NULL,NULL,NULL),
(476,'La Peña',1,25,NULL,NULL,NULL),
(477,'La Pintada',1,5,NULL,NULL,NULL),
(478,'La Plata',1,41,NULL,NULL,NULL),
(479,'La Playa',1,54,NULL,NULL,NULL),
(480,'La Primavera',1,99,NULL,NULL,NULL),
(481,'La Salina',1,85,NULL,NULL,NULL),
(482,'La Sierra',1,19,NULL,NULL,NULL),
(483,'La Tebaida',1,63,NULL,NULL,NULL),
(484,'La Tola',1,52,NULL,NULL,NULL),
(485,'La Unión',1,5,NULL,NULL,NULL),
(486,'La Unión',1,52,NULL,NULL,NULL),
(487,'La Unión',1,70,NULL,NULL,NULL),
(488,'La Unión',1,76,NULL,NULL,NULL),
(489,'La Uvita',1,15,NULL,NULL,NULL),
(490,'La Vega',1,19,NULL,NULL,NULL),
(491,'La Vega',1,25,NULL,NULL,NULL),
(492,'La Victoria',1,15,NULL,NULL,NULL),
(493,'La Victoria',1,17,NULL,NULL,NULL),
(494,'La Victoria',1,76,NULL,NULL,NULL),
(495,'La Virginia',1,66,NULL,NULL,NULL),
(496,'Labateca',1,54,NULL,NULL,NULL),
(497,'Labranzagrande',1,15,NULL,NULL,NULL),
(498,'Landázuri',1,68,NULL,NULL,NULL),
(499,'Lebrija',1,68,NULL,NULL,NULL),
(500,'Leiva',1,52,NULL,NULL,NULL),
(501,'Lejanías',1,50,NULL,NULL,NULL),
(502,'Lenguazaque',1,25,NULL,NULL,NULL),
(503,'Leticia',1,91,NULL,NULL,NULL),
(504,'Liborina',1,5,NULL,NULL,NULL),
(505,'Linares',1,52,NULL,NULL,NULL),
(506,'Lloró',1,27,NULL,NULL,NULL),
(507,'Lorica',1,23,NULL,NULL,NULL),
(508,'Los Córdobas',1,23,NULL,NULL,NULL),
(509,'Los Palmitos',1,70,NULL,NULL,NULL),
(510,'Los Patios',1,54,NULL,NULL,NULL),
(511,'Los Santos',1,68,NULL,NULL,NULL),
(512,'Lourdes',1,54,NULL,NULL,NULL),
(513,'Luruaco',1,8,NULL,NULL,NULL),
(514,'Lérida',1,73,NULL,NULL,NULL),
(515,'Líbano',1,73,NULL,NULL,NULL),
(516,'López (Micay)',1,19,NULL,NULL,NULL),
(517,'Macanal',1,15,NULL,NULL,NULL),
(518,'Macaravita',1,68,NULL,NULL,NULL),
(519,'Maceo',1,5,NULL,NULL,NULL),
(520,'Machetá',1,25,NULL,NULL,NULL),
(521,'Madrid',1,25,NULL,NULL,NULL),
(522,'Magangué',1,13,NULL,NULL,NULL),
(523,'Magüi (Payán)',1,52,NULL,NULL,NULL),
(524,'Mahates',1,13,NULL,NULL,NULL),
(525,'Maicao',1,44,NULL,NULL,NULL),
(526,'Majagual',1,70,NULL,NULL,NULL),
(527,'Malambo',1,8,NULL,NULL,NULL),
(528,'Mallama (Piedrancha)',1,52,NULL,NULL,NULL),
(529,'Manatí',1,8,NULL,NULL,NULL),
(530,'Manaure',1,44,NULL,NULL,NULL),
(531,'Manaure Balcón del Cesar',1,20,NULL,NULL,NULL),
(532,'Manizales',1,17,NULL,NULL,NULL),
(533,'Manta',1,25,NULL,NULL,NULL),
(534,'Manzanares',1,17,NULL,NULL,NULL),
(535,'Maní',1,85,NULL,NULL,NULL),
(536,'Mapiripan',1,50,NULL,NULL,NULL),
(537,'Margarita',1,13,NULL,NULL,NULL),
(538,'Marinilla',1,5,NULL,NULL,NULL),
(539,'Maripí',1,15,NULL,NULL,NULL),
(540,'Mariquita',1,73,NULL,NULL,NULL),
(541,'Marmato',1,17,NULL,NULL,NULL),
(542,'Marquetalia',1,17,NULL,NULL,NULL),
(543,'Marsella',1,66,NULL,NULL,NULL),
(544,'Marulanda',1,17,NULL,NULL,NULL),
(545,'María la Baja',1,13,NULL,NULL,NULL),
(546,'Matanza',1,68,NULL,NULL,NULL),
(547,'Medellín',1,5,NULL,NULL,NULL),
(548,'Medina',1,25,NULL,NULL,NULL),
(549,'Medio Atrato',1,27,NULL,NULL,NULL),
(550,'Medio Baudó',1,27,NULL,NULL,NULL),
(551,'Medio San Juan (ANDAGOYA)',1,27,NULL,NULL,NULL),
(552,'Melgar',1,73,NULL,NULL,NULL),
(553,'Mercaderes',1,19,NULL,NULL,NULL),
(554,'Mesetas',1,50,NULL,NULL,NULL),
(555,'Milán',1,18,NULL,NULL,NULL),
(556,'Miraflores',1,15,NULL,NULL,NULL),
(557,'Miraflores',1,95,NULL,NULL,NULL),
(558,'Miranda',1,19,NULL,NULL,NULL),
(559,'Mistrató',1,66,NULL,NULL,NULL),
(560,'Mitú',1,97,NULL,NULL,NULL),
(561,'Mocoa',1,86,NULL,NULL,NULL),
(562,'Mogotes',1,68,NULL,NULL,NULL),
(563,'Molagavita',1,68,NULL,NULL,NULL),
(564,'Momil',1,23,NULL,NULL,NULL),
(565,'Mompós',1,13,NULL,NULL,NULL),
(566,'Mongua',1,15,NULL,NULL,NULL),
(567,'Monguí',1,15,NULL,NULL,NULL),
(568,'Moniquirá',1,15,NULL,NULL,NULL),
(569,'Montebello',1,5,NULL,NULL,NULL),
(570,'Montecristo',1,13,NULL,NULL,NULL),
(571,'Montelíbano',1,23,NULL,NULL,NULL),
(572,'Montenegro',1,63,NULL,NULL,NULL),
(573,'Monteria',1,23,NULL,NULL,NULL),
(574,'Monterrey',1,85,NULL,NULL,NULL),
(575,'Morales',1,13,NULL,NULL,NULL),
(576,'Morales',1,19,NULL,NULL,NULL),
(577,'Morelia',1,18,NULL,NULL,NULL),
(578,'Morroa',1,70,NULL,NULL,NULL),
(579,'Mosquera',1,25,NULL,NULL,NULL),
(580,'Mosquera',1,52,NULL,NULL,NULL),
(581,'Motavita',1,15,NULL,NULL,NULL),
(582,'Moñitos',1,23,NULL,NULL,NULL),
(583,'Murillo',1,73,NULL,NULL,NULL),
(584,'Murindó',1,5,NULL,NULL,NULL),
(585,'Mutatá',1,5,NULL,NULL,NULL),
(586,'Mutiscua',1,54,NULL,NULL,NULL),
(587,'Muzo',1,15,NULL,NULL,NULL),
(588,'Málaga',1,68,NULL,NULL,NULL),
(589,'Nariño',1,5,NULL,NULL,NULL),
(590,'Nariño',1,25,NULL,NULL,NULL),
(591,'Nariño',1,52,NULL,NULL,NULL),
(592,'Natagaima',1,73,NULL,NULL,NULL),
(593,'Nechí',1,5,NULL,NULL,NULL),
(594,'Necoclí',1,5,NULL,NULL,NULL),
(595,'Neira',1,17,NULL,NULL,NULL),
(596,'Neiva',1,41,NULL,NULL,NULL),
(597,'Nemocón',1,25,NULL,NULL,NULL),
(598,'Nilo',1,25,NULL,NULL,NULL),
(599,'Nimaima',1,25,NULL,NULL,NULL),
(600,'Nobsa',1,15,NULL,NULL,NULL),
(601,'Nocaima',1,25,NULL,NULL,NULL),
(602,'Norcasia',1,17,NULL,NULL,NULL),
(603,'Norosí',1,13,NULL,NULL,NULL),
(604,'Novita',1,27,NULL,NULL,NULL),
(605,'Nueva Granada',1,47,NULL,NULL,NULL),
(606,'Nuevo Colón',1,15,NULL,NULL,NULL),
(607,'Nunchía',1,85,NULL,NULL,NULL),
(608,'Nuquí',1,27,NULL,NULL,NULL),
(609,'Nátaga',1,41,NULL,NULL,NULL),
(610,'Obando',1,76,NULL,NULL,NULL),
(611,'Ocamonte',1,68,NULL,NULL,NULL),
(612,'Ocaña',1,54,NULL,NULL,NULL),
(613,'Oiba',1,68,NULL,NULL,NULL),
(614,'Oicatá',1,15,NULL,NULL,NULL),
(615,'Olaya',1,5,NULL,NULL,NULL),
(616,'Olaya Herrera',1,52,NULL,NULL,NULL),
(617,'Onzaga',1,68,NULL,NULL,NULL),
(618,'Oporapa',1,41,NULL,NULL,NULL),
(619,'Orito',1,86,NULL,NULL,NULL),
(620,'Orocué',1,85,NULL,NULL,NULL),
(621,'Ortega',1,73,NULL,NULL,NULL),
(622,'Ospina',1,52,NULL,NULL,NULL),
(623,'Otanche',1,15,NULL,NULL,NULL),
(624,'Ovejas',1,70,NULL,NULL,NULL),
(625,'Pachavita',1,15,NULL,NULL,NULL),
(626,'Pacho',1,25,NULL,NULL,NULL),
(627,'Padilla',1,19,NULL,NULL,NULL),
(628,'Paicol',1,41,NULL,NULL,NULL),
(629,'Pailitas',1,20,NULL,NULL,NULL),
(630,'Paime',1,25,NULL,NULL,NULL),
(631,'Paipa',1,15,NULL,NULL,NULL),
(632,'Pajarito',1,15,NULL,NULL,NULL),
(633,'Palermo',1,41,NULL,NULL,NULL),
(634,'Palestina',1,17,NULL,NULL,NULL),
(635,'Palestina',1,41,NULL,NULL,NULL),
(636,'Palmar',1,68,NULL,NULL,NULL),
(637,'Palmar de Varela',1,8,NULL,NULL,NULL),
(638,'Palmas del Socorro',1,68,NULL,NULL,NULL),
(639,'Palmira',1,76,NULL,NULL,NULL),
(640,'Palmito',1,70,NULL,NULL,NULL),
(641,'Palocabildo',1,73,NULL,NULL,NULL),
(642,'Pamplona',1,54,NULL,NULL,NULL),
(643,'Pamplonita',1,54,NULL,NULL,NULL),
(644,'Pandi',1,25,NULL,NULL,NULL),
(645,'Panqueba',1,15,NULL,NULL,NULL),
(646,'Paratebueno',1,25,NULL,NULL,NULL),
(647,'Pasca',1,25,NULL,NULL,NULL),
(648,'Patía (El Bordo)',1,19,NULL,NULL,NULL),
(649,'Pauna',1,15,NULL,NULL,NULL),
(650,'Paya',1,15,NULL,NULL,NULL),
(651,'Paz de Ariporo',1,85,NULL,NULL,NULL),
(652,'Paz de Río',1,15,NULL,NULL,NULL),
(653,'Pedraza',1,47,NULL,NULL,NULL),
(654,'Pelaya',1,20,NULL,NULL,NULL),
(655,'Pensilvania',1,17,NULL,NULL,NULL),
(656,'Peque',1,5,NULL,NULL,NULL),
(657,'Pereira',1,66,NULL,NULL,NULL),
(658,'Pesca',1,15,NULL,NULL,NULL),
(659,'Peñol',1,5,NULL,NULL,NULL),
(660,'Piamonte',1,19,NULL,NULL,NULL),
(661,'Pie de Cuesta',1,68,NULL,NULL,NULL),
(662,'Piedras',1,73,NULL,NULL,NULL),
(663,'Piendamó',1,19,NULL,NULL,NULL),
(664,'Pijao',1,63,NULL,NULL,NULL),
(665,'Pijiño',1,47,NULL,NULL,NULL),
(666,'Pinchote',1,68,NULL,NULL,NULL),
(667,'Pinillos',1,13,NULL,NULL,NULL),
(668,'Piojo',1,8,NULL,NULL,NULL),
(669,'Pisva',1,15,NULL,NULL,NULL),
(670,'Pital',1,41,NULL,NULL,NULL),
(671,'Pitalito',1,41,NULL,NULL,NULL),
(672,'Pivijay',1,47,NULL,NULL,NULL),
(673,'Planadas',1,73,NULL,NULL,NULL),
(674,'Planeta Rica',1,23,NULL,NULL,NULL),
(675,'Plato',1,47,NULL,NULL,NULL),
(676,'Policarpa',1,52,NULL,NULL,NULL),
(677,'Polonuevo',1,8,NULL,NULL,NULL),
(678,'Ponedera',1,8,NULL,NULL,NULL),
(679,'Popayán',1,19,NULL,NULL,NULL),
(680,'Pore',1,85,NULL,NULL,NULL),
(681,'Potosí',1,52,NULL,NULL,NULL),
(682,'Pradera',1,76,NULL,NULL,NULL),
(683,'Prado',1,73,NULL,NULL,NULL),
(684,'Providencia',1,52,NULL,NULL,NULL),
(685,'Providencia',1,88,NULL,NULL,NULL),
(686,'Pueblo Bello',1,20,NULL,NULL,NULL),
(687,'Pueblo Nuevo',1,23,NULL,NULL,NULL),
(688,'Pueblo Rico',1,66,NULL,NULL,NULL),
(689,'Pueblorrico',1,5,NULL,NULL,NULL),
(690,'Puebloviejo',1,47,NULL,NULL,NULL),
(691,'Puente Nacional',1,68,NULL,NULL,NULL),
(692,'Puerres',1,52,NULL,NULL,NULL),
(693,'Puerto Asís',1,86,NULL,NULL,NULL),
(694,'Puerto Berrío',1,5,NULL,NULL,NULL),
(695,'Puerto Boyacá',1,15,NULL,NULL,NULL),
(696,'Puerto Caicedo',1,86,NULL,NULL,NULL),
(697,'Puerto Carreño',1,99,NULL,NULL,NULL),
(698,'Puerto Colombia',1,8,NULL,NULL,NULL),
(699,'Puerto Concordia',1,50,NULL,NULL,NULL),
(700,'Puerto Escondido',1,23,NULL,NULL,NULL),
(701,'Puerto Gaitán',1,50,NULL,NULL,NULL),
(702,'Puerto Guzmán',1,86,NULL,NULL,NULL),
(703,'Puerto Leguízamo',1,86,NULL,NULL,NULL),
(704,'Puerto Libertador',1,23,NULL,NULL,NULL),
(705,'Puerto Lleras',1,50,NULL,NULL,NULL),
(706,'Puerto López',1,50,NULL,NULL,NULL),
(707,'Puerto Nare',1,5,NULL,NULL,NULL),
(708,'Puerto Nariño',1,91,NULL,NULL,NULL),
(709,'Puerto Parra',1,68,NULL,NULL,NULL),
(710,'Puerto Rico',1,18,NULL,NULL,NULL),
(711,'Puerto Rico',1,50,NULL,NULL,NULL),
(712,'Puerto Rondón',1,81,NULL,NULL,NULL),
(713,'Puerto Salgar',1,25,NULL,NULL,NULL),
(714,'Puerto Santander',1,54,NULL,NULL,NULL),
(715,'Puerto Tejada',1,19,NULL,NULL,NULL),
(716,'Puerto Triunfo',1,5,NULL,NULL,NULL),
(717,'Puerto Wilches',1,68,NULL,NULL,NULL),
(718,'Pulí',1,25,NULL,NULL,NULL),
(719,'Pupiales',1,52,NULL,NULL,NULL),
(720,'Puracé (Coconuco)',1,19,NULL,NULL,NULL),
(721,'Purificación',1,73,NULL,NULL,NULL),
(722,'Purísima',1,23,NULL,NULL,NULL),
(723,'Pácora',1,17,NULL,NULL,NULL),
(724,'Páez',1,15,NULL,NULL,NULL),
(725,'Páez (Belalcazar)',1,19,NULL,NULL,NULL),
(726,'Páramo',1,68,NULL,NULL,NULL),
(727,'Quebradanegra',1,25,NULL,NULL,NULL),
(728,'Quetame',1,25,NULL,NULL,NULL),
(729,'Quibdó',1,27,NULL,NULL,NULL),
(730,'Quimbaya',1,63,NULL,NULL,NULL),
(731,'Quinchía',1,66,NULL,NULL,NULL),
(732,'Quipama',1,15,NULL,NULL,NULL),
(733,'Quipile',1,25,NULL,NULL,NULL),
(734,'Ragonvalia',1,54,NULL,NULL,NULL),
(735,'Ramiriquí',1,15,NULL,NULL,NULL),
(736,'Recetor',1,85,NULL,NULL,NULL),
(737,'Regidor',1,13,NULL,NULL,NULL),
(738,'Remedios',1,5,NULL,NULL,NULL),
(739,'Remolino',1,47,NULL,NULL,NULL),
(740,'Repelón',1,8,NULL,NULL,NULL),
(741,'Restrepo',1,50,NULL,NULL,NULL),
(742,'Restrepo',1,76,NULL,NULL,NULL),
(743,'Retiro',1,5,NULL,NULL,NULL),
(744,'Ricaurte',1,25,NULL,NULL,NULL),
(745,'Ricaurte',1,52,NULL,NULL,NULL),
(746,'Rio Negro',1,68,NULL,NULL,NULL),
(747,'Rioblanco',1,73,NULL,NULL,NULL),
(748,'Riofrío',1,76,NULL,NULL,NULL),
(749,'Riohacha',1,44,NULL,NULL,NULL),
(750,'Risaralda',1,17,NULL,NULL,NULL),
(751,'Rivera',1,41,NULL,NULL,NULL),
(752,'Roberto Payán (San José)',1,52,NULL,NULL,NULL),
(753,'Roldanillo',1,76,NULL,NULL,NULL),
(754,'Roncesvalles',1,73,NULL,NULL,NULL),
(755,'Rondón',1,15,NULL,NULL,NULL),
(756,'Rosas',1,19,NULL,NULL,NULL),
(757,'Rovira',1,73,NULL,NULL,NULL),
(758,'Ráquira',1,15,NULL,NULL,NULL),
(759,'Río Iró',1,27,NULL,NULL,NULL),
(760,'Río Quito',1,27,NULL,NULL,NULL),
(761,'Río Sucio',1,17,NULL,NULL,NULL),
(762,'Río Viejo',1,13,NULL,NULL,NULL),
(763,'Río de oro',1,20,NULL,NULL,NULL),
(764,'Ríonegro',1,5,NULL,NULL,NULL),
(765,'Ríosucio',1,27,NULL,NULL,NULL),
(766,'Sabana de Torres',1,68,NULL,NULL,NULL),
(767,'Sabanagrande',1,8,NULL,NULL,NULL),
(768,'Sabanalarga',1,5,NULL,NULL,NULL),
(769,'Sabanalarga',1,8,NULL,NULL,NULL),
(770,'Sabanalarga',1,85,NULL,NULL,NULL),
(771,'Sabanas de San Angel (SAN ANGEL)',1,47,NULL,NULL,NULL),
(772,'Sabaneta',1,5,NULL,NULL,NULL),
(773,'Saboyá',1,15,NULL,NULL,NULL),
(774,'Sahagún',1,23,NULL,NULL,NULL),
(775,'Saladoblanco',1,41,NULL,NULL,NULL),
(776,'Salamina',1,17,NULL,NULL,NULL),
(777,'Salamina',1,47,NULL,NULL,NULL),
(778,'Salazar',1,54,NULL,NULL,NULL),
(779,'Saldaña',1,73,NULL,NULL,NULL),
(780,'Salento',1,63,NULL,NULL,NULL),
(781,'Salgar',1,5,NULL,NULL,NULL),
(782,'Samacá',1,15,NULL,NULL,NULL),
(783,'Samaniego',1,52,NULL,NULL,NULL),
(784,'Samaná',1,17,NULL,NULL,NULL),
(785,'Sampués',1,70,NULL,NULL,NULL),
(786,'San Agustín',1,41,NULL,NULL,NULL),
(787,'San Alberto',1,20,NULL,NULL,NULL),
(788,'San Andrés',1,68,NULL,NULL,NULL),
(789,'San Andrés Sotavento',1,23,NULL,NULL,NULL),
(790,'San Andrés de Cuerquía',1,5,NULL,NULL,NULL),
(791,'San Antero',1,23,NULL,NULL,NULL),
(792,'San Antonio',1,73,NULL,NULL,NULL),
(793,'San Antonio de Tequendama',1,25,NULL,NULL,NULL),
(794,'San Benito',1,68,NULL,NULL,NULL),
(795,'San Benito Abad',1,70,NULL,NULL,NULL),
(796,'San Bernardo',1,25,NULL,NULL,NULL),
(797,'San Bernardo',1,52,NULL,NULL,NULL),
(798,'San Bernardo del Viento',1,23,NULL,NULL,NULL),
(799,'San Calixto',1,54,NULL,NULL,NULL),
(800,'San Carlos',1,5,NULL,NULL,NULL),
(801,'San Carlos',1,23,NULL,NULL,NULL),
(802,'San Carlos de Guaroa',1,50,NULL,NULL,NULL),
(803,'San Cayetano',1,25,NULL,NULL,NULL),
(804,'San Cayetano',1,54,NULL,NULL,NULL),
(805,'San Cristobal',1,13,NULL,NULL,NULL),
(806,'San Diego',1,20,NULL,NULL,NULL),
(807,'San Eduardo',1,15,NULL,NULL,NULL),
(808,'San Estanislao',1,13,NULL,NULL,NULL),
(809,'San Fernando',1,13,NULL,NULL,NULL),
(810,'San Francisco',1,5,NULL,NULL,NULL),
(811,'San Francisco',1,25,NULL,NULL,NULL),
(812,'San Francisco',1,86,NULL,NULL,NULL),
(813,'San Gíl',1,68,NULL,NULL,NULL),
(814,'San Jacinto',1,13,NULL,NULL,NULL),
(815,'San Jacinto del Cauca',1,13,NULL,NULL,NULL),
(816,'San Jerónimo',1,5,NULL,NULL,NULL),
(817,'San Joaquín',1,68,NULL,NULL,NULL),
(818,'San José',1,17,NULL,NULL,NULL),
(819,'San José de Miranda',1,68,NULL,NULL,NULL),
(820,'San José de Montaña',1,5,NULL,NULL,NULL),
(821,'San José de Pare',1,15,NULL,NULL,NULL),
(822,'San José de Uré',1,23,NULL,NULL,NULL),
(823,'San José del Fragua',1,18,NULL,NULL,NULL),
(824,'San José del Guaviare',1,95,NULL,NULL,NULL),
(825,'San José del Palmar',1,27,NULL,NULL,NULL),
(826,'San Juan de Arama',1,50,NULL,NULL,NULL),
(827,'San Juan de Betulia',1,70,NULL,NULL,NULL),
(828,'San Juan de Nepomuceno',1,13,NULL,NULL,NULL),
(829,'Pasto',1,52,NULL,NULL,NULL),
(830,'San Juan de Río Seco',1,25,NULL,NULL,NULL),
(831,'San Juan de Urabá',1,5,NULL,NULL,NULL),
(832,'San Juan del Cesar',1,44,NULL,NULL,NULL),
(833,'San Juanito',1,50,NULL,NULL,NULL),
(834,'San Lorenzo',1,52,NULL,NULL,NULL),
(835,'San Luis',1,73,NULL,NULL,NULL),
(836,'San Luís',1,5,NULL,NULL,NULL),
(837,'San Luís de Gaceno',1,15,NULL,NULL,NULL),
(838,'San Luís de Palenque',1,85,NULL,NULL,NULL),
(839,'San Marcos',1,70,NULL,NULL,NULL),
(840,'San Martín',1,20,NULL,NULL,NULL),
(841,'San Martín',1,50,NULL,NULL,NULL),
(842,'San Martín de Loba',1,13,NULL,NULL,NULL),
(843,'San Mateo',1,15,NULL,NULL,NULL),
(844,'San Miguel',1,68,NULL,NULL,NULL),
(845,'San Miguel',1,86,NULL,NULL,NULL),
(846,'San Miguel de Sema',1,15,NULL,NULL,NULL),
(847,'San Onofre',1,70,NULL,NULL,NULL),
(848,'San Pablo',1,13,NULL,NULL,NULL),
(849,'San Pablo',1,52,NULL,NULL,NULL),
(850,'San Pablo de Borbur',1,15,NULL,NULL,NULL),
(851,'San Pedro',1,5,NULL,NULL,NULL),
(852,'San Pedro',1,70,NULL,NULL,NULL),
(853,'San Pedro',1,76,NULL,NULL,NULL),
(854,'San Pedro de Cartago',1,52,NULL,NULL,NULL),
(855,'San Pedro de Urabá',1,5,NULL,NULL,NULL),
(856,'San Pelayo',1,23,NULL,NULL,NULL),
(857,'San Rafael',1,5,NULL,NULL,NULL),
(858,'San Roque',1,5,NULL,NULL,NULL),
(859,'San Sebastián',1,19,NULL,NULL,NULL),
(860,'San Sebastián de Buenavista',1,47,NULL,NULL,NULL),
(861,'San Vicente',1,5,NULL,NULL,NULL),
(862,'San Vicente del Caguán',1,18,NULL,NULL,NULL),
(863,'San Vicente del Chucurí',1,68,NULL,NULL,NULL),
(864,'San Zenón',1,47,NULL,NULL,NULL),
(865,'Sandoná',1,52,NULL,NULL,NULL),
(866,'Santa Ana',1,47,NULL,NULL,NULL),
(867,'Santa Bárbara',1,5,NULL,NULL,NULL),
(868,'Santa Bárbara',1,68,NULL,NULL,NULL),
(869,'Santa Bárbara (Iscuandé)',1,52,NULL,NULL,NULL),
(870,'Santa Bárbara de Pinto',1,47,NULL,NULL,NULL),
(871,'Santa Catalina',1,13,NULL,NULL,NULL),
(872,'Santa Fé de Antioquia',1,5,NULL,NULL,NULL),
(873,'Santa Genoveva de Docorodó',1,27,NULL,NULL,NULL),
(874,'Santa Helena del Opón',1,68,NULL,NULL,NULL),
(875,'Santa Isabel',1,73,NULL,NULL,NULL),
(876,'Santa Lucía',1,8,NULL,NULL,NULL),
(877,'Santa Marta',1,47,NULL,NULL,NULL),
(878,'Santa María',1,15,NULL,NULL,NULL),
(879,'Santa María',1,41,NULL,NULL,NULL),
(880,'Santa Rosa',1,13,NULL,NULL,NULL),
(881,'Santa Rosa',1,19,NULL,NULL,NULL),
(882,'Santa Rosa de Cabal',1,66,NULL,NULL,NULL),
(883,'Santa Rosa de Osos',1,5,NULL,NULL,NULL),
(884,'Santa Rosa de Viterbo',1,15,NULL,NULL,NULL),
(885,'Santa Rosa del Sur',1,13,NULL,NULL,NULL),
(886,'Santa Rosalía',1,99,NULL,NULL,NULL),
(887,'Santa Sofía',1,15,NULL,NULL,NULL),
(888,'Santana',1,15,NULL,NULL,NULL),
(889,'Santander de Quilichao',1,19,NULL,NULL,NULL),
(890,'Santiago',1,54,NULL,NULL,NULL),
(891,'Santiago',1,86,NULL,NULL,NULL),
(892,'Santo Domingo',1,5,NULL,NULL,NULL),
(893,'Santo Tomás',1,8,NULL,NULL,NULL),
(894,'Santuario',1,5,NULL,NULL,NULL),
(895,'Santuario',1,66,NULL,NULL,NULL),
(896,'Sapuyes',1,52,NULL,NULL,NULL),
(897,'Saravena',1,81,NULL,NULL,NULL),
(898,'Sardinata',1,54,NULL,NULL,NULL),
(899,'Sasaima',1,25,NULL,NULL,NULL),
(900,'Sativanorte',1,15,NULL,NULL,NULL),
(901,'Sativasur',1,15,NULL,NULL,NULL),
(902,'Segovia',1,5,NULL,NULL,NULL),
(903,'Sesquilé',1,25,NULL,NULL,NULL),
(904,'Sevilla',1,76,NULL,NULL,NULL),
(905,'Siachoque',1,15,NULL,NULL,NULL),
(906,'Sibaté',1,25,NULL,NULL,NULL),
(907,'Sibundoy',1,86,NULL,NULL,NULL),
(908,'Silos',1,54,NULL,NULL,NULL),
(909,'Silvania',1,25,NULL,NULL,NULL),
(910,'Silvia',1,19,NULL,NULL,NULL),
(911,'Simacota',1,68,NULL,NULL,NULL),
(912,'Simijaca',1,25,NULL,NULL,NULL),
(913,'Simití',1,13,NULL,NULL,NULL),
(914,'Sincelejo',1,70,NULL,NULL,NULL),
(915,'Sincé',1,70,NULL,NULL,NULL),
(916,'Sipí',1,27,NULL,NULL,NULL),
(917,'Sitionuevo',1,47,NULL,NULL,NULL),
(918,'Soacha',1,25,NULL,NULL,NULL),
(919,'Soatá',1,15,NULL,NULL,NULL),
(920,'Socha',1,15,NULL,NULL,NULL),
(921,'Socorro',1,68,NULL,NULL,NULL),
(922,'Socotá',1,15,NULL,NULL,NULL),
(923,'Sogamoso',1,15,NULL,NULL,NULL),
(924,'Solano',1,18,NULL,NULL,NULL),
(925,'Soledad',1,8,NULL,NULL,NULL),
(926,'Solita',1,18,NULL,NULL,NULL),
(927,'Somondoco',1,15,NULL,NULL,NULL),
(928,'Sonsón',1,5,NULL,NULL,NULL),
(929,'Sopetrán',1,5,NULL,NULL,NULL),
(930,'Soplaviento',1,13,NULL,NULL,NULL),
(931,'Sopó',1,25,NULL,NULL,NULL),
(932,'Sora',1,15,NULL,NULL,NULL),
(933,'Soracá',1,15,NULL,NULL,NULL),
(934,'Sotaquirá',1,15,NULL,NULL,NULL),
(935,'Sotara (Paispamba)',1,19,NULL,NULL,NULL),
(936,'Sotomayor (Los Andes)',1,52,NULL,NULL,NULL),
(937,'Suaita',1,68,NULL,NULL,NULL),
(938,'Suan',1,8,NULL,NULL,NULL),
(939,'Suaza',1,41,NULL,NULL,NULL),
(940,'Subachoque',1,25,NULL,NULL,NULL),
(941,'Sucre',1,19,NULL,NULL,NULL),
(942,'Sucre',1,68,NULL,NULL,NULL),
(943,'Sucre',1,70,NULL,NULL,NULL),
(944,'Suesca',1,25,NULL,NULL,NULL),
(945,'Supatá',1,25,NULL,NULL,NULL),
(946,'Supía',1,17,NULL,NULL,NULL),
(947,'Suratá',1,68,NULL,NULL,NULL),
(948,'Susa',1,25,NULL,NULL,NULL),
(949,'Susacón',1,15,NULL,NULL,NULL),
(950,'Sutamarchán',1,15,NULL,NULL,NULL),
(951,'Sutatausa',1,25,NULL,NULL,NULL),
(952,'Sutatenza',1,15,NULL,NULL,NULL),
(953,'Suárez',1,19,NULL,NULL,NULL),
(954,'Suárez',1,73,NULL,NULL,NULL),
(955,'Sácama',1,85,NULL,NULL,NULL),
(956,'Sáchica',1,15,NULL,NULL,NULL),
(957,'Tabio',1,25,NULL,NULL,NULL),
(958,'Tadó',1,27,NULL,NULL,NULL),
(959,'Talaigua Nuevo',1,13,NULL,NULL,NULL),
(960,'Tamalameque',1,20,NULL,NULL,NULL),
(961,'Tame',1,81,NULL,NULL,NULL),
(962,'Taminango',1,52,NULL,NULL,NULL),
(963,'Tangua',1,52,NULL,NULL,NULL),
(964,'Taraira',1,97,NULL,NULL,NULL),
(965,'Tarazá',1,5,NULL,NULL,NULL),
(966,'Tarqui',1,41,NULL,NULL,NULL),
(967,'Tarso',1,5,NULL,NULL,NULL),
(968,'Tasco',1,15,NULL,NULL,NULL),
(969,'Tauramena',1,85,NULL,NULL,NULL),
(970,'Tausa',1,25,NULL,NULL,NULL),
(971,'Tello',1,41,NULL,NULL,NULL),
(972,'Tena',1,25,NULL,NULL,NULL),
(973,'Tenerife',1,47,NULL,NULL,NULL),
(974,'Tenjo',1,25,NULL,NULL,NULL),
(975,'Tenza',1,15,NULL,NULL,NULL),
(976,'Teorama',1,54,NULL,NULL,NULL),
(977,'Teruel',1,41,NULL,NULL,NULL),
(978,'Tesalia',1,41,NULL,NULL,NULL),
(979,'Tibacuy',1,25,NULL,NULL,NULL),
(980,'Tibaná',1,15,NULL,NULL,NULL),
(981,'Tibasosa',1,15,NULL,NULL,NULL),
(982,'Tibirita',1,25,NULL,NULL,NULL),
(983,'Tibú',1,54,NULL,NULL,NULL),
(984,'Tierralta',1,23,NULL,NULL,NULL),
(985,'Timaná',1,41,NULL,NULL,NULL),
(986,'Timbiquí',1,19,NULL,NULL,NULL),
(987,'Timbío',1,19,NULL,NULL,NULL),
(988,'Tinjacá',1,15,NULL,NULL,NULL),
(989,'Tipacoque',1,15,NULL,NULL,NULL),
(990,'Tiquisio (Puerto Rico)',1,13,NULL,NULL,NULL),
(991,'Titiribí',1,5,NULL,NULL,NULL),
(992,'Toca',1,15,NULL,NULL,NULL),
(993,'Tocaima',1,25,NULL,NULL,NULL),
(994,'Tocancipá',1,25,NULL,NULL,NULL),
(995,'Toguí',1,15,NULL,NULL,NULL),
(996,'Toledo',1,5,NULL,NULL,NULL),
(997,'Toledo',1,54,NULL,NULL,NULL),
(998,'Tolú',1,70,NULL,NULL,NULL),
(999,'Tolú Viejo',1,70,NULL,NULL,NULL),
(1000,'Tona',1,68,NULL,NULL,NULL),
(1001,'Topagá',1,15,NULL,NULL,NULL),
(1002,'Topaipí',1,25,NULL,NULL,NULL),
(1003,'Toribío',1,19,NULL,NULL,NULL),
(1004,'Toro',1,76,NULL,NULL,NULL),
(1005,'Tota',1,15,NULL,NULL,NULL),
(1006,'Totoró',1,19,NULL,NULL,NULL),
(1007,'Trinidad',1,85,NULL,NULL,NULL),
(1008,'Trujillo',1,76,NULL,NULL,NULL),
(1009,'Tubará',1,8,NULL,NULL,NULL),
(1010,'Tuchín',1,23,NULL,NULL,NULL),
(1011,'Tulúa',1,76,NULL,NULL,NULL),
(1012,'Tumaco',1,52,NULL,NULL,NULL),
(1013,'Tunja',1,15,NULL,NULL,NULL),
(1014,'Tunungua',1,15,NULL,NULL,NULL),
(1015,'Turbaco',1,13,NULL,NULL,NULL),
(1016,'Turbaná',1,13,NULL,NULL,NULL),
(1017,'Turbo',1,5,NULL,NULL,NULL),
(1018,'Turmequé',1,15,NULL,NULL,NULL),
(1019,'Tuta',1,15,NULL,NULL,NULL),
(1020,'Tutasá',1,15,NULL,NULL,NULL),
(1021,'Támara',1,85,NULL,NULL,NULL),
(1022,'Támesis',1,5,NULL,NULL,NULL),
(1023,'Túquerres',1,52,NULL,NULL,NULL),
(1024,'Ubalá',1,25,NULL,NULL,NULL),
(1025,'Ubaque',1,25,NULL,NULL,NULL),
(1026,'Ubaté',1,25,NULL,NULL,NULL),
(1027,'Ulloa',1,76,NULL,NULL,NULL),
(1028,'Une',1,25,NULL,NULL,NULL),
(1029,'Unguía',1,27,NULL,NULL,NULL),
(1030,'Unión Panamericana (ÁNIMAS)',1,27,NULL,NULL,NULL),
(1031,'Uramita',1,5,NULL,NULL,NULL),
(1032,'Uribe',1,50,NULL,NULL,NULL),
(1033,'Uribia',1,44,NULL,NULL,NULL),
(1034,'Urrao',1,5,NULL,NULL,NULL),
(1035,'Urumita',1,44,NULL,NULL,NULL),
(1036,'Usiacuri',1,8,NULL,NULL,NULL),
(1037,'Valdivia',1,5,NULL,NULL,NULL),
(1038,'Valencia',1,23,NULL,NULL,NULL),
(1039,'Valle de San José',1,68,NULL,NULL,NULL),
(1040,'Valle de San Juan',1,73,NULL,NULL,NULL),
(1041,'Valle del Guamuez',1,86,NULL,NULL,NULL),
(1042,'Valledupar',1,20,NULL,NULL,NULL),
(1043,'Valparaiso',1,5,NULL,NULL,NULL),
(1044,'Valparaiso',1,18,NULL,NULL,NULL),
(1045,'Vegachí',1,5,NULL,NULL,NULL),
(1046,'Venadillo',1,73,NULL,NULL,NULL),
(1047,'Venecia',1,5,NULL,NULL,NULL),
(1048,'Venecia (Ospina Pérez)',1,25,NULL,NULL,NULL),
(1049,'Ventaquemada',1,15,NULL,NULL,NULL),
(1050,'Vergara',1,25,NULL,NULL,NULL),
(1051,'Versalles',1,76,NULL,NULL,NULL),
(1052,'Vetas',1,68,NULL,NULL,NULL),
(1053,'Viani',1,25,NULL,NULL,NULL),
(1054,'Vigía del Fuerte',1,5,NULL,NULL,NULL),
(1055,'Vijes',1,76,NULL,NULL,NULL),
(1056,'Villa Caro',1,54,NULL,NULL,NULL),
(1057,'Villa Rica',1,19,NULL,NULL,NULL),
(1058,'Villa de Leiva',1,15,NULL,NULL,NULL),
(1059,'Villa del Rosario',1,54,NULL,NULL,NULL),
(1060,'Villagarzón',1,86,NULL,NULL,NULL),
(1061,'Villagómez',1,25,NULL,NULL,NULL),
(1062,'Villahermosa',1,73,NULL,NULL,NULL),
(1063,'Villamaría',1,17,NULL,NULL,NULL),
(1064,'Villanueva',1,13,NULL,NULL,NULL),
(1065,'Villanueva',1,44,NULL,NULL,NULL),
(1066,'Villanueva',1,68,NULL,NULL,NULL),
(1067,'Villanueva',1,85,NULL,NULL,NULL),
(1068,'Villapinzón',1,25,NULL,NULL,NULL),
(1069,'Villarrica',1,73,NULL,NULL,NULL),
(1070,'Villavicencio',1,50,NULL,NULL,NULL),
(1071,'Villavieja',1,41,NULL,NULL,NULL),
(1072,'Villeta',1,25,NULL,NULL,NULL),
(1073,'Viotá',1,25,NULL,NULL,NULL),
(1074,'Viracachá',1,15,NULL,NULL,NULL),
(1075,'Vista Hermosa',1,50,NULL,NULL,NULL),
(1076,'Viterbo',1,17,NULL,NULL,NULL),
(1077,'Vélez',1,68,NULL,NULL,NULL),
(1078,'Yacopí',1,25,NULL,NULL,NULL),
(1079,'Yacuanquer',1,52,NULL,NULL,NULL),
(1080,'Yaguará',1,41,NULL,NULL,NULL),
(1081,'Yalí',1,5,NULL,NULL,NULL),
(1082,'Yarumal',1,5,NULL,NULL,NULL),
(1083,'Yolombó',1,5,NULL,NULL,NULL),
(1084,'Yondó (Casabe)',1,5,NULL,NULL,NULL),
(1085,'Yopal',1,85,NULL,NULL,NULL),
(1086,'Yotoco',1,76,NULL,NULL,NULL),
(1087,'Yumbo',1,76,NULL,NULL,NULL),
(1088,'Zambrano',1,13,NULL,NULL,NULL),
(1089,'Zapatoca',1,68,NULL,NULL,NULL),
(1090,'Zapayán (PUNTA DE PIEDRAS)',1,47,NULL,NULL,NULL),
(1091,'Zaragoza',1,5,NULL,NULL,NULL),
(1092,'Zarzal',1,76,NULL,NULL,NULL),
(1093,'Zetaquirá',1,15,NULL,NULL,NULL),
(1094,'Zipacón',1,25,NULL,NULL,NULL),
(1095,'Zipaquirá',1,25,NULL,NULL,NULL),
(1096,'Zona Bananera (PRADO - SEVILLA)',1,47,NULL,NULL,NULL),
(1097,'Ábrego',1,54,NULL,NULL,NULL),
(1098,'Íquira',1,41,NULL,NULL,NULL),
(1099,'Úmbita',1,15,NULL,NULL,NULL),
(1100,'Útica',1,25,NULL,NULL,NULL);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `servientrega` */

DROP TABLE IF EXISTS `servientrega`;

CREATE TABLE `servientrega` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clase` varchar(200) DEFAULT NULL,
  `regional` varchar(200) DEFAULT NULL,
  `states_id` int(11) DEFAULT NULL,
  `cities_id` int(11) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `direccion_completa` varchar(255) DEFAULT NULL,
  `lat` varchar(200) DEFAULT NULL,
  `lon` varchar(200) DEFAULT NULL,
  `telefono` varchar(200) DEFAULT NULL,
  `idcl` varchar(200) DEFAULT NULL,
  `point` point DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=353 DEFAULT CHARSET=utf8;

/*Data for the table `servientrega` */

insert  into `servientrega`(`id`,`clase`,`regional`,`states_id`,`cities_id`,`nombre`,`direccion`,`direccion_completa`,`lat`,`lon`,`telefono`,`idcl`,`point`) values 
(1,'DIRECTO','ANTIOQUIA',1,1,'NUEVO APARTADO','CALLE 98 NO 97 - 45','ANTIOQUIA APARTADO CALLE 98 NO 97 - 45 ','7.8816913','-76.630550','2391503','1','\0\0\0\0\0\0\0���چ@�c�Z(S�'),
(2,'DIRECTO','ANTIOQUIA',1,4,'BELLO MUNICIPIO','CLL 49 # 49 - 55','ANTIOQUIA BELLO CLL 49 # 49 - 55 ','6.3337926','-75.55799189999990','2726695','1','\0\0\0\0\0\0\0U�2��U@�{�#��R�'),
(3,'DIRECTO','ANTIOQUIA',1,7,'SABANETA MAYORCA','CARRERA 48 A NO 50 SUR - 106','ANTIOQUIA SABANETA CARRERA 48 A NO 50 SUR - 106 ','6.1541334','-75.61707129999990',' 2779377 - 3173673824','1','\0\0\0\0\0\0\0��`%՝@03�~�R�'),
(4,'DIRECTO','ANTIOQUIA',1,9,'AVDA.ORIENTAL','CRA 46 # 53 - 48 LC 103','ANTIOQUIA MEDELLIN CRA 46 # 53 - 48 LC 103 ','6.212844','-75.57530109999990','5128100','1','\0\0\0\0\0\0\0k����@�v����R�'),
(5,'DIRECTO','ANTIOQUIA',1,9,'AV FERROCARRIL','CRA 57 # 48 -95','ANTIOQUIA MEDELLIN CRA 57 # 48 -95 ','6.250960399999990','-75.57520819999990','5116940','1','\0\0\0\0\0\0\08]Z��\0@\0�\r6��R�'),
(6,'DIRECTO','ANTIOQUIA',1,9,'SAN JUAN','CLL 44 # 79 - 19','ANTIOQUIA MEDELLIN CLL 44 # 79 - 19 ','6.250223399999990','-75.59771700000000','2509668','1','\0\0\0\0\0\0\0���:\0@����@�R�'),
(7,'DIRECTO','ANTIOQUIA',1,9,'TERMINAL NORTE','CRA 64C # 78 - 660 L 661','ANTIOQUIA MEDELLIN CRA 64C # 78 - 660 L 661 ','6.279301','-75.57121','2579745','1','\0\0\0\0\0\0\0���@{Ic���R�'),
(8,'DIRECTO','ANTIOQUIA',1,9,'AEROPUERTO OLAYA HERRERA','CRA 65 A NO 13-57 TERMINAL DE CARGA LOCAL 1025 ','ANTIOQUIA MEDELLIN CRA 65 A NO 13-57 TERMINAL DE CARGA LOCAL 1025  ','6.21844','-75.58672','3613738','1','\0\0\0\0\0\0\0��@���@/�\nҌ�R�'),
(9,'DIRECTO','ANTIOQUIA',1,9,'POBLADO PATIO BONITO','CRA 47 # 7 - 72','ANTIOQUIA MEDELLIN CRA 47 # 7 - 72 ','6.2094922','-75.5762632','3522286','1','\0\0\0\0\0\0\0����@�x��R�'),
(10,'DIRECTO','ANTIOQUIA',1,9,'EL RODEO (CARULLA CRISTO REY)','CLL 6 SUR # 52 - 92 LC 21','ANTIOQUIA MEDELLIN CLL 6 SUR # 52 - 92 LC 21 ','6.20443','-75.58763','2558639','1','\0\0\0\0\0\0\0�HV�@�	ܺ��R�'),
(11,'DIRECTO','ANTIOQUIA',1,9,'HOME CENTER LOS MOLINOS','CRA 82 CLL 32 ESQUINA LC 1200','ANTIOQUIA MEDELLIN CRA 82 CLL 32 ESQUINA LC 1200 ','6.23413','-75.60414',' 1 - 3436172','1','\0\0\0\0\0\0\0�Tƿ�@&��:��R�'),
(12,'DIRECTO','ANTIOQUIA',1,9,'BELEN II','CRA 77 # 28 - 22','ANTIOQUIA MEDELLIN CRA 77 # 28 - 22 ','6.229583','-75.5968927','3472624','1','\0\0\0\0\0\0\0��R��@\'np}3�R�'),
(13,'DIRECTO','ANTIOQUIA',1,9,'BARRIO COLOMBIA II','CRA 45 NO  28 - 10','ANTIOQUIA MEDELLIN CRA 45 NO  28 - 10 ','6.228908','-75.57243679999990','2326401','1','\0\0\0\0\0\0\0�+��f�@���͢�R�'),
(14,'DIRECTO','ANTIOQUIA',1,9,'ESTACION HOSPITAL','CALLE 66 N 50C-50','ANTIOQUIA MEDELLIN CALLE 66 N 50C-50 ','6.263197','-75.562966',NULL,'1','\0\0\0\0\0\0\0\n����\r@�����R�'),
(15,'DIRECTO','ANTIOQUIA',1,9,'BASE MILITAR DEL HUECO','CALLE 46 N 55-18','ANTIOQUIA MEDELLIN CALLE 46 N 55-18 ','6.248461','-75.5737471',NULL,'1','\0\0\0\0\0\0\0iSu�l�@1��E��R�'),
(16,'DIRECTO','ANTIOQUIA',1,9,'POPULAR','CRA 42 B NO 107 - 52 ','ANTIOQUIA MEDELLIN CRA 42 B NO 107 - 52  ','6.2956243','-75.5479636','5724691','1','\0\0\0\0\0\0\0���\"�.@�Y���R�'),
(17,'DIRECTO','ANTIOQUIA',1,9,'ACOPIO SECTOR GUAYABAL','CL 10 SUR NO. 51 - 59','ANTIOQUIA MEDELLIN CL 10 SUR NO. 51 - 59 ','6.1986837','-75.58759529999990','2859870','1','\0\0\0\0\0\0\0<�f�s�@2Q)��R�'),
(18,'DIRECTO','ANTIOQUIA',1,9,'SAN DIEGO PLAZUELAS II','CRA 43 # 33 - 57 LC 184','ANTIOQUIA MEDELLIN CRA 43 # 33 - 57 LC 184 ','6.2261423','-75.56847499999990','2612905','1','\0\0\0\0\0\0\0	��ؑ�@�e��a�R�'),
(19,'DIRECTO','ANTIOQUIA',1,9,'INDUSTRIALES','CRA 48 # 18 - 47','ANTIOQUIA MEDELLIN CRA 48 # 18 - 47 ','6.2202157','-75.57571879999990',' 3119618 - 3549000','1','\0\0\0\0\0\0\0SCv9��@Il����R�'),
(20,'DIRECTO','ANTIOQUIA',1,9,'PARQUE BERRIO CRA 50','CRA 50 # 51 - 13','ANTIOQUIA MEDELLIN CRA 50 # 51 - 13 ','6.2053505','-75.5797528',' 2932686 - 3549000','1','\0\0\0\0\0\0\0v��fG�@��|�\Z�R�'),
(21,'DIRECTO','ANTIOQUIA',1,9,'LA SETENTA Y SEIS','CRA 76 # 33A - 83','ANTIOQUIA MEDELLIN CRA 76 # 33A - 83 ','6.240190399999990','-75.5968263',' 4110255 - 4110312','1','\0\0\0\0\0\0\0�@�x��@\"��f2�R�'),
(22,'DIRECTO','ANTIOQUIA',1,9,'POBLADO INTERNACIONAL','CRA 43A # 23 - 44','ANTIOQUIA MEDELLIN CRA 43A # 23 - 44 ','6.2495666','-75.56149940000000','2629720','1','\0\0\0\0\0\0\0�c��@P�-���R�'),
(23,'DIRECTO','ANTIOQUIA',1,9,'HOME CENTER SAN JUAN','CLL 44 # 65 - 100','ANTIOQUIA MEDELLIN CLL 44 # 65 - 100 ','6.2488071','-75.584277','4342815','1','\0\0\0\0\0\0\0��I��@��[�d�R�'),
(24,'DIRECTO','ANTIOQUIA',1,9,'ESTACION ALPUJARRA','CRA 52 # 43- 31 LOC 113','ANTIOQUIA MEDELLIN CRA 52 # 43- 31 LOC 113 ','6.24427','-75.57192',' 8390647 - 8390671','1','\0\0\0\0\0\0\0`�5�!�@j�WV��R�'),
(25,'DIRECTO','ANTIOQUIA',1,9,'COLOMBIA CLL 50','CLL 50 # 65 -91','ANTIOQUIA MEDELLIN CLL 50 # 65 -91 ','6.2564955','-75.5817558','2301475','1','\0\0\0\0\0\0\0�H���@�Э|;�R�'),
(26,'DIRECTO','ANTIOQUIA',1,11,'RIONEGRO PARQUE','CRA 50 # 45 - 30 ','ANTIOQUIA RIONEGRO (ANT) CRA 50 # 45 - 30  ','6.1714399','-75.42827',' 5620116 - 5627819','1','\0\0\0\0\0\0\0�����@����h�R�'),
(27,'DIRECTO','ANTIOQUIA',1,1,'APARTADO','CLL 98 # 99 -35 ','ANTIOQUIA APARTADO CLL 98 # 99 -35  ','7.882178699999990','-76.632026',' 8280645 - 8282196 - 8283533 - 8284777','1','\0\0\0\0\0\0\08�f�Y�@0.s(S�'),
(28,'DIRECTO','ANTIOQUIA',10,14,'MONTELIBANO','CRA 4 # 16 - 22','CORDOBA MONTELIBANO CRA 4 # 16 - 22 ','7.983001000000000','-75.4234546','7722799','1','\0\0\0\0\0\0\0��kЗ�@ϕR��R�'),
(29,'DIRECTO','ANTIOQUIA',1,4,'PUERTA DEL NORTE','DG 55 # 37 - 41 LOCAL 151','ANTIOQUIA BELLO DG 55 # 37 - 41 LOCAL 151 ','6.33939','-75.54583','4816077','1','\0\0\0\0\0\0\0�[Z\r�[@>�����R�'),
(30,'DIRECTO','ANTIOQUIA',1,3,'PUERTO BERRIO II','CLL 53 # 4 - 25','ANTIOQUIA PUERTO BERRIO CLL 53 # 4 - 25 ','6.4902015','-74.40116619999990','8332188','1','\0\0\0\0\0\0\0���a��@�P����R�'),
(31,'DIRECTO','ANTIOQUIA',1,6,'CHIGORODO','CLL 97 # 103 - 76','ANTIOQUIA CHIGORODO CLL 97 # 103 - 76 ','7.669906199999990','-76.6829166','3182616967','1','\0\0\0\0\0\0\0����@P���+S�'),
(32,'DIRECTO','ANTIOQUIA',1,8,'LA CEJA','CLL 19 # 22 - 15/19 LC 3 Y 4','ANTIOQUIA LA CEJA CLL 19 # 22 - 15/19 LC 3 Y 4 ','6.03109','-75.43265','5537701','1','\0\0\0\0\0\0\09��@R\'����R�'),
(33,'DIRECTO','ANTIOQUIA',1,10,'ITAGUI STA. MARIA - MUNICIPIO','CLL 81 #52DD - 12  LC 153','ANTIOQUIA ITAGUI CLL 81 #52DD - 12  LC 153 ','6.190030999999990','-75.59492159999990',' 2850562 - 3618350','1','\0\0\0\0\0\0\0��|��@��2�R�'),
(34,'DIRECTO','ANTIOQUIA',1,12,'TURBO OF PPAL','CLL 101 # 14B - 34','ANTIOQUIA TURBO CLL 101 # 14B - 34 ','8.0880534','-76.7419376','8277434','1','\0\0\0\0\0\0\0l��U- @���{/S�'),
(35,'DIRECTO','ANTIOQUIA',1,2,'ENVIGADO MUNICIPIO II','CRA 43 A CLL 33 B SUR - 4','ANTIOQUIA ENVIGADO CRA 43 A CLL 33 B SUR - 4 ','6.1698194','-75.5881838','2711258','1','\0\0\0\0\0\0\0��#�@^B�ͤ�R�'),
(36,'DIRECTO','ANTIOQUIA',1,10,'ITAGUI MAYORISTA','CLL 84 A # 47 - 50 BQ 22 LC 7','ANTIOQUIA ITAGUI CLL 84 A # 47 - 50 BQ 22 LC 7 ','6.1883556','-75.58857689999990',' 3619121 - 3719160 - 3738135','1','\0\0\0\0\0\0\0�XJ��@�+r>��R�'),
(37,'DIRECTO','ANTIOQUIA',1,2,'ENVIGADO PARQUE','CRA 42 # 38 A SUR - 23','ANTIOQUIA ENVIGADO CRA 42 # 38 A SUR - 23 ','6.179833800000000','-75.5788281','2702026','1','\0\0\0\0\0\0\0��Z&�@_���R�'),
(38,'DIRECTO','ANTIOQUIA',1,9,'AVDA. 33','CLL 33 # 65 B - 87  PISO 1','ANTIOQUIA MEDELLIN CLL 33 # 65 B - 87  PISO 1 ','6.2948502','-75.5428735','2654330','1','\0\0\0\0\0\0\0Q��5�-@]~p��R�'),
(39,'DIRECTO','ANTIOQUIA',1,9,'POBLADO PARQUE','CRA 43A # 9 -12','ANTIOQUIA MEDELLIN CRA 43A # 9 -12 ','6.265691500000000','-75.5526388','2664405','1','\0\0\0\0\0\0\0�L�n@ !o^�R�'),
(40,'DIRECTO','ANTIOQUIA',1,9,'NUTIBARA','CL 53 NO 20 - 21','ANTIOQUIA MEDELLIN CL 53 NO 20 - 21 ','6.2390602','-75.5443324','5114877','1','\0\0\0\0\0\0\0�s2��@�	�W��R�'),
(41,'DIRECTO','ANTIOQUIA',1,9,'LA SETENTA','CIRCULAR 2 # 70 - 50','ANTIOQUIA MEDELLIN CIRCULAR 2 # 70 - 50 ','6.2452575','-75.58971679999990','4113487','1','\0\0\0\0\0\0\0e6�$�@�y���R�'),
(42,'DIRECTO','ANTIOQUIA',1,9,'BUENOS AIRES CLL 49','CLL 49 # 36 - 40 LC B','ANTIOQUIA MEDELLIN CLL 49 # 36 - 40 LC B ','6.2150577','-75.5772381','2284738','1','\0\0\0\0\0\0\0\r�8�@U`x��R�'),
(43,'DIRECTO','ANTIOQUIA',1,9,'LOS COLORES II','CRA 80 NO 54 - 35 PISO 1 ','ANTIOQUIA MEDELLIN CRA 80 NO 54 - 35 PISO 1  ','6.268524999999990','-75.5956412','4214750','1','\0\0\0\0\0\0\0(��7�@��D��R�'),
(44,'DIRECTO','ANTIOQUIA',1,11,'ZONA FRANCA RIONEGRO','ZONA FRANCA INDUSTRIAL RIONEGRO LC 108 C','ANTIOQUIA RIONEGRO (ANT) ZONA FRANCA INDUSTRIAL RIONEGRO LC 108 C ','6.157171','-75.416263','5619520','1','\0\0\0\0\0\0\0��Co�@;�\r��R�'),
(45,'DIRECTO','ANTIOQUIA',1,5,'CAUCASIA OF PPAL','CRA 3 # 17 - 75','ANTIOQUIA CAUCASIA CRA 3 # 17 - 75 ','7.9862678','-75.1930747',' 8390647 - 8390671','1','\0\0\0\0\0\0\08d�/��@׋�U[�R�'),
(46,'DIRECTO','ANTIOQUIA',1,10,'ITAGUI PARQUE','CRA 49 # 51 - 47','ANTIOQUIA ITAGUI CRA 49 # 51 - 47 ','6.171616999999990','-75.6081514',' 2773344 - 3723943','1','\0\0\0\0\0\0\0��]��@�����R�'),
(47,'DIRECTO','ANTIOQUIA',1,9,'CONSUMO FLORESTA','CRA 84 # 45F - 12','ANTIOQUIA MEDELLIN CRA 84 # 45F - 12 ','6.2559537','-75.60100419999990','4135980','1','\0\0\0\0\0\0\0))�@��Q�v�R�'),
(48,'DIRECTO','ANTIOQUIA',1,9,'UNICENTRO CONQUISTADORES','CLL 34 B # 66 A - 10','ANTIOQUIA MEDELLIN CLL 34 B # 66 A - 10 ','6.2295634','-75.5699498','2650396','1','\0\0\0\0\0\0\0,o���@\'׹z�R�'),
(49,'DIRECTO','ANTIOQUIA',1,9,'LA PLAYA','CLL 51 # 42 -19','ANTIOQUIA MEDELLIN CLL 51 # 42 -19 ','6247789399999990','-75.5619115','2391503','1','\0\0\0\0\0\0\0����T26C��[��R�'),
(50,'DIRECTO','ANTIOQUIA',1,9,'PALACE','CRA 50 # 35 - 78','ANTIOQUIA MEDELLIN CRA 50 # 35 - 78 ','6.2368584','-75.5730147','2629489','1','\0\0\0\0\0\0\0�!\'��@%(�E��R�'),
(51,'DIRECTO','ANTIOQUIA',1,9,'VERACRUZ','CRA 52 # 51A - 29','ANTIOQUIA MEDELLIN CRA 52 # 51A - 29 ','6.2511963','-75.5691978',' 5718172 - 5719054','1','\0\0\0\0\0\0\0<�U�9@!���m�R�'),
(52,'DIRECTO','ANTIOQUIA',1,9,'HOLLIWOOD II','CLL 48 # 51A-21 LC 287 CC HOLLIWOOD','ANTIOQUIA MEDELLIN CLL 48 # 51A-21 LC 287 CC HOLLIWOOD ','6.24813','-75.56990','2318350','1','\0\0\0\0\0\0\0<�l��@c�=y�R�'),
(53,'DIRECTO','ANTIOQUIA',1,9,'EST. PARQUE EL BERRIO','CRA 51 # 50 - 22','ANTIOQUIA MEDELLIN CRA 51 # 50 - 22 ','6.2499452','-75.5685757','5138023','1','\0\0\0\0\0\0\01+o���@83U�c�R�'),
(54,'DIRECTO','ANTIOQUIA',1,9,'ALHAMBRA','CRA 52A # 45 - 02','ANTIOQUIA MEDELLIN CRA 52A # 45 - 02 ','6.2461528','-75.5710904','5118869','1','\0\0\0\0\0\0\0{F�z�@�ÿ���R�'),
(55,'DIRECTO','ANTIOQUIA',1,9,'SECTOR CARIBE ACOPIO NORTE','CR 65 NO 78 - 49','ANTIOQUIA MEDELLIN CR 65 NO 78 - 49 ','6.2757233','-75.5737669','2578415','1','\0\0\0\0\0\0\0�p5W\Z@��͘��R�'),
(56,'DIRECTO','ANTIOQUIA',1,11,'RIONEGRO','CRA 52 # 47 - 35','ANTIOQUIA RIONEGRO (ANT) CRA 52 # 47 - 35 ','6.1714399','-75.42827',' 5312626 - 5623433','1','\0\0\0\0\0\0\0�����@����h�R�'),
(57,'DIRECTO','ANTIOQUIA',1,5,'CAUCASIA TRONCAL','CRA 20 # 11-64','ANTIOQUIA CAUCASIA CRA 20 # 11-64 ','7.9817819','-75.202067',' 8391123 - 8392536','1','\0\0\0\0\0\0\08<X�@t&m���R�'),
(58,'DIRECTO','BOGOTA',11,19,'ALAMOS','TRANSV 93 # 64 - 47','CUNDINAMARCA BOGOTA TRANSV 93 # 64 - 47 ','4.6901157','-74.117407','4308148','1','\0\0\0\0\0\0\0ӧ���@�T����R�'),
(59,'DIRECTO','BOGOTA',11,19,'AV ROJAS','CRA 70 # 68A - 37','CUNDINAMARCA BOGOTA CRA 70 # 68A - 37 ','4.680194','-74.094248','4363621','1','\0\0\0\0\0\0\0ལƄ�@�m�(�R�'),
(60,'DIRECTO','BOGOTA',11,19,'NQS EL CAMPIN','CR 30 NO 53A-89','CUNDINAMARCA BOGOTA CR 30 NO 53A-89 ','4.451861','-74.0790899','7034594','1','\0\0\0\0\0\0\0�Ye���@i|��R�'),
(61,'DIRECTO','BOGOTA',11,19,'PARQUE SANTANDER 2','CL 17 # 7 90','CUNDINAMARCA BOGOTA CL 17 # 7 90 ','4.604229','-74.0730146',' 2814285 - 3414165','1','\0\0\0\0\0\0\0�-��j@Z�mE��R�'),
(62,'DIRECTO','BOGOTA',11,19,'NQS CALLE 8','AV CRA 30 # 8 10/22 LC 2','CUNDINAMARCA BOGOTA AV CRA 30 # 8 10/22 LC 2 ','4.60847','-74.09624','2016144','1','\0\0\0\0\0\0\0scz�o@J$��(�R�'),
(63,'DIRECTO','BOGOTA',11,19,'PALOQUEMAO CLL 22','CLL 22 # 19A - 27','CUNDINAMARCA BOGOTA CLL 22 # 19A - 27 ','4.615335099999990','-74.0810236','3377763','1','\0\0\0\0\0\0\0�R�g\Zv@\r�}/�R�'),
(64,'DIRECTO','BOGOTA',11,19,'GALERIAS CRA 24','CRA 24 # 51 - 29','CUNDINAMARCA BOGOTA CRA 24 # 51 - 29 ','4.597503200000000','-74.09552599999990','2556760','1','\0\0\0\0\0\0\0z���c@�z�R�'),
(65,'DIRECTO','BOGOTA',11,19,'EDUARDO SANTOS','AV CLL 3 # 20 A - 75 LC 8','CUNDINAMARCA BOGOTA AV CLL 3 # 20 A - 75 LC 8 ','4.59791','-74.09311',' 2805620 - 4661448','1','\0\0\0\0\0\0\01�߄Bd@�;����R�'),
(66,'DIRECTO','BOGOTA',11,19,'VENECIA CRA 54','AV CRA 54 # 45 A - 19/21 SUR LC 104','CUNDINAMARCA BOGOTA AV CRA 54 # 45 A - 19/21 SUR LC 104 ','4.59481','-74.11129','7282616','1','\0\0\0\0\0\0\0�Ue�a@̗`�R�'),
(67,'DIRECTO','BOGOTA',11,19,'NIZA II','TRANSV 60 # 125 - 29 LC 3','CUNDINAMARCA BOGOTA TRANSV 60 # 125 - 29 LC 3 ','4.70890','-74.07193','6392386','1','\0\0\0\0\0\0\0������@{fI���R�'),
(68,'DIRECTO','BOGOTA',11,19,'AVENIDA 19','AV 19 # 4 - 37 L 5','CUNDINAMARCA BOGOTA AV 19 # 4 - 37 L 5 ','4.7102604','-74.2023409',' 2431769 - 2431853','1','\0\0\0\0\0\0\0U���N�@*	?\'�R�'),
(69,'DIRECTO','BOGOTA',11,19,'CALLE 66','CRA 7 # 66 - 40','CUNDINAMARCA BOGOTA CRA 7 # 66 - 40 ','4.6502723','-74.05832',' 3464137 - 6407420','1','\0\0\0\0\0\0\0��W���@�,σ��R�'),
(70,'DIRECTO','BOGOTA',11,19,'CEDRITOS','AV 19 # 142 - 46','CUNDINAMARCA BOGOTA AV 19 # 142 - 46 ','4.724577','-74.04591099999990',' 6254814 - 6254855','1','\0\0\0\0\0\0\04�Y���@��4��R�'),
(71,'DIRECTO','BOGOTA',11,19,'CHICO','CRA 11 # 92 - 30 LC 102','CUNDINAMARCA BOGOTA CRA 11 # 92 - 30 LC 102 ','4.67361','-74.04786',' 2183929 - 6108451','1','\0\0\0\0\0\0\0���Ʊ@W�c#�R�'),
(72,'DIRECTO','BOGOTA',11,19,'GALERIAS','CLL 53 # 27 - 28','CUNDINAMARCA BOGOTA CLL 53 # 27 - 28 ','4.642399999999990','-74.0762562','3101667','1','\0\0\0\0\0\0\0*�;Nё@�G�a�R�'),
(73,'DIRECTO','BOGOTA',11,19,'MARLY','CRA 13 # 50 - 93','CUNDINAMARCA BOGOTA CRA 13 # 50 - 93 ','4.6202533','-74.0684438','2359337','1','\0\0\0\0\0\0\0��Z�#{@K�\Zba�R�'),
(74,'DIRECTO','BOGOTA',11,19,'AV 1DE MAYO','CLL 22 SUR # 24 F - 08 LC 6','CUNDINAMARCA BOGOTA CLL 22 SUR # 24 F - 08 LC 6 ','4.58640','-74.10674','2724718','1','\0\0\0\0\0\0\0c�=yX@4K�ԆR�'),
(75,'DIRECTO','BOGOTA',11,19,'CIUDAD JARDIN DEL SUR','CRA 10 #17 - 06 SUR','CUNDINAMARCA BOGOTA CRA 10 #17 - 06 SUR ','4.577995','-74.092641','2724739','1','\0\0\0\0\0\0\0����O@3Q���R�'),
(76,'DIRECTO','BOGOTA',11,19,'VEINTE DE JULIO','CLL 25 SUR # 6 - 39','CUNDINAMARCA BOGOTA CLL 25 SUR # 6 - 39 ','4.5704559','-74.0940336',' 2728273 - 2780377','1','\0\0\0\0\0\0\0�=i�%H@i.���R�'),
(77,'DIRECTO','BOGOTA',11,19,'EXITO CLLE 80','CRA 59 # 79 - 30','CUNDINAMARCA BOGOTA CRA 59 # 79 - 30 ','4.720694','-74.122928','2509235','1','\0\0\0\0\0\0\0������@��f\rއR�'),
(78,'DIRECTO','BOGOTA',11,19,'CALLE 134','AV CLL 134 # 19 - 50','CUNDINAMARCA BOGOTA AV CLL 134 # 19 - 50 ','4.7177437','-74.0476781','6261023','1','\0\0\0\0\0\0\0�Y4��@r(\r�R�'),
(79,'DIRECTO','BOGOTA',11,19,'AVENIDA 15','AV 15 # 106 - 43','CUNDINAMARCA BOGOTA AV 15 # 106 - 43 ','4.691060999999990','-74.04587599999990','2154586','1','\0\0\0\0\0\0\0�)�~��@����R�'),
(80,'DIRECTO','BOGOTA',11,19,'AV AMERICAS CON 59','AV AMERICAS # 59 - 11','CUNDINAMARCA BOGOTA AV AMERICAS # 59 - 11 ','4.627692','-74.11552499999990','2600118','1','\0\0\0\0\0\0\0�����@�7��d�R�'),
(81,'DIRECTO','BOGOTA',11,19,'ZONA FRANCA CRA 106','CRA 106 # 15 - 25','CUNDINAMARCA BOGOTA CRA 106 # 15 - 25 ','4.672109','-74.155589','4396551','1','\0\0\0\0\0\0\0�cyW=�@���+��R�'),
(82,'DIRECTO','BOGOTA',11,19,'MARTIRES II','CLL 10  # 13 - 16','CUNDINAMARCA BOGOTA CLL 10  # 13 - 16 ','4.6008444','-74.0812353','3425078','1','\0\0\0\0\0\0\0`��Cg@�1��2�R�'),
(83,'DIRECTO','BOGOTA',11,19,'DORADO PLAZA II','CRA 85 C # 25 F - 33 LC 1 Y 3','CUNDINAMARCA BOGOTA CRA 85 C # 25 F - 33 LC 1 Y 3 ','4.67477','-74.12068',' 4105322 - 4124005','1','\0\0\0\0\0\0\0�I)���@\0R�8��R�'),
(84,'DIRECTO','BOGOTA',11,19,'C.CIAL HAYUELOS','CLL 20 # 82 - 52 LC 164 B','CUNDINAMARCA BOGOTA CLL 20 # 82 - 52 LC 164 B ','4.66262','-74.13085',' 3569526 - 4148736','1','\0\0\0\0\0\0\0��vۅ�@����_�R�'),
(85,'DIRECTO','BOGOTA',11,19,'AV BOYACA II','CRA 72 # 73 A - 55 PISO 1 LC 1','CUNDINAMARCA BOGOTA CRA 72 # 73 A - 55 PISO 1 LC 1 ','4.689528','-74.09384899999990','2237400','1','\0\0\0\0\0\0\0��Ơ�@�p<��R�'),
(86,'DIRECTO','BOGOTA',11,19,'CALLE 140 CON 15','CLL 140 # 15 - 32','CUNDINAMARCA BOGOTA CLL 140 # 15 - 32 ','4.7215618','-74.0418304','6151081','1','\0\0\0\0\0\0\0�+���@��iY��R�'),
(87,'DIRECTO','BOGOTA',11,19,'EXITO CHAPINERO','CLL 52 # 13 - 10','CUNDINAMARCA BOGOTA CLL 52 # 13 - 10 ','4.6391864','-74.0653416','2115108','0','\0\0\0\0\0\0\0 10ᆎ@_Ĉ�.�R�'),
(88,'DIRECTO','BOGOTA',11,19,'LA CABRERA','CRA 15 # 86A - 71','CUNDINAMARCA BOGOTA CRA 15 # 86A - 71 ','4.6721899','-74.054532',' 2367405 - 2367615','1','\0\0\0\0\0\0\0YΔ�R�@x%�s}�R�'),
(89,'DIRECTO','BOGOTA',11,19,'EXITO BOSA','CLL 65 SUR # 78H - 54','CUNDINAMARCA BOGOTA CLL 65 SUR # 78H - 54 ','4.6047582','-74.18355230000000','7775252','0','\0\0\0\0\0\0\0��˻Ek@�f%R��R�'),
(90,'DIRECTO','BOGOTA',11,19,'COUNTRY 2','CLL 79 # 15 - 22','CUNDINAMARCA BOGOTA CLL 79 # 15 - 22 ','4.6655122','-74.05784609999990',' 6359180 - 6918941','1','\0\0\0\0\0\0\0&[�|�@�� ���R�'),
(91,'DIRECTO','BOGOTA',11,19,'SIETE DE AGOSTO CRA 24','CRA 24 # 66A - 45','CUNDINAMARCA BOGOTA CRA 24 # 66A - 45 ','4.658122','-74.070358','6310886','1','\0\0\0\0\0\0\0�˻�@�@׾��R�'),
(92,'DIRECTO','BOGOTA',11,19,'PORTAL DE LA 80','AV CLL 80 # 100 - 52 L 2 - 122','CUNDINAMARCA BOGOTA AV CLL 80 # 100 - 52 L 2 - 122 ','4.6977003','-74.09272349999990','2284932','1','\0\0\0\0\0\0\07���q�@�.�R�'),
(93,'DIRECTO','BOGOTA',11,19,'CLL 72 CON 12','CLL 72 # 12 - 41 L 3','CUNDINAMARCA BOGOTA CLL 72 # 12 - 41 L 3 ','4.657848','-74.060373',' 2550536 - 5453522','1','\0\0\0\0\0\0\0���碡@�#�&݃R�'),
(94,'DIRECTO','BOGOTA',11,19,'SUBA','CRA 92 # 145 A - 03 L 2','CUNDINAMARCA BOGOTA CRA 92 # 145 A - 03 L 2 ','4.741776199999990','-74.0847134','6825202','1','\0\0\0\0\0\0\0��.��@���k�R�'),
(95,'DIRECTO','BOGOTA',11,19,'AV CALLE SEXTA CON 19','AV 6 # 19A - 46','CUNDINAMARCA BOGOTA AV 6 # 19A - 46 ','4.601276','-74.090407','3602772','1','\0\0\0\0\0\0\0�zO�g@q:ɅR�'),
(96,'DIRECTO','BOGOTA',11,19,'CHICO CLL 97','CRA 15 # 97 - 67 L 104','CUNDINAMARCA BOGOTA CRA 15 # 97 - 67 L 104 ','4.682802199999990','-74.04886359999990',' 6213795 - 6213806','1','\0\0\0\0\0\0\0��0�@��ʔ �R�'),
(97,'DIRECTO','BOGOTA',11,19,'TIMON','CRA  71 # 21 - 91','CUNDINAMARCA BOGOTA CRA  71 # 21 - 91 ','4.6544203','-74.1211254','4245643','1','\0\0\0\0\0\0\0�Z�Z �@������R�'),
(98,'DIRECTO','BOGOTA',11,19,'AV CHILE','CRA 7 # 70A - 08','CUNDINAMARCA BOGOTA CRA 7 # 70A - 08 ','4.6535854','-74.05587299999990','2354605','1','\0\0\0\0\0\0\0���}E�@��Xl��R�'),
(99,'DIRECTO','BOGOTA',11,19,'KENNEDY','CLL 37 SUR # 78H - 28','CUNDINAMARCA BOGOTA CLL 37 SUR # 78H - 28 ','4.6232841','-74.15254139999990',' 2730489 - 4547954','1','\0\0\0\0\0\0\0�w�/>~@H=ÉR�'),
(100,'DIRECTO','BOGOTA',11,19,'ANDES','CRA 62 # 99 - 80 L 102','CUNDINAMARCA BOGOTA CRA 62 # 99 - 80 L 102 ','4.689132','-74.067285',' 2263896 - 6133234','1','\0\0\0\0\0\0\0��ѫ�@���eN�R�'),
(101,'DIRECTO','BOGOTA',11,19,'CORFERIAS','CRA 37 # 24 - 67 B/LLON 3','CUNDINAMARCA BOGOTA CRA 37 # 24 - 67 B/LLON 3 ','4.62918','-74.08920',' 3680720 - 3810000','1','\0\0\0\0\0\0\0j0\r�G�@h��s��R�'),
(102,'DIRECTO','BOGOTA',11,19,'UNICENTRO','AV 15 # 119 - 57','CUNDINAMARCA BOGOTA AV 15 # 119 - 57 ','4.699758','-74.043302',' 6120567 - 6120747','1','\0\0\0\0\0\0\0ut\\��@}v�ułR�'),
(103,'DIRECTO','BOGOTA',11,19,'PARQUE CENTRAL BAVARIA','CLL 28 #13 - 08 L17','CUNDINAMARCA BOGOTA CLL 28 #13 - 08 L17 ','4.6160651','-74.07083759999990','2853345','1','\0\0\0\0\0\0\0����v@��m���R�'),
(104,'DIRECTO','BOGOTA',11,19,'KENNEDY 2','CLL 36 SUR # 73D - 55','CUNDINAMARCA BOGOTA CLL 36 SUR # 73D - 55 ','4.6208917','-74.148321',' 4501007 - 4943387','1','\0\0\0\0\0\0\0nn��{@�]~�R�'),
(105,'DIRECTO','BOGOTA',11,19,'CATASTRO','CLL  25 # 28-58','CUNDINAMARCA BOGOTA CLL  25 # 28-58 ','4.6242814','-74.0808291','3689986','1','\0\0\0\0\0\0\0��C@+D�M,�R�'),
(106,'DIRECTO','BOGOTA',11,19,'TERMINAL DE TRANSPORTE II','CLL 33 B # 68 D - 13 MODULO 4 LOCAL 147','CUNDINAMARCA BOGOTA CLL 33 B # 68 D - 13 MODULO 4 LOCAL 147 ','4.65244','-74.11232','4284433','1','\0\0\0\0\0\0\0�h:;�@�9@0�R�'),
(107,'DIRECTO','BOGOTA',11,19,'CENTRO EMPRESARIAL 128','CRA 7 # 127-48 CASA D','CUNDINAMARCA BOGOTA CRA 7 # 127-48 CASA D ','4.702971','-74.028543','6475789','1','\0\0\0\0\0\0\0J%<���@���ӁR�'),
(108,'DIRECTO','BOGOTA',11,19,'HOMECENTER CTRO CIAL CALIMA','AV.CRA 27 # 21 - 75 LC 2','CUNDINAMARCA BOGOTA AV.CRA 27 # 21 - 75 LC 2 ','4.61819','-74.08394','7430104','1','\0\0\0\0\0\0\0bۢ�y@��E_�R�'),
(109,'DIRECTO','BOGOTA',11,19,'RIONEGRO CALLE 93','AV CLL 93 # 49 36','CUNDINAMARCA BOGOTA AV CLL 93 # 49 36 ','4.6808974','-74.0622149','2366089','1','\0\0\0\0\0\0\0ɹ+=�@�44T��R�'),
(110,'DIRECTO','BOGOTA',11,19,'PALOQUEMAO AV 19','CLL 19 # 21 - 89','CUNDINAMARCA BOGOTA CLL 19 # 21 - 89 ','4.6142927','-74.0834635','2772248','1','\0\0\0\0\0\0\0��B%	u@8�JwW�R�'),
(111,'DIRECTO','BOGOTA',11,19,'JAVERIANA','CRA 7 # 40 - 62 EDIF CENTRAL PS 1','CUNDINAMARCA BOGOTA CRA 7 # 40 - 62 EDIF CENTRAL PS 1 ','4.62767','-74.06534','3384977','1','\0\0\0\0\0\0\0���컂@��҇.�R�'),
(112,'DIRECTO','BOGOTA',11,19,'TCI 1','AV CLL 26 # 116 - 29 TCI # 1','CUNDINAMARCA BOGOTA AV CLL 26 # 116 - 29 TCI # 1 ','4.694266','-74.140230','7700385','1','\0\0\0\0\0\0\0!撪��@��?���R�'),
(113,'DIRECTO','BOGOTA',11,19,'AMERICAS CARRERA 43','AV AMERICAS # 43 - 37','CUNDINAMARCA BOGOTA AV AMERICAS # 43 - 37 ','4.626100999999990','-74.100278','2690564','1','\0\0\0\0\0\0\0��۞ �@��j�j�R�'),
(114,'DIRECTO','BOGOTA',11,19,'C.C. UNICENTRO OCCIDENTE','CRA 111C # 86 - 74 L 219','CUNDINAMARCA BOGOTA CRA 111C # 86 - 74 L 219 ','4.7242141','-74.1133741','4406665','1','\0\0\0\0\0\0\0S5�a��@��p�A�R�'),
(115,'DIRECTO','BOGOTA',11,19,'AV CRA  68 CLL 19','AV CRA 68 # 19 - 42 L 102','CUNDINAMARCA BOGOTA AV CRA 68 # 19 - 42 L 102 ','4.6432312','-74.112512','2601485','1','\0\0\0\0\0\0\0=3��@\Z�e3�R�'),
(116,'DIRECTO','BOGOTA',11,19,'TORRES SAMSUNG','CRA 7 # 113 - 43 L 105','CUNDINAMARCA BOGOTA CRA 7 # 113 - 43 L 105 ','4.692053100000000','-74.0344133','6190580','1','\0\0\0\0\0\0\0�a^���@	���3�R�'),
(117,'DIRECTO','BOGOTA',11,19,'CENTRO SUBA','CLL 139 # 91A - 19 L 1 - 103','CUNDINAMARCA BOGOTA CLL 139 # 91A - 19 L 1 - 103 ','4.737687','-74.085665',' 6857389 - 6859609','1','\0\0\0\0\0\0\0Ǽ�8d�@\\Z\r�{�R�'),
(118,'DIRECTO','BOGOTA',11,19,'DIRECCION GENERAL','CALLE 6 # 34A - 11','CUNDINAMARCA BOGOTA CALLE 6 # 34A - 11 ','4.610254','-74.102829',' 7700380 - 770380','1','\0\0\0\0\0\0\0C�l�p@%���R�'),
(119,'DIRECTO','BOGOTA',11,19,'AV CARACAS','AV CARACAS 34 -10  L 101','CUNDINAMARCA BOGOTA AV CARACAS 34 -10  L 101 ','4.622761','-74.069258',' 2327109 - 2878750','1','\0\0\0\0\0\0\0��@�}@!?�n�R�'),
(120,'DIRECTO','BOGOTA',11,19,'CHAPINERO','CLL 60 # 13 A - 25','CUNDINAMARCA BOGOTA CLL 60 # 13 A - 25 ','4.646974','-74.064738',' 3452471 - 3453804','1','\0\0\0\0\0\0\0\0s-Z��@�3ڪ$�R�'),
(121,'DIRECTO','BOGOTA',11,19,'FONTIBON','CLL 17 # 96C - 38','CUNDINAMARCA BOGOTA CLL 17 # 96C - 38 ','4.6684352','-74.1425167','4217099','1','\0\0\0\0\0\0\0��Fz�@�h]��R�'),
(122,'DIRECTO','BOGOTA',11,19,'LAGO','CRA 15 # 74 - 11','CUNDINAMARCA BOGOTA CRA 15 # 74 - 11 ','4.6612403','-74.0604869','2178135','1','\0\0\0\0\0\0\0]0]-�@�Ur߃R�'),
(123,'DIRECTO','BOGOTA',11,19,'RESTREPO','CLL 15 SUR # 18 - 30','CUNDINAMARCA BOGOTA CLL 15 SUR # 18 - 30 ','4.5866117','-74.09971759999990',' 2393856 - 2729245','1','\0\0\0\0\0\0\0��˼�X@t���a�R�'),
(124,'DIRECTO','BOGOTA',11,19,'SEGUROS BOLIVAR','CRA 10 # 16 - 15	','CUNDINAMARCA BOGOTA CRA 10 # 16 - 15	 ','4.6042811','-74.075276',' 2829735 - 3343967','1','\0\0\0\0\0\0\0](��j@�mRфR�'),
(125,'DIRECTO','BOGOTA',11,19,'CALLE 68','CRA 24 # 70A - 56','CUNDINAMARCA BOGOTA CRA 24 # 70A - 56 ','4.6008054','-74.094244',' 2350961 - 3127739 - 3463599','1','\0\0\0\0\0\0\0q\n�9g@v��R�'),
(126,'DIRECTO','BOGOTA',11,19,'VENECIA','AUTOP SUR # 48 - 37','CUNDINAMARCA BOGOTA AUTOP SUR # 48 - 37 ','4.594144','-74.134787','2386989','1','\0\0\0\0\0\0\0`u�Hg`@C;�Y��R�'),
(127,'DIRECTO','BOGOTA',11,19,'MODELIA','CRA 75 # 25F - 09','CUNDINAMARCA BOGOTA CRA 75 # 25F - 09 ','4.6693291','-74.1162125','4101657','1','\0\0\0\0\0\0\0a��d�@���p�R�'),
(128,'DIRECTO','BOGOTA',11,19,'COUNTER UNICENTRO','AV 15 # 124 - 30 L 1-105','CUNDINAMARCA BOGOTA AV 15 # 124 - 30 L 1-105 ','4.8164756','-74.36169009999990','6298104','1','\0\0\0\0\0\0\0���-D@W�;�%�R�'),
(129,'DIRECTO','BOGOTA',11,19,'CAFAM FLORESTA','TRANSV 48 # 94 - 97 LC 1','CUNDINAMARCA BOGOTA TRANSV 48 # 94 - 97 LC 1 ','4.68556','-74.07290','6030025','1','\0\0\0\0\0\0\0u��p�@8��d��R�'),
(130,'DIRECTO','BOGOTA',11,19,'CAOBOS','AV CRA 19 # 148 - 16','CUNDINAMARCA BOGOTA AV CRA 19 # 148 - 16 ','4.7307086','-74.0452134','6156797','1','\0\0\0\0\0\0\0���>�@ҕ���R�'),
(131,'DIRECTO','BOGOTA',11,19,'HOME CENTER CLL 80','AV 68 # 80 - 77 LC 19','CUNDINAMARCA BOGOTA AV 68 # 80 - 77 LC 19 ','4.68441','-74.07871','6304647','1','\0\0\0\0\0\0\0�8��ռ@����	�R�'),
(132,'DIRECTO','BOGOTA',11,19,'HOME CENTER CLL 170','CRA 45 # 175 - 50 LC 22','CUNDINAMARCA BOGOTA CRA 45 # 175 - 50 LC 22 ','4.75581','-74.04543','6693758','1','\0\0\0\0\0\0\0���@w;S�R�'),
(133,'DIRECTO','BOGOTA',11,19,'CLL 13 ZONA INDUSTRIAL','CLL 13 # 68B - 78 INT 6','CUNDINAMARCA BOGOTA CLL 13 # 68B - 78 INT 6 ','4.6398907','-74.1215295','4126726','1','\0\0\0\0\0\0\0���?�@���#ǇR�'),
(134,'DIRECTO','BOGOTA',11,19,'CIUDAD SALITRE CALLE 24','AV CLL 24 N 69 D 65','CUNDINAMARCA BOGOTA AV CLL 24 N 69 D 65 ','4.658913099999990','-74.1147821','7467129','1','\0\0\0\0\0\0\0�����@�j�X�R�'),
(135,'DIRECTO','BOGOTA',11,19,'DIRECCION GENERAL 24 HORAS','AV 6 NO 34 A - 11','CUNDINAMARCA BOGOTA AV 6 NO 34 A - 11 ','4.7113428','-74.2031835','7700380','1','\0\0\0\0\0\0\0��8?j�@��]�\0�R�'),
(136,'DIRECTO','BOGOTA',11,19,'CALLE 100','CLL 100 # 19A - 73 L 03','CUNDINAMARCA BOGOTA CLL 100 # 19A - 73 L 03 ','4.6860623','-74.054181',' 2189409 - 2563113','1','\0\0\0\0\0\0\0����@\"ĕ�w�R�'),
(137,'DIRECTO','BOGOTA',11,19,'CAN','CLL 44 # 57A - 56','CUNDINAMARCA BOGOTA CLL 44 # 57A - 56 ','4.647813','-74.0936524',' 3153094 - 3153095','1','\0\0\0\0\0\0\0ѱ�J\\�@H̢f��R�'),
(138,'DIRECTO','BOGOTA',11,19,'CENTRO CRA 9','CRA 9 # 17 - 70 L 105','CUNDINAMARCA BOGOTA CRA 9 # 17 - 70 L 105 ','4.605188000000000','-74.073628',' 2864060 - 2864302','1','\0\0\0\0\0\0\0�/g�k@y7R��R�'),
(139,'DIRECTO','BOGOTA',11,19,'COUNTRY','CRA 15 # 79 - 19','CUNDINAMARCA BOGOTA CRA 15 # 79 - 19 ','4.665639','-74.05781979999990','6369688','1','\0\0\0\0\0\0\0��E��@��Q��R�'),
(140,'DIRECTO','BOGOTA',11,19,'SIETE DE AGOSTO','CRA 24 #61D-90','CUNDINAMARCA BOGOTA CRA 24 #61D-90 ','4.648558','-74.073335','2101533','1','\0\0\0\0\0\0\0�<���@��H���R�'),
(141,'DIRECTO','BOGOTA',11,19,'TOBERIN','CLL 161 # 20 - 15','CUNDINAMARCA BOGOTA CLL 161 # 20 - 15 ','4.741099','-74.0447329',' 6712784 - 6784823','1','\0\0\0\0\0\0\0�d\0���@��a�܂R�'),
(142,'DIRECTO','BOGOTA',11,19,'WORLD TRADE CENTER','CLL 100 # 8A - 55 L 147','CUNDINAMARCA BOGOTA CLL 100 # 8A - 55 L 147 ','4.6812268','-74.041275','6112460','1','\0\0\0\0\0\0\0������@��?��R�'),
(143,'DIRECTO','BOGOTA',11,19,'EXITO NORTE','CRA 43 # 173 - 98 LC 2092 - 180','CUNDINAMARCA BOGOTA CRA 43 # 173 - 98 LC 2092 - 180 ','4.75323','-74.04725','6774879','0','\0\0\0\0\0\0\0�v��N@/�$�R�'),
(144,'DIRECTO','BOGOTA',11,19,'EXITO AMERICAS','AV AMERICAS # 68 A -78 LC 2084 - 122','CUNDINAMARCA BOGOTA AV AMERICAS # 68 A -78 LC 2084 - 122 ','4.63215','-74.12264','4191632','0','\0\0\0\0\0\0\0e�`TR�@�KqUهR�'),
(145,'DIRECTO','BOGOTA',11,19,'AEROPUERTO','AV EL DORADO # 103 - 22 ENT 2 INT 12','CUNDINAMARCA BOGOTA AV EL DORADO # 103 - 22 ENT 2 INT 12 ','4.693280','-74.131296',' 110132 - 4398900','1','\0\0\0\0\0\0\0.�;1��@�V\'g�R�'),
(146,'DIRECTO','BOGOTA',11,19,'JUMBO 170','CLL 170 # 69 - 47 L 3','CUNDINAMARCA BOGOTA CLL 170 # 69 - 47 L 3 ','4.759657199999990','-74.0645156','7047671','1','\0\0\0\0\0\0\0�����	@<\n!�R�'),
(147,'DIRECTO','BOGOTA',11,19,'HOTEL TEQUENDAMA','CRA 10 # 26 - 21 L 22','CUNDINAMARCA BOGOTA CRA 10 # 26 - 21 L 22 ','4.608049299999990','-74.0729571',' 2869457 - 3820300','1','\0\0\0\0\0\0\0/l�y�n@��AT��R�'),
(148,'DIRECTO','BOGOTA',11,19,'LA CAPUCHINA','CRA 13 # 13 - 47','CUNDINAMARCA BOGOTA CRA 13 # 13 - 47 ','4.6035676','-74.07883149999990','2841534','1','\0\0\0\0\0\0\0j���\rj@:�F��R�'),
(149,'DIRECTO','BOGOTA',11,19,'ANTIGUO COUNTRY','CRA 15 # 80 - 44','CUNDINAMARCA BOGOTA CRA 15 # 80 - 44 ','4.6667725','-74.0567484',' 2573066 - 6914504','1','\0\0\0\0\0\0\0}iƪ@l�\nġ�R�'),
(150,'DIRECTO','BOGOTA',11,19,'CHAPINERO CRA 17 II','CRA 17 NO 63 - 24','CUNDINAMARCA BOGOTA CRA 17 NO 63 - 24 ','4.650414','-74.067574','2495570','1','\0\0\0\0\0\0\02q� �@��!S�R�'),
(151,'DIRECTO','BOGOTA',11,19,'CMI CASTILLA','TRANSV 78C # 6B - 33','CUNDINAMARCA BOGOTA TRANSV 78C # 6B - 33 ','4.633335','-74.147189',' 2927293 - 4110787','1','\0\0\0\0\0\0\0\r�a���@.Ui�k�R�'),
(152,'DIRECTO','BOGOTA',11,19,'NQS U. NACIONAL','AV CRA 30 # 45 - 20','CUNDINAMARCA BOGOTA AV CRA 30 # 45 - 20 ','4.634801599999990','-74.0793322','2686313','1','\0\0\0\0\0\0\01�=n	�@C!]��R�'),
(153,'DIRECTO','BOGOTA',11,19,'PARQUE CENTRAL BAVARIA 2','CRA 13 # 28 - 38 L 135 - 136','CUNDINAMARCA BOGOTA CRA 13 # 28 - 38 L 135 - 136 ','4.6160572','-74.0692483',' 3368858 - 3368880','1','\0\0\0\0\0\0\0��ٲ�v@n�k�n�R�'),
(154,'DIRECTO','BOGOTA',11,19,'C.C. PLAZA IMPERIAL','AV CRA 104 # 148 - 07 L 2 - 71','CUNDINAMARCA BOGOTA AV CRA 104 # 148 - 07 L 2 - 71 ','4.7484054','-74.095339','6870576','1','\0\0\0\0\0\0\0e�4�]�@\"¿\Z�R�'),
(155,'DIRECTO','BOGOTA',11,19,'CARULLA CL 98','CALLE 98 NO 17 A  - 31','CUNDINAMARCA BOGOTA CALLE 98 NO 17 A  - 31 ','4.6830788','-74.04899619999990','3202002790','1','\0\0\0\0\0\0\0]Jy�@/(��\"�R�'),
(156,'DIRECTO','BOGOTA',11,19,'SERVIPAK','AV QUITO CRA 30 NO 71 - 19','CUNDINAMARCA BOGOTA AV QUITO CRA 30 NO 71 - 19 ','4.6661391','-74.0753509','6305026','1','\0\0\0\0\0\0\0�XD^ �@YΔ�҄R�'),
(157,'DIRECTO','BOGOTA',11,19,'MUELLE DE CARGA 2','AV EL DORADO # 106 - 39 LC 140','CUNDINAMARCA BOGOTA AV EL DORADO # 106 - 39 LC 140 ','4.692768','-74.136171','4289033','1','\0\0\0\0\0\0\0�~�d�@y���R�'),
(158,'DIRECTO','BOGOTA',11,19,'SAN ANDRESITO CALLE 8','CLL 8 # 20 - 30 L 121','CUNDINAMARCA BOGOTA CLL 8 # 20 - 30 L 121 ','4.603535','-74.0894754',' 2770306 - 2770307','1','\0\0\0\0\0\0\0\'�;j@�2���R�'),
(159,'DIRECTO','BOGOTA',11,19,'JUMBO CRA 30','CRA 32 NO 18 - 10 LC 1030','CUNDINAMARCA BOGOTA CRA 32 NO 18 - 10 LC 1030 ','4.61851','-74.09070',' 3123498785 - 7563539','1','\0\0\0\0\0\0\0��x�Zy@�o_΅R�'),
(160,'DIRECTO','BOGOTA',11,19,'COLINA CAMPESTRE III','CARRERA 58 NO 128 B-14','CUNDINAMARCA BOGOTA CARRERA 58 NO 128 B-14 ','4.7163355','-74.069422','6179924','1','\0\0\0\0\0\0\0�ڥ\r��@���hq�R�'),
(161,'DIRECTO','BOYACA',5,59,'OF CHIQUINQUIRA PPAL','CRA 9 NO 17 - 101','BOYACA CHIQUINQUIRA CRA 9 NO 17 - 101 ','5.61703','-73.8151603',' 7264079 - 7264521','1','\0\0\0\0\0\0\0��Z��w@�_�+tR�'),
(162,'DIRECTO','BOYACA',5,59,'CHIQUINQUIRA CRA 8A','CRA 8A NO 4A - 09 TERMINAL','BOYACA CHIQUINQUIRA CRA 8A NO 4A - 09 TERMINAL ','5.609929699999990','-73.82377340000000','7265317','1','\0\0\0\0\0\0\0�oIi�p@$��tR�'),
(163,'DIRECTO','BOYACA',5,61,'TUNJA SUR CR 11','CRA 11 # 7 - 41','BOYACA TUNJA CRA 11 # 7 - 41 ','5.5220547','-73.36669580000000','7408020','1','\0\0\0\0\0\0\0r�܁�@(%��wWR�'),
(164,'DIRECTO','BOYACA',5,55,'CIUDADELA PARQUE INDUSTRIAL','CARRERA 1A NO 1 - 02 EDIFICIO ADMINISTRATIVO CIUDA','BOYACA DUITAMA CARRERA 1A NO 1 - 02 EDIFICIO ADMINISTRATIVO CIUDA ','5.790197','-73.063577',' 3114632309 - 7604435','1','\0\0\0\0\0\0\0ٖg))@��C�DR�'),
(165,'DIRECTO','BOYACA',5,55,'DUITAMA OF PPAL','CRA 17 # 14-32','BOYACA DUITAMA CRA 17 # 14-32 ','5.8261448','-73.0341564',' 0987603403 - 7611272 - 987611271','1','\0\0\0\0\0\0\0�\n��M@�<S�/BR�'),
(166,'DIRECTO','BOYACA',5,55,'OFICINA AV LAS AMERICAS','AV LAS AMERICAS # 21 - 19','BOYACA DUITAMA AV LAS AMERICAS # 21 - 19 ','5.8221639','-73.03729489999990',' 0987611141 - 0987623553','1','\0\0\0\0\0\0\0��YU�I@��%\ncBR�'),
(167,'DIRECTO','BOYACA',5,61,'TUNJA CENTRO','CRA 9 #19 - 98','BOYACA TUNJA CRA 9 #19 - 98 ','5.5324197','-73.3610941','987400166','1','\0\0\0\0\0\0\0\n�<�2!@ԑm*WR�'),
(168,'DIRECTO','BOYACA',5,61,'TUNJA UNICENTRO II','CL 40 NO 1 A - 71','BOYACA TUNJA CL 40 NO 1 A - 71 ','5.5309168','-73.36150649999990',NULL,'1','\0\0\0\0\0\0\0��S��@��(�\"WR�'),
(169,'DIRECTO','BOYACA',5,56,'OF SOGAMOSO PPAL','CRA 10 # 14 - 117','BOYACA SOGAMOSO CRA 10 # 14 - 117 ','5.7176086','-72.92578600000000',' 0987711350 - 3144004478 - 7711351 - 987711350','1','\0\0\0\0\0\0\0:P����@F�@;R�'),
(170,'DIRECTO','BOYACA',5,61,'TUNJA NORTE CONCESIONARIOS AV SEXTA','AV  6 # 51 - 174 BDG 6','BOYACA TUNJA AV  6 # 51 - 174 BDG 6 ','5.55999','-73.34852','987405757','1','\0\0\0\0\0\0\0!Y�n=@��&NVR�'),
(171,'DIRECTO','BOYACA',5,57,'JENESANO OFIC.PRINC.','CLL 8 # 2 - 30','BOYACA JENESANO CLL 8 # 2 - 30 ','5.385492999999990','-73.3636459','3004694237','1','\0\0\0\0\0\0\0�XO���@��s�EWR�'),
(172,'DIRECTO','BOYACA',5,58,'OF PAIPA PPAL','CRA 21 # 23 - 73','BOYACA PAIPA CRA 21 # 23 - 73 ','5.780947','-73.118280',' 0987850596 - 0987851620','1','\0\0\0\0\0\0\0#���@V�F�GR�'),
(173,'DIRECTO','EJE CAFETERO',19,35,'BODEGA ARMENIA','AV 19 # 2N - 50','QUINDIO ARMENIA (Q) AV 19 # 2N - 50 ','4.557235299999990','-75.6543034','7417800','1','\0\0\0\0\0\0\0���:@X*^��R�'),
(174,'DIRECTO','EJE CAFETERO',23,45,'PRINCIPAL IBAGUE','CRA 8 # 122 - 27 VIA EL SALADO DIAGONAL SANTA ANA','TOLIMA IBAGUE CRA 8 # 122 - 27 VIA EL SALADO DIAGONAL SANTA ANA ','4.42584','-75.25045',' 0982640270 - 2640270 - 2643096 - 2723340','1','\0\0\0\0\0\0\0�P�f�@ �o_�R�'),
(175,'DIRECTO','EJE CAFETERO',19,35,'CARRERA 15','CRA 15 # 22 - 42 LOCAL 1','QUINDIO ARMENIA (Q) CRA 15 # 22 - 42 LOCAL 1 ','4.53240','-75.67476','967358736','1','\0\0\0\0\0\0\0ŏ1w-!@�)�D/�R�'),
(176,'DIRECTO','EJE CAFETERO',19,35,'ARMENIA CALIMA LA 14','C.CIAL CALIMA 14 CRA 6 ENTRE CLL 3 Y 4 LC 1 - 08 ','QUINDIO ARMENIA (Q) C.CIAL CALIMA 14 CRA 6 ENTRE CLL 3 Y 4 LC 1 - 08  ','4.540880','-75.660584',' 096-7341440','1','\0\0\0\0\0\0\0�=\\r�)@�G�R�'),
(177,'DIRECTO','EJE CAFETERO',6,62,'BODEGA MANIZALES','KM 11 VIA AL MAGDALENA ','CALDAS MANIZALES KM 11 VIA AL MAGDALENA  ','5.0402279','-75.44532389999990','988742199','1','\0\0\0\0\0\0\0\n���1)@��/��R�'),
(178,'DIRECTO','EJE CAFETERO',6,63,'OFICINA PPAL CENTRO','CRA 2 # 14 - 25','CALDAS LA DORADA CRA 2 # 14 - 25 ','5.4535759','-74.6625892','8577707','1','\0\0\0\0\0\0\0�c3v�@�+��g�R�'),
(179,'DIRECTO','EJE CAFETERO',6,63,'OFICINA PPAL CENTRO CR 2','CR 2 NO 14 - 18 P1','CALDAS LA DORADA CR 2 NO 14 - 18 P1 ','5.4535135','-74.6625703',NULL,'1','\0\0\0\0\0\0\0�-��e�@BsB�g�R�'),
(180,'DIRECTO','EJE CAFETERO',19,35,'EL NORTE II','CRA 14 # 18N - 42','QUINDIO ARMENIA (Q) CRA 14 # 18N - 42 ','4.5413127','-75.6641685','967358585','1','\0\0\0\0\0\0\0I�]�M*@#�����R�'),
(181,'DIRECTO','EJE CAFETERO',23,45,'LA VEINTE','CRA 5 # 20 - 02','TOLIMA IBAGUE CRA 5 # 20 - 02 ','4.441661799999990','-75.2322305','982617964','1','\0\0\0\0\0\0\0ޑ��B�@��P���R�'),
(182,'DIRECTO','EJE CAFETERO',23,45,'CARRERA 5','CRA 5 # 24 - 13','TOLIMA IBAGUE CRA 5 # 24 - 13 ','4.4403084','-75.2281874','982612986','1','\0\0\0\0\0\0\0ɝ�4��@�S���R�'),
(183,'DIRECTO','EJE CAFETERO',6,62,'EL CENTRO','CRA 23 # 17 - 12','CALDAS MANIZALES CRA 23 # 17 - 12 ','5.06694','-75.5207516','8842045','1','\0\0\0\0\0\0\0M-[�D@�Մ�S�R�'),
(184,'DIRECTO','EJE CAFETERO',6,62,'PRINCIPAL MANIZALES','CRA 23 # 73 - 90 PS 1','CALDAS MANIZALES CRA 23 # 73 - 90 PS 1 ','5.04756','-75.48303','8868819','1','\0\0\0\0\0\0\0d���0@)?����R�'),
(185,'DIRECTO','EJE CAFETERO',20,38,'CIUDAD VICTORIA','C.C. CIUDAD VICTORIA LC 221','RISARALDA PEREIRA C.C. CIUDAD VICTORIA LC 221 ','4.8090033','-75.6787442','3334016','1','\0\0\0\0\0\0\0�lo\\k<@aV��p�R�'),
(186,'DIRECTO','EJE CAFETERO',20,38,'BULEVARES','CL 14 # 13-16 C.C. BULEVARES LC 4','RISARALDA PEREIRA CL 14 # 13-16 C.C. BULEVARES LC 4 ','4.813371600000000','-75.6926984','3252261','1','\0\0\0\0\0\0\0�|�@@u�+U�R�'),
(187,'DIRECTO','EJE CAFETERO',19,35,'PARQUE FUNDADORES','CRA 14 # 2 N -14','QUINDIO ARMENIA (Q) CRA 14 # 2 N -14 ','4.5408466','-75.66493299999990','967358584','1','\0\0\0\0\0\0\0v���)@�A$C��R�'),
(188,'DIRECTO','EJE CAFETERO',19,35,'PRINCIPAL II ARMENIA ','CRA 18 # 12 - 05','QUINDIO ARMENIA (Q) CRA 18 # 12 - 05 ','4.5384945','-75.6708486',' 0967462978 - 7358737','1','\0\0\0\0\0\0\0�M*\Zk\'@Pd�.��R�'),
(189,'DIRECTO','EJE CAFETERO',20,38,'LA 21','CLL 21 # 8 - 71','RISARALDA PEREIRA CLL 21 # 8 - 71 ','4.813454399999990','-75.6960596',' 3330125 - 3358809','1','\0\0\0\0\0\0\0\'&�0�@@G��=��R�'),
(190,'DIRECTO','EJE CAFETERO',20,38,'LA CIRCUNVALAR II','AV CIRCUNVALAR CRA 13 # 8-47 LC 1','RISARALDA PEREIRA AV CIRCUNVALAR CRA 13 # 8-47 LC 1 ','4.80852','-75.68997','3352996','1','\0\0\0\0\0\0\0Υ���;@\'N�w(�R�'),
(191,'DIRECTO','EJE CAFETERO',20,38,'INTERNACIONAL','CLL 23 # 7 - 61','RISARALDA PEREIRA CLL 23 # 7 - 61 ','4.8145852','-75.697616','3343973','1','\0\0\0\0\0\0\0�8g�\"B@�J����R�'),
(192,'DIRECTO','EJE CAFETERO',5,60,'PUERTO BOYACA','CRA 2 # 9 - 63','BOYACA PUERTO BOYACA CRA 2 # 9 - 63 ','5.9787459','-74.593908','987382520','1','\0\0\0\0\0\0\0X`~]<�@L5���R�'),
(193,'DIRECTO','EJE CAFETERO',20,38,'LAS GARZAS','AV 30 DE AG N° 34-38 GARZAS LC 17','RISARALDA PEREIRA AV 30 DE AG N° 34-38 GARZAS LC 17 ','4.81208','-75.70755','3265147','1','\0\0\0\0\0\0\0V�F�?@:��H�R�'),
(194,'DIRECTO','EJE CAFETERO',24,50,'CENTRO CARTAGO','CRA 5 # 9 - 81 LC 2','VALLE CARTAGO CRA 5 # 9 - 81 LC 2 ','4.74909','-75.91113','2093966','1','\0\0\0\0\0\0\0�\n�r�@�4�O�R�'),
(195,'DIRECTO','EJE CAFETERO',23,45,'LOS PANCHES','CRA 3 # 15 - 41 L 001','TOLIMA IBAGUE CRA 3 # 15 - 41 L 001 ','44419678','-75.2379','982610155','1','\0\0\0\0\0\0\0\0\0\0�R.�A����9�R�'),
(196,'DIRECTO','EJE CAFETERO',6,62,'TORRES PANORAMA','TORRES PANORAMA CRA 23 # 62 - 16 LC 109','CALDAS MANIZALES TORRES PANORAMA CRA 23 # 62 - 16 LC 109 ','5.058897','-75.48742399999990','8856900','1','\0\0\0\0\0\0\0~T�~O<@?�n�1�R�'),
(197,'DIRECTO','EJE CAFETERO',6,62,'PARQUE CALDAS II','CRA 22 NO 27 - 03','CALDAS MANIZALES CRA 22 NO 27 - 03 ','5.067889999999990','-75.5143798','8826166','1','\0\0\0\0\0\0\0����E@F�@���R�'),
(198,'DIRECTO','EJE CAFETERO',6,63,'OFICINA PPAL BODEGA','CLL 11 # 12 A - 10','CALDAS LA DORADA CLL 11 # 12 A - 10 ','5.4470898','-74.6694054','8570657','1','\0\0\0\0\0\0\0/�����@1��תR�'),
(199,'DIRECTO','EJE CAFETERO',20,37,'PLAZA DEL SOL','C.C.PLAZA DEL SOL LC 104A','RISARALDA DOSQUEBRADAS C.C.PLAZA DEL SOL LC 104A ','4.8287849','-75.67962229999990','3322522','1','\0\0\0\0\0\0\0��#��P@q��~�R�'),
(200,'DIRECTO','EJE CAFETERO',20,37,'PRINCIPAL II DOSQUEBRADAS','CLL 15 # 9 - 64 LA MACARENA','RISARALDA DOSQUEBRADAS CLL 15 # 9 - 64 LA MACARENA ','4.844577999999990','-75.66356999999990','963138100','1','\0\0\0\0\0\0\0&�#�`@�&N�w�R�'),
(201,'DIRECTO','EJE CAFETERO',20,38,'CALLE 44','CLL 44 # 7 - 20 LC 9','RISARALDA PEREIRA CLL 44 # 7 - 20 LC 9 ','4.81658','-75.715288','3361271','1','\0\0\0\0\0\0\0NE*�-D@)�QG��R�'),
(202,'DIRECTO','EJE CAFETERO',20,38,'C.C. UNICENTRO','C.C. UNICENTRO LC B 69','RISARALDA PEREIRA C.C. UNICENTRO LC B 69 ','4.807059199999990','-75.68378310000000',' 1 - 3200721','1','\0\0\0\0\0\0\0W��m:@�1\Z��R�'),
(203,'DIRECTO','EJE CAFETERO',20,38,'LA QUINTA II','CRA 5 # 18 - 63 ','RISARALDA PEREIRA CRA 5 # 18 - 63  ','4.816339','-75.6936556','3355041','1','\0\0\0\0\0\0\0!��^�C@�+u�d�R�'),
(204,'DIRECTO','EJE CAFETERO',23,45,'CALLE 13','CLL 13 # 2 - 91 LC 104','TOLIMA IBAGUE CLL 13 # 2 - 91 LC 104 ','4.442817','-75.240031','982617345','1','\0\0\0\0\0\0\0�s��q�@����\\�R�'),
(205,'DIRECTO','EJE CAFETERO',23,45,'LA 42','CRA 5 # 41 - 98 LC 4','TOLIMA IBAGUE CRA 5 # 41 - 98 LC 4 ','4.4354452','-75.2126377','982654440','1','\0\0\0\0\0\0\0�̴X�@c�\'ۛ�R�'),
(206,'DIRECTO','EJE CAFETERO',6,62,'PLAZA 51','CRA 23 # 49-71 LC  115 EDIFICIO PLAZA 51 ','CALDAS MANIZALES CRA 23 # 49-71 LC  115 EDIFICIO PLAZA 51  ','5.06418','-75.49865','8811845','1','\0\0\0\0\0\0\0�:�f�A@������R�'),
(207,'DIRECTO','EJE CAFETERO',6,62,'CC FUNDADORES MANIZALES','CALLE 33 B NO 20 - 03 LC 9807 ','CALDAS MANIZALES CALLE 33 B NO 20 - 03 LC 9807  ','5.068970999999990','-75.50981139999990','8894180','1','\0\0\0\0\0\0\0O}uU�F@*�����R�'),
(208,'DIRECTO','LLANOS',15,30,'C.CIAL VILLACENTRO','AV 40 # 16B - 159 CTRO C.CIAL VICO LC 17','META VILLAVICENCIO AV 40 # 16B - 159 CTRO C.CIAL VICO LC 17 ','4.10319','-73.61681',' 1 - 6704937','1','\0\0\0\0\0\0\0����i@!v��ygR�'),
(209,'DIRECTO','LLANOS',15,30,'AV MARACOS','CALLE 15 NO 15 C - 23','META VILLAVICENCIO CALLE 15 NO 15 C - 23 ','4.1323457','-73.6062869','6717055','1','\0\0\0\0\0\0\0�����@�ߑg�fR�'),
(210,'DIRECTO','LLANOS',7,64,'OF PPAL','CALLE 26 NO 05-64 LA CAMPIA','CASANARE YOPAL CALLE 26 NO 05-64 LA CAMPIA ','5.329197','-72.407328',' 3164369435 - 6354487 - 6357789','1','\0\0\0\0\0\0\0�óQ@��u�\ZR�'),
(211,'DIRECTO','LLANOS',15,30,'CENTRO HOTEL SAVOY','CRA 31 # 40 - 48 LOCAL 5','META VILLAVICENCIO CRA 31 # 40 - 48 LOCAL 5 ','4.153517','-73.638022','6727543','1','\0\0\0\0\0\0\0\'�y�3�@::Z�hR�'),
(212,'DIRECTO','LLANOS',15,30,'SAN BENITO','CRA 38 # 24A - 138','META VILLAVICENCIO CRA 38 # 24A - 138 ','4.154429599999990','-73.6310925','6681688','1','\0\0\0\0\0\0\0$#�\"�@��chR�'),
(213,'DIRECTO','LLANOS',15,30,'ÉXITO LA SABANA','CLL 7 # 45 -185 C.CIAL LA SABANA','META VILLAVICENCIO CLL 7 # 45 -185 C.CIAL LA SABANA ','4.12579','-73.63688','6823587','1','\0\0\0\0\0\0\0Ƨ\0π@�T��hR�'),
(214,'DIRECTO','LLANOS',15,30,'PORVENIR PPAL','CLL 31 # 24 - 81','META VILLAVICENCIO CLL 31 # 24 - 81 ','41433248','-73.6356291','6680633','1','\0\0\0\0\0\0\0\0\0\0\0���A�8�%�hR�'),
(215,'DIRECTO','LLANOS',7,64,'CENTRO CLL 10','CLL 10 # 21 - 48','CASANARE YOPAL CLL 10 # 21 - 48 ','5.347328999999990','-72.3987027',' 6341722 - 6333261','1','\0\0\0\0\0\0\02ϟ6�c@�TTX�R�'),
(216,'DIRECTO','NORTE',3,53,'BARRANQUILLA CENTRO','CRA 45 # 34 - 02 L 3','ATLANTICO BARRANQUILLA CRA 45 # 34 - 02 L 3 ','10.98382','-74.77757',' 3707592 - 3791382','1','\0\0\0\0\0\0\0+MJA��%@r��ñR�'),
(217,'DIRECTO','NORTE',3,53,'PRADO 76','CLL 76 # 48 - 119 L 5','ATLANTICO BARRANQUILLA CLL 76 # 48 - 119 L 5 ','10.99852','-74.80907',' 3569560 - 3586571','1','\0\0\0\0\0\0\0��p>�%@/���ǳR�'),
(218,'DIRECTO','NORTE',3,53,'SAO 93 II','CRA 46 NO 92 - 30','ATLANTICO BARRANQUILLA CRA 46 NO 92 - 30 ','11.00443','-74.82621','3594192','1','\0\0\0\0\0\0\0>\"�D&@4h��R�'),
(219,'DIRECTO','NORTE',3,53,'TERPEL CLL 82 II','CLL 82 NO 42 H - 35','ATLANTICO BARRANQUILLA CLL 82 NO 42 H - 35 ','10.997082','-74.818150','3781533','1','\0\0\0\0\0\0\0M�*���%@<Nё\\�R�'),
(220,'DIRECTO','NORTE',3,53,'GRAN BULEVARD 106','CLL 106 # 50 - 67 LC 15','ATLANTICO BARRANQUILLA CLL 106 # 50 - 67 LC 15 ','11.01352','-74.83649','3784626','1','\0\0\0\0\0\0\0���&@�[Z\r��R�'),
(221,'DIRECTO','NORTE',3,53,'PORTAL DEL PRADO','CRA 50 # 48 - 227 L 116','ATLANTICO BARRANQUILLA CRA 50 # 48 - 227 L 116 ','10.990017','-74.786038',' 3720532 - 3721308','1','\0\0\0\0\0\0\0��\Z���%@s�SrN�R�'),
(222,'DIRECTO','NORTE',3,53,'PORTAL DE SOLEDAD','CLL 63 # 14 - 50 L 107 - 108','ATLANTICO BARRANQUILLA CLL 63 # 14 - 50 L 107 - 108 ','10.96116','-74.80764',' 3231602 - 3805031','1','\0\0\0\0\0\0\0\\r�)�%@A��_��R�'),
(223,'DIRECTO','NORTE',3,53,'BARRANQUILLA COUNTRY','CRA 53 # 76 - 279','ATLANTICO BARRANQUILLA CRA 53 # 76 - 279 ','11.002729','-74.805771','3688861','1','\0\0\0\0\0\0\0g��e&@,D����R�'),
(224,'DIRECTO','NORTE',3,53,'MURILLO OF PPAL','CLL 45 # 33 - 116','ATLANTICO BARRANQUILLA CLL 45 # 33 - 116 ','10.976013','-74.787965','3792377','1','\0\0\0\0\0\0\0�����%@!Y�n�R�'),
(225,'DIRECTO','NORTE',3,53,'BARRANQUILLA PANORAMA','CLL 30 # 8 ESQ L 16','ATLANTICO BARRANQUILLA CLL 30 # 8 ESQ L 16 ','10.94633','-74.78458',' 3343012 - 3632108','1','\0\0\0\0\0\0\0�r�]��%@ F�6�R�'),
(226,'DIRECTO','NORTE',3,53,'METROPLAZA','CLL 76 # 42 - 18 C.CIAL METRO PLAZA','ATLANTICO BARRANQUILLA CLL 76 # 42 - 18 C.CIAL METRO PLAZA ','10.99224','-74.81355','3580433','1','\0\0\0\0\0\0\0됛��%@K�4�R�'),
(227,'DIRECTO','NORTE',3,53,'BUENAVISTA CLL 98 II','CLL 98 # 52 - 115','ATLANTICO BARRANQUILLA CLL 98 # 52 - 115 ','11.01232','-74.82761','3776355','1','\0\0\0\0\0\0\0g,��N&@�����R�'),
(228,'DIRECTO','NORTE',3,53,'CC VIVA BARRANQUILLA','CR 51 B NO 87 - 50 LC 005','ATLANTICO BARRANQUILLA CR 51 B NO 87 - 50 LC 005 ','11.002100','-74.807694',' 3005072924 - 3165329353','1','\0\0\0\0\0\0\0��N@&@:�,B��R�'),
(229,'DIRECTO','NORTE',9,67,'VALLEDUPAR BODEGA PRINCIPAL','CLL 21 # 17- 09','CESAR VALLEDUPAR CLL 21 # 17- 09 ','10.463846','-73.248499','5805133','1','\0\0\0\0\0\0\0��9}�$@��Yh�OR�'),
(230,'DIRECTO','NORTE',9,67,'LOS GALLOS II','TRV 18 # 20 - 94','CESAR VALLEDUPAR TRV 18 # 20 - 94 ','10.467051','-73.253565','5602022','1','\0\0\0\0\0\0\0� O!�$@A��h:PR�'),
(231,'DIRECTO','NORTE',4,54,'BOCAGRANDE','BOCAGRANDE CRA 3 CLL 6 ESQ','BOLIVAR CARTAGENA BOCAGRANDE CRA 3 CLL 6 ESQ ','10.4001599','-75.5568634',' 6620770 - 6659360','1','\0\0\0\0\0\0\0]W\'���$@��b���R�'),
(232,'DIRECTO','NORTE',4,54,'C.CIAL  SAN FERNANDO II','CARRERA 31 NO 83 B - 104 LC 125','BOLIVAR CARTAGENA CARRERA 31 NO 83 B - 104 LC 125 ','10.423052','-75.545481','6932446','1','\0\0\0\0\0\0\0��E��$@��#)��R�'),
(233,'DIRECTO','NORTE',4,54,'PORTAL DE SAN FELIPE','CALLE 30 NO. 17-109 LOCAL 1-12','BOLIVAR CARTAGENA CALLE 30 NO. 17-109 LOCAL 1-12 ','10.42107','-75.53943','6560202','1','\0\0\0\0\0\0\0H��|��$@�h��R�'),
(234,'DIRECTO','NORTE',4,54,'CARIBE PLAZA','CLL 29 # 22-108 CC CARIBE PLAZA LC 0-06','BOLIVAR CARTAGENA CLL 29 # 22-108 CC CARIBE PLAZA LC 0-06 ','10.414130','-75.530156','6445008','1','\0\0\0\0\0\0\0�����$@�qn��R�'),
(235,'DIRECTO','NORTE',10,13,'C.CIAL BUENAVISTA MONTERIA','CRA 6 NO 68 - 72','CORDOBA MONTERIA CRA 6 NO 68 - 72 ','8.781804','-75.860519','947851156','1','\0\0\0\0\0\0\0��\'�H�!@��H��R�'),
(236,'DIRECTO','NORTE',9,66,'LA MINA DRUMOND','CAMPAMENTO LA MINA DRUMOND','CESAR MINA DRUMOND PRIBBENOW CAMPAMENTO LA MINA DRUMOND ','11.366493','-72.224912','3163948581','1','\0\0\0\0\0\0\0mr���&@�M�dR�'),
(237,'DIRECTO','NORTE',3,53,'20 DE JULIO','CRA 43 # 69F - 57 L 19','ATLANTICO BARRANQUILLA CRA 43 # 69F - 57 L 19 ','10.98995','-74.80446',' 3583295 - 3681066','1','\0\0\0\0\0\0\0�Y����%@,��E|�R�'),
(238,'DIRECTO','NORTE',3,53,'METROCENTRO','CLL 45 # 1 - 85 L 1-123','ATLANTICO BARRANQUILLA CLL 45 # 1 - 85 L 1-123 ','10.93025','-74.79921',' 3249938 - 3632743','1','\0\0\0\0\0\0\0?5^�I�%@�(�A&�R�'),
(239,'DIRECTO','NORTE',3,53,'AEROPUERTO (CIL)','CLL 19 # 2 - 545 LT 2B','ATLANTICO BARRANQUILLA CLL 19 # 2 - 545 LT 2B ','10.93778','-74.77981','3711900','1','\0\0\0\0\0\0\0��=�$�%@\0�3h�R�'),
(240,'DIRECTO','NORTE',3,53,'BUENAVISTA II','CLL 99 # 52 - 53','ATLANTICO BARRANQUILLA CLL 99 # 52 - 53 ','11.0164723','-74.826027','3780570','1','\0\0\0\0\0\0\0n��o&@;�Y�ݴR�'),
(241,'DIRECTO','NORTE',4,54,'CARTAGENA BODEGA PRINCIPAL','BOSQUE TRANSVERSAL 53 # 21 B - 111','BOLIVAR CARTAGENA BOSQUE TRANSVERSAL 53 # 21 B - 111 ','10.393527','-75.485817','6535040','1','\0\0\0\0\0\0\0�/�^|�$@е/��R�'),
(242,'DIRECTO','NORTE',4,54,'GALERIA II','CLL 32 # 10A - 25 LC 1','BOLIVAR CARTAGENA CLL 32 # 10A - 25 LC 1 ','10.42405','-75.54545','6647186','1','\0\0\0\0\0\0\0Ӽ��$@�����R�'),
(243,'DIRECTO','NORTE',4,54,'CENTRO DE CONVENCIONES','CLL 24 # 8 A - 344','BOLIVAR CARTAGENA CLL 24 # 8 A - 344 ','10.420642','-75.548728','6604783','1','\0\0\0\0\0\0\0�bc^�$@��\\�R�'),
(244,'DIRECTO','NORTE',10,13,'BODEGA PRINCIPAL II','CRA 2 # 44 - 45','CORDOBA MONTERIA CRA 2 # 44 - 45 ','8.766858','-75.877397',' 3173664859 - 7810480','1','\0\0\0\0\0\0\0Z����!@�&�E\'�R�'),
(245,'DIRECTO','NORTE',13,27,'MAICAO PPAL II','CL 17 NO 10 - 42','LA GUAJIRA MAICAO CL 17 NO 10 - 42 ','11.3817865','-72.2382167',NULL,'1','\0\0\0\0\0\0\0�\'�y�&@�A�>R�'),
(246,'DIRECTO','NORTE',3,53,'GRANCENTRO','CLL 70 # 52 - 63 L 107','ATLANTICO BARRANQUILLA CLL 70 # 52 - 63 L 107 ','10.9968476','-74.8010958',' 3560527 - 3582270','1','\0\0\0\0\0\0\031�b�%@�}Q\'E�R�'),
(247,'DIRECTO','NORTE',3,53,'PETROMIL CALLE 84','CLL 84 # 51B - 45','ATLANTICO BARRANQUILLA CLL 84 # 51B - 45 ','11.0062062','-74.8172251',' 3735571 - 3781482','1','\0\0\0\0\0\0\0��u-&@\Z~�jM�R�'),
(248,'DIRECTO','NORTE',3,53,'VIA 40 MIX II','VIA 40 # 73 - 290 LC 3','ATLANTICO BARRANQUILLA VIA 40 # 73 - 290 LC 3 ','11.01079','-74.79201','3602609','1','\0\0\0\0\0\0\0h?RD�&@��J��R�'),
(249,'DIRECTO','NORTE',13,28,'RIOHACHA PPAL II','CARRERA 7 NO 22 - 13','LA GUAJIRA RIOHACHA CARRERA 7 NO 22 - 13 ','11.53675','-72.90252','7276300','1','\0\0\0\0\0\0\0�A`��\'@�>��9R�'),
(250,'DIRECTO','NORTE',2,36,'AV LIBERTADORES 3','AV LIBERTADORES NO 3 A - 73 LC 101','ARCHIPIELAGO DE SAN ANDRES SAN ANDRES AV LIBERTADORES NO 3 A - 73 LC 101 ','12.58351','-81.69457',NULL,'1','\0\0\0\0\0\0\0&ǝ��*)@���slT�'),
(251,'DIRECTO','NORTE',14,29,'SANTA MARTA CALLE 22','CLL 22 # 6 - 103 L 1 Y 2','MAGDALENA SANTA MARTA CLL 22 # 6 - 103 L 1 Y 2 ','11.2398115','-74.2091916',' 4213796 - 9544213796','1','\0\0\0\0\0\0\0�h���z&@D&*ec�R�'),
(252,'DIRECTO','NORTE',14,29,'SANTA MARTA PRINCIPAL','AV RIO CLL 28 # 19 C - 40','MAGDALENA SANTA MARTA AV RIO CLL 28 # 19 C - 40 ','11231663','-74.191667','4217910','1','\0\0\0\0\0\0\0\0\0\0�5leA<.�ED�R�'),
(253,'DIRECTO','NORTE',14,29,'AVENIDA LIBERTADOR II','LOCAL COMERCIAL # 3 C.C. AQUARELA PLAZA','MAGDALENA SANTA MARTA LOCAL COMERCIAL # 3 C.C. AQUARELA PLAZA ','11.2410735','-74.1865713','4208760','1','\0\0\0\0\0\0\0Q�\\�m{&@������R�'),
(254,'DIRECTO','NORTE',9,67,'VALLEDUPAR CRA 7','CRA 7A  19B-48','CESAR VALLEDUPAR CRA 7A  19B-48 ','10.4697303','-73.2437685','5849145','1','\0\0\0\0\0\0\0k�h}��$@��1�OR�'),
(255,'DIRECTO','NORTE',9,67,'CALLE 21 MERCADO','CL 21 NO 17 - 104 LC 1','CESAR VALLEDUPAR CL 21 NO 17 - 104 LC 1 ','10.463865','-73.249283',' 5805133 - 5885812 - 5885879','1','\0\0\0\0\0\0\0�����$@��@�OR�'),
(256,'DIRECTO','NORTE',4,54,'SANTA LUCIA','C.C.RONDA REAL L 105 Y 215','BOLIVAR CARTAGENA C.C.RONDA REAL L 105 Y 215 ','10.4222113','-75.545377',' 6431280 - 6610499','1','\0\0\0\0\0\0\0�\0[,�$@f��t��R�'),
(257,'DIRECTO','NORTE',14,29,'CENTRO II','CLL 15 # 4 - 69','MAGDALENA SANTA MARTA CLL 15 # 4 - 69 ','11.244100','-74.210870','4208760','1','\0\0\0\0\0\0\0>�٬�|&@Hm��~�R�'),
(258,'DIRECTO','NORTE',22,42,'SINCELEJO CENTRO','CLL 23 # 17 - 40','SUCRE SINCELEJO CLL 23 # 17 - 40 ','9.3009308','-75.3957361',' 2812761 - 2826146','1','\0\0\0\0\0\0\0޷��\"@.ց�S�R�'),
(259,'DIRECTO','NORTE',3,52,'GRAN PLAZA DEL SOL','CRA 32 NO 30 - 132 AREA COMUN SOTANO 1','ATLANTICO SOLEDAD CRA 32 NO 30 - 132 AREA COMUN SOTANO 1 ','10.92865','-74.77547','3629797','1','\0\0\0\0\0\0\0]�Fx�%@�A�L��R�'),
(260,'DIRECTO','NORTE',13,28,'C.CIAL SUCHIMA','CLL 15 NO 8 - 56','LA GUAJIRA RIOHACHA CLL 15 NO 8 - 56 ','11.5429137','-72.90843389999990',' 3016421153 - 7281343','1','\0\0\0\0\0\0\0����\'@����#:R�'),
(261,'DIRECTO','NORTE',4,54,'BOSQUE DIAG 21','BOSQUE DG 21 # 45A - 138','BOLIVAR CARTAGENA BOSQUE DG 21 # 45A - 138 ','10.3985905','-75.52181139999990','6448240','1','\0\0\0\0\0\0\0��\r�$@~k�[e�R�'),
(262,'DIRECTO','NORTE',10,13,'CALLE 32 II','CL 32 NO 3 - 31 LC 2','CORDOBA MONTERIA CL 32 NO 3 - 31 LC 2 ','8.75785','-75.88464','7822489','1','\0\0\0\0\0\0\0�J��!@�.��R�'),
(263,'DIRECTO','NORTE',2,36,'SAN ANDRES BODEGA PRINCIPAL','CLL 9 # 10 A - 93 SECTOR SHOLL HOUSE','ARCHIPIELAGO DE SAN ANDRES SAN ANDRES CLL 9 # 10 A - 93 SECTOR SHOLL HOUSE ','12.583836','-81.703473','5128533','1','\0\0\0\0\0\0\0ut\\��*)@�\'��mT�'),
(264,'DIRECTO','NORTE',2,36,'AV LIBERTADORES II','CRA 3 NO 3 - 12  AV LA JAIBA','ARCHIPIELAGO DE SAN ANDRES SAN ANDRES CRA 3 NO 3 - 12  AV LA JAIBA ','12.58351','-81.69457','5133463','1','\0\0\0\0\0\0\0&ǝ��*)@���slT�'),
(265,'DIRECTO','NORTE',14,29,'RODADERO','CRA 3 # 8 - 43','MAGDALENA SANTA MARTA CRA 3 # 8 - 43 ','11.252873','-74.211163','4228262','1','\0\0\0\0\0\0\03�x�&@�б��R�'),
(266,'DIRECTO','NORTE',22,42,'AV MARISCAL','CLL 32 # 28 - 81','SUCRE SINCELEJO CLL 32 # 28 - 81 ','9.303295000000000','-75.37848149999990',' 2804946 - 800527','1','\0\0\0\0\0\0\0�t{I�\"@\0)x\n9�R�'),
(267,'DIRECTO','NORTE',22,42,'BODEGA PRINCIPAL','CRA 4 # 25 - 256','SUCRE SINCELEJO CRA 4 # 25 - 256 ','9.2928684','-75.41298019999990',' 2785818 - 2785828 - 2785838','1','\0\0\0\0\0\0\0N���\"@I9�Dn�R�'),
(268,'DIRECTO','NORTE',13,28,'RIOHACHA CALLE ANCHA','CLL 6 # 9 - 11 L 102','LA GUAJIRA RIOHACHA CLL 6 # 9 - 11 L 102 ','11.5500764','-72.9097287',' 316453171 - 7274214 - 7274477','1','\0\0\0\0\0\0\0S�(��\'@㮹�8:R�'),
(269,'DIRECTO','NORTE',13,28,'RIOHACHA CL 15','CALLE 15 NO 12 A - 80','LA GUAJIRA RIOHACHA CALLE 15 NO 12 A - 80 ','11.540297','-72.913803',' 7272027 - 7272231','1','\0\0\0\0\0\0\0�@�Ρ\'@&���{:R�'),
(270,'DIRECTO','NORTE',13,27,'MAICAO PRINCIPAL','CRA 9 # 15 - 55 ','LA GUAJIRA MAICAO CRA 9 # 15 - 55  ','11.3807394','-72.2369184',' 3164347642 - 7260620','1','\0\0\0\0\0\0\0�NF��&@��ʫ)R�'),
(271,'DIRECTO','NORTE',13,27,'LA RAYA PARAGUACHON II ','CALLE 1 NO 1 168 LC 15 ','LA GUAJIRA MAICAO CALLE 1 NO 1 168 LC 15  ','11.366714','-72.222699','3166932732','1','\0\0\0\0\0\0\0�����&@�vN�@R�'),
(272,'DIRECTO','NORTE',9,67,'CRA 10','CRA 10 # 16 - 72','CESAR VALLEDUPAR CRA 10 # 16 - 72 ','10.4741701','-73.24757939999990',' 5708502 - 955744862','1','\0\0\0\0\0\0\0k{`l��$@m�DW�OR�'),
(273,'DIRECTO','NORTE',9,67,'GUATAPURI PLAZA','DG 10 # 6N-15 LC 110','CESAR VALLEDUPAR DG 10 # 6N-15 LC 110 ','10.48347','-73.24941','5849155','1','\0\0\0\0\0\0\0�1=a��$@�R\\U�OR�'),
(274,'DIRECTO','OCCIDENTE',24,51,'PRINCIPAL RICAURTE','CRA 18 # 11 - 36','VALLE BUGA CRA 18 # 11 - 36 ','3.905327','-76.303383','722280233','1','\0\0\0\0\0\0\0�|	>@dʇ�jS�'),
(275,'DIRECTO','OCCIDENTE',24,48,'ACOPI','CRA 34 # 10 - 260 ACOPI YUMBO - ZONA NTE','VALLE CALI CRA 34 # 10 - 260 ACOPI YUMBO - ZONA NTE ','3.42406','-76.53322','6912999','1','\0\0\0\0\0\0\0�U��yd@�d�F \"S�'),
(276,'DIRECTO','OCCIDENTE',24,48,'AV LAS AMERICAS CON 23','AV LAS AMERICAS NO 23 - 10','VALLE CALI AV LAS AMERICAS NO 23 - 10 ','3.4611825','-76.5275686',NULL,'1','\0\0\0\0\0\0\0q�Ws��@`���!S�'),
(277,'DIRECTO','OCCIDENTE',24,48,'CIUDAD JARDIN II','CALLE 16 NO 106 - 109 LC 103','VALLE CALI CALLE 16 NO 106 - 109 LC 103 ','3.36279','-76.53147','3399595','1','\0\0\0\0\0\0\0׆�q��\n@�3��\"S�'),
(278,'DIRECTO','OCCIDENTE',24,48,'SAN FERNANDO CALI','CLL 4B # 27 - 16','VALLE CALI CLL 4B # 27 - 16 ','3.4311912','-76.5462692',' 5560532 - 5571729','1','\0\0\0\0\0\0\0�2_s@�3�\"S�'),
(279,'DIRECTO','OCCIDENTE',24,48,'ROOSVELT II','CLL 6 # 38 A - 46','VALLE CALI CLL 6 # 38 A - 46 ','3.4355533','-76.5386876','5140025','1','\0\0\0\0\0\0\0�QY^|@�0��y\"S�'),
(280,'DIRECTO','OCCIDENTE',24,48,'NAPA','CALLE 15 A NO 25 - 526','VALLE CALI CALLE 15 A NO 25 - 526 ','3.3466457','-76.5369087','6957057','1','\0\0\0\0\0\0\0hdF.��\n@�N�\\\"S�'),
(281,'DIRECTO','OCCIDENTE',16,31,'IPIALES BATALLON II','CALLE 17 NO 7  - 75','NARIÑO IPIALES CALLE 17 NO 7  - 75 ','0.826818','-77.642399','7757408','1','\0\0\0\0\0\0\0�ϷKu�?���iS�'),
(282,'DIRECTO','OCCIDENTE',24,47,'VERSALLES PALMIRA ','CLL 42 # 29 - 29','VALLE PALMIRA CLL 42 # 29 - 29 ','3.5524349','-76.2989887','922859858','1','\0\0\0\0\0\0\0-Z%�bk@���\"S�'),
(283,'DIRECTO','OCCIDENTE',24,47,'SANTA BARBARA CLL 31','CLL 31 # 34 - 86','VALLE PALMIRA CLL 31 # 34 - 86 ','3.528125','-76.300162',' 2859860 - 3017922469','1','\0\0\0\0\0\0\0�����9@ `��5S�'),
(284,'DIRECTO','OCCIDENTE',16,32,'SAN ANDRESITO CLL 15','CLL 15 # 22B - 09','NARIÑO PASTO CLL 15 # 22B - 09 ','1.211652','-77.280569',' 7227079 - 7227081','1','\0\0\0\0\0\0\0� \"5�b�?_ѭ��QS�'),
(285,'DIRECTO','OCCIDENTE',8,65,'MODELO II','CLL 1 N # 10 - 10','CAUCA POPAYAN CLL 1 N # 10 - 10 ','2.4461652','-76.60794349999990','8373063','1','\0\0\0\0\0\0\0	�t��@/�ڋ�&S�'),
(286,'DIRECTO','OCCIDENTE',24,49,'BUENAVENTURA CENTRO','CRA 5A  #  2 - 09  - ZONA ISLA','VALLE BUENAVENTURA CRA 5A  #  2 - 09  - ZONA ISLA ','3.88704','-77.07653',' 2419406 - 2420731','1','\0\0\0\0\0\0\0b�qm�@m���DS�'),
(287,'DIRECTO','OCCIDENTE',24,49,'PRINCIPAL JUAN XXIII','CLL 6 # 35 A - 59 - ZONA CONTINENTE','VALLE BUENAVENTURA CLL 6 # 35 A - 59 - ZONA CONTINENTE ','3.88110','-77.03414',' 2420100 - 2443297','1','\0\0\0\0\0\0\0x$(~@߉Y/BS�'),
(288,'DIRECTO','OCCIDENTE',24,48,'ZACCOUR','CRA 3 # 11 - 32 OF 11','VALLE CALI CRA 3 # 11 - 32 OF 11 ','3.4464895','-76.55085679999990',' 8881097 - 8894333','1','\0\0\0\0\0\0\0$Di�@�1�<A#S�'),
(289,'DIRECTO','OCCIDENTE',24,48,'PORVENIR','CLL 34 # 2C - 13','VALLE CALI CLL 34 # 2C - 13 ','3.461649','-76.514158',' 4425198 - 4434222','1','\0\0\0\0\0\0\0M��u�@����� S�'),
(290,'DIRECTO','OCCIDENTE',24,48,'GOBERNACION','CRA 6 # 11 - 26','VALLE CALI CRA 6 # 11 - 26 ','3.4355533','-76.5386876','8821039','1','\0\0\0\0\0\0\0�QY^|@�0��y\"S�'),
(291,'DIRECTO','OCCIDENTE',24,48,'CENTRO CRA 6 ','CRA 6 # 11 - 69','VALLE CALI CRA 6 # 11 - 69 ','3.450837','-76.531988',' 8808850 - 8821231','1','\0\0\0\0\0\0\00��mP�@Swe\"S�'),
(292,'DIRECTO','OCCIDENTE',24,48,'ACOPIO CRA 15','CRA 15 # 30A - 35','VALLE CALI CRA 15 # 30A - 35 ','3.445382','-76.514303',' 4100620 - 4432169','1','\0\0\0\0\0\0\0�!p$�@O!W� S�'),
(293,'DIRECTO','OCCIDENTE',8,65,'PRINCIPAL BELLO HORIZONTE CLL 66','CLL 66 NORTE NO 9 - 96','CAUCA POPAYAN CLL 66 NORTE NO 9 - 96 ','2.482123','-76.5691258','8249060','1','\0\0\0\0\0\0\0�1�Mc�@ԓ��l$S�'),
(294,'DIRECTO','OCCIDENTE',8,65,'CIUDAD JARDIN CL 18 N','CLL 18 NORTE NO 8 N 26','CAUCA POPAYAN CLL 18 NORTE NO 8 N 26 ','2.454570','-76.597352','8377595','1','\0\0\0\0\0\0\0���@��;&S�'),
(295,'DIRECTO','OCCIDENTE',24,48,'LA PRIMERA II','CALLE 21 NO 2-60','VALLE CALI CALLE 21 NO 2-60 ','3.45704','-76.52548',' 3146866134 - 8355033','1','\0\0\0\0\0\0\0�g��@��v�!S�'),
(296,'DIRECTO','OCCIDENTE',8,65,'HISTORICO','CLL 6 # 7 - 97','CAUCA POPAYAN CLL 6 # 7 - 97 ','2.4408881','-76.6080087','8243533','1','\0\0\0\0\0\0\0��W��@��R��&S�'),
(297,'DIRECTO','OCCIDENTE',24,51,'BOSQUE CRA 8','CRA 8 # 14 - 02','VALLE BUGA CRA 8 # 14 - 02 ','3.904293','-76.293926','2391632','1','\0\0\0\0\0\0\0����;@l\\���S�'),
(298,'DIRECTO','OCCIDENTE',24,48,'VERSALLES AV 4','AV  4 NTE. # 19 N -26','VALLE CALI AV  4 NTE. # 19 N -26 ','3.4729433','-76.5239129','6611930','1','\0\0\0\0\0\0\0�2��@���ɇ!S�'),
(299,'DIRECTO','OCCIDENTE',24,48,'PALACIO ROSA','AV 4N # 10 - 123 LC 2','VALLE CALI AV 4N # 10 - 123 LC 2 ','3.45542','-76.53466','3451065','1','\0\0\0\0\0\0\0���=��@����7\"S�'),
(300,'DIRECTO','OCCIDENTE',24,48,'SANTA MONICA II','CALLE 23 N NO 6 N - 52 LC 2 ','VALLE CALI CALLE 23 N NO 6 N - 52 LC 2  ','3.46473','-76.53037','6682037','1','\0\0\0\0\0\0\0�»\\ķ@�1��!S�'),
(301,'DIRECTO','OCCIDENTE',24,46,'TULUA PRINCIPAL VARIANTE','CRA 40 NO 32 - 03','VALLE TULUA CRA 40 NO 32 - 03 ','4.0771122','-76.1887364',' 2344242 - 2345000','1','\0\0\0\0\0\0\0�}$��N@%d�AS�'),
(302,'DIRECTO','OCCIDENTE',24,49,'TERMINAL BUENAVENTURA','CRA 5 # 7 - 32 LC 18 ','VALLE BUENAVENTURA CRA 5 # 7 - 32 LC 18  ','3.88913','-77.07388','922423317','1','\0\0\0\0\0\0\0�#0�@�-s�DS�'),
(303,'DIRECTO','OCCIDENTE',24,51,'BUGA - CENTRO CLL 6','CLL 6 # 10-38','VALLE BUGA CLL 6 # 10-38 ','3.898114999999990','-76.300574',' 2280120 - 2280234 - 2391645','1','\0\0\0\0\0\0\0@`��V/@���<S�'),
(304,'DIRECTO','OCCIDENTE',24,48,'PASEO LA QUINTA','CLL 5 # 46 - 85 LC 119','VALLE CALI CLL 5 # 46 - 85 LC 119 ','3.41614','-76.54810',' 5528759 - 5528930','1','\0\0\0\0\0\0\0�uT5AT@�#S�'),
(305,'DIRECTO','OCCIDENTE',24,48,'LA LUNA','CALLE 13 NO 23 C - 17 ','VALLE CALI CALLE 13 NO 23 C - 17  ','3.433855','-76.527956',' 5571732 - 5583336','1','\0\0\0\0\0\0\0\r�a��x@�n��!S�'),
(306,'DIRECTO','OCCIDENTE',24,48,'CENTRO - ACOPIO','CRA 7 # 15 - 63','VALLE CALI CRA 7 # 15 - 63 ','3.451552','-76.528384','8891279','1','\0\0\0\0\0\0\0��Kǜ@���!S�'),
(307,'DIRECTO','OCCIDENTE',24,48,'LA MERCED','AV 3N # 44N - 31','VALLE CALI AV 3N # 44N - 31 ','3.478607','-76.517211','6642783','1','\0\0\0\0\0\0\0�%�/�@j�*�!S�'),
(308,'DIRECTO','OCCIDENTE',24,48,'AMERICAS II','AV LAS AMERICAS # 22 N 35 (ZONA CENTRO)','VALLE CALI AV LAS AMERICAS # 22 N 35 (ZONA CENTRO) ','3.46175','-76.52664','4139668','1','\0\0\0\0\0\0\0�l����@�0Bx�!S�'),
(309,'DIRECTO','OCCIDENTE',16,32,'PRINCIPAL PARQUE BOLIVAR','CRA 6 # 22 - 87','NARIÑO PASTO CRA 6 # 22 - 87 ','1.208472','-77.286250','7200275','1','\0\0\0\0\0\0\0~b��U�?���QRS�'),
(310,'DIRECTO','OCCIDENTE',16,32,'PARQUE INFANTIL','CLL 16B # 29 - 48','NARIÑO PASTO CLL 16B # 29 - 48 ','1.2178387','-77.28214729999990','7362333','1','\0\0\0\0\0\0\0���nD|�?����RS�'),
(311,'DIRECTO','OCCIDENTE',24,47,'CENTRO CRA 29','CRA 29 # 31 - 58','VALLE PALMIRA CRA 29 # 31 - 58 ','3.5285619','-76.2994805','2859859','1','\0\0\0\0\0\0\0KS�~:@�RB�*S�'),
(312,'DIRECTO','ORIENTE',21,39,'BODEGA PPAL II','CLL 48 # 20 - 09','SANTANDER BARRANCABERMEJA CLL 48 # 20 - 09 ','7.058520199999990','-73.85741709999990','6213020','1','\0\0\0\0\0\0\0�$��;@�����vR�'),
(313,'DIRECTO','ORIENTE',21,40,'SAN FRANCISCO II','CRA 21 # 19 -33 LC 6','SANTANDER BUCARAMANGA CRA 21 # 19 -33 LC 6 ','7.1292784','-73.12453590000000','6323583','1','\0\0\0\0\0\0\0Q��a�@ckle�GR�'),
(314,'DIRECTO','ORIENTE',17,33,'CLL 11 II','CLL 11 # 1 - 10','NORTE DE SANTANDER CUCUTA CLL 11 # 1 - 10 ','7.886638499999990','-72.4996574',' 5710648 - 5719413','1','\0\0\0\0\0\0\0q���@\rc�R�'),
(315,'DIRECTO','ORIENTE',21,40,'CINEMAS','CRA 35 A # 48 - 148','SANTANDER BUCARAMANGA CRA 35 A # 48 - 148 ','7.115642','-73.109674','6470206','1','\0\0\0\0\0\0\0-@�jv@-��GR�'),
(316,'DIRECTO','ORIENTE',21,40,'CALLE 22 BUCARAMANGA','CR 18 NO 22 - 04 LC 2','SANTANDER BUCARAMANGA CR 18 NO 22 - 04 LC 2 ','7.126408','-73.126718','6424364','1','\0\0\0\0\0\0\0��Gq�@!t�%HR�'),
(317,'DIRECTO','ORIENTE',21,39,'PARQUE URIBE II','CL 49 NO 14 - 47','SANTANDER BARRANCABERMEJA CL 49 NO 14 - 47 ','7.059391300000000','-73.8629323','6213019','1','\0\0\0\0\0\0\0�<@_�eH:wR�'),
(318,'DIRECTO','ORIENTE',17,33,'BODEGA PPAL VIA AL AEROPUERTO','CLL 22 N # 11 - 85','NORTE DE SANTANDER CUCUTA CLL 22 N # 11 - 85 ','7.9422616','-72.5081824','975829840','1','\0\0\0\0\0\0\0F�9��@�y� R�'),
(319,'DIRECTO','ORIENTE',21,41,'OFICINA PRINCIPAL FLORIDABLANCA','CLL 11 # 8 - 08','SANTANDER FLORIDABLANCA CLL 11 # 8 - 08 ','7.058115','-73.084216','6388883','1','\0\0\0\0\0\0\0Ϡ��;@2s��cER�'),
(320,'DIRECTO','ORIENTE',21,41,'ZONA FRANCA SANTANDER','KM 3981 ANILLO VIAL RIO FRIO ED ZUZABITA','SANTANDER FLORIDABLANCA KM 3981 ANILLO VIAL RIO FRIO ED ZUZABITA ','7.060956','-73.127221','6389399','1','\0\0\0\0\0\0\0���?k>@S��c$HR�'),
(321,'DIRECTO','ORIENTE',17,33,'ZONA INDUSTRIAL II','CALLE 7 NO 3 - 48 LC 107 ','NORTE DE SANTANDER CUCUTA CALLE 7 NO 3 - 48 LC 107  ','7.89000','-72.46638','5799613','1','\0\0\0\0\0\0\0���(\\�@��+�R�'),
(322,'DIRECTO','ORIENTE',21,40,'GLORIETA ESTADIO','CRA 30 # 13A - 18','SANTANDER BUCARAMANGA CRA 30 # 13A - 18 ','7.1349922','-73.11794139999990','6451828','1','\0\0\0\0\0\0\0A�0e;�@�Z�GR�'),
(323,'DIRECTO','ORIENTE',21,40,'CALLE 22 BUCARAMANGA ','CLL 22 #18  - 29','SANTANDER BUCARAMANGA CLL 22 #18  - 29 ','7.126478400000000','-73.12649309999990',' 6424364 - 6481122&#x0D,1','0','\0\0\0\0\0\0\0�����@��vHR�'),
(324,'DIRECTO','ORIENTE',21,40,'OFICINA CENTRO CR 21','CRA 21 # 34 - 40','SANTANDER BUCARAMANGA CRA 21 # 34 - 40 ','7.1207859','-73.1219976','6340052','1','\0\0\0\0\0\0\0�E�L�{@1���GR�'),
(325,'DIRECTO','ORIENTE',21,40,'SOTOMAYOR CRA 27','CRA 27 # 42 - 83','SANTANDER BUCARAMANGA CRA 27 # 42 - 83 ','7.118091','-73.1154018','6437135','1','\0\0\0\0\0\0\0�����x@�9;�bGR�'),
(326,'DIRECTO','ORIENTE',21,40,'GAITAN CR 15','CR 15 NO 16 06 LC 102','SANTANDER BUCARAMANGA CR 15 NO 16 - 06 LC 102 ','7.131411','-73.130288','6718885','1','\0\0\0\0\0\0\0�V횐�@��z�VHR�'),
(327,'DIRECTO','REGIONAL CEN',11,24,'VILLETA PRINCIPAL','CLL 5 # 6 - 17 LC 101','CUNDINAMARCA VILLETA CLL 5 # 6 - 17 LC 101 ','5.01120','-74.47083',' 1 - 8444750 - 8447756','1','\0\0\0\0\0\0\0]�Fx@r�&\"�R�'),
(328,'DIRECTO','REGIONAL CEN',11,16,'CHIA OFICINA PPAL 2','CR 12 NO 5B-55','CUNDINAMARCA CHIA CR 12 NO 5B-55 ','4.8559068','-74.0644343','8855503','1','\0\0\0\0\0\0\00�	�rl@g�\n��R�'),
(329,'DIRECTO','REGIONAL CEN',11,18,'SOACHA SAN BERNANDINO CLL 12','CLL 12 # 3 B - 42','CUNDINAMARCA SOACHA CLL 12 # 3 B - 42 ','4.5786697','-74.21931959999990','7818581','1','\0\0\0\0\0\0\0��2ʎP@�WU	�R�'),
(330,'DIRECTO','REGIONAL CEN',11,20,'TENJO AUTOPISTA MEDELLIN II','KM 8.5 COSTADO SUR CENTRO EMP MILAN BODEGA 4','CUNDINAMARCA TENJO KM 8.5 COSTADO SUR CENTRO EMP MILAN BODEGA 4 ','4.775269','-74.17868729999990','3902961','1','\0\0\0\0\0\0\0i��@rmۜo�R�'),
(331,'DIRECTO','REGIONAL CEN',23,44,'ESPINAL OF PPAL','CLL 8 # 4 - 58','TOLIMA ESPINAL CLL 8 # 4 - 58 ','4.152708899999990','-74.88395600000000',' 2485723 - 3212169255','1','\0\0\0\0\0\0\0|;͸_�@��/���R�'),
(332,'DIRECTO','REGIONAL CEN',11,23,'GIRARDOT PPAL','CRA 10 # 20 - 22','CUNDINAMARCA GIRARDOT CRA 10 # 20 - 22 ','4.2978631','-74.80416269999990',' 8330866 - 8331850','1','\0\0\0\0\0\0\0��D1@iJ�fw�R�'),
(333,'DIRECTO','REGIONAL CEN',11,15,'FUSAGASUGA ZONA INDUSTRIAL','CALLE 25 NO 37-81 LC 1','CUNDINAMARCA FUSAGASUGA CALLE 25 NO 37-81 LC 1 ','4.325733','-74.369470','8781540','1','\0\0\0\0\0\0\0p��M@��e��R�'),
(334,'DIRECTO','REGIONAL CEN',11,17,'FACATATIVA OFICINA PPAL.','CRA 2 # 7 - 30','CUNDINAMARCA FACATATIVA CRA 2 # 7 - 30 ','4.8098374','-74.356493',' 3115041021 - 8901155','1','\0\0\0\0\0\0\0�O�F=@0�ЖR�'),
(335,'DIRECTO','REGIONAL CEN',23,43,'MELGAR PPAL II','CARRERA 23 NO 5 - 41 LC 1','TOLIMA MELGAR CARRERA 23 NO 5 - 41 LC 1 ','4.205908','-74.642391',' 2432003 - 2432004','1','\0\0\0\0\0\0\0g�����@�$��R�'),
(336,'DIRECTO','REGIONAL CEN',11,21,'FUNZA DIRECTO II','CALLE 14 NO 11 -38','CUNDINAMARCA FUNZA CALLE 14 NO 11 -38 ','4.7149518','-74.2111114','8259263','1','\0\0\0\0\0\0\0��S�@�cق�R�'),
(337,'DIRECTO','REGIONAL CEN',11,22,'MADRID PPAL II','CLL 7 # 04 - 57','CUNDINAMARCA MADRID CLL 7 # 04 - 57 ','4.732296000000000','-74.2623493','8251854','1','\0\0\0\0\0\0\0j��\0��@9�TʐR�'),
(338,'DIRECTO','SUR',12,25,'SAN PEDRO CRA 6','CRA 6 # 6 - 76','HUILA NEIVA CRA 6 # 6 - 76 ','2.9262436','-75.28742900000000',' 8632039 - 8710219','1','\0\0\0\0\0\0\0��g�h@��<e�R�'),
(339,'DIRECTO','SUR',12,25,'GOBERNACION CRA 4','CRA 4 # 8 - 77 L 1','HUILA NEIVA CRA 4 # 8 - 77 L 1 ','2.927285','-75.290028','8716273','1','\0\0\0\0\0\0\0%��ek@(��я�R�'),
(340,'DIRECTO','SUR',12,25,'CARRERA 6','CRA 6 # 13 - 03','HUILA NEIVA CRA 6 # 13 - 03 ','2.931836','-75.288930','8642298','1','\0\0\0\0\0\0\07��nft@T5A�}�R�'),
(341,'DIRECTO','SUR',12,25,'LA TOMA','CRA 7 # 11 - 74 L 3','HUILA NEIVA CRA 7 # 11 - 74 L 3 ','2.931228','-75.287689','8631494','1','\0\0\0\0\0\0\0��h�\'s@Кi�R�'),
(342,'DIRECTO','SUR',12,26,'PITALITO CALLE 4 CENTRO','CLL 4 # 4 - 16','HUILA PITALITO CLL 4 # 4 - 16 ','1.850682','-76.047506',' 0 - 8364912','1','\0\0\0\0\0\0\0仔�d��?H�V\nS�'),
(343,'DIRECTO','SUR',12,26,'REPUESTOS','CRA 4 # 1 A - 24','HUILA PITALITO CRA 4 # 1 A - 24 ','1.849988','-76.04801250000000','8368496','1','\0\0\0\0\0\0\0�_���?&S�S�'),
(344,'DIRECTO','SUR',18,34,'MOCOA PPAL CLL 8','CL 8 NO 8 - 22','PUTUMAYO MOCOA CL 8 NO 8 - 22 ','1.1471025','-76.6480881',NULL,'1','\0\0\0\0\0\0\0���&�Z�?P��Fz)S�'),
(345,'DIRECTO','SUR',18,34,'MOCOA PPAL AV COLOMBIA','CRA 9 # 20 - 27 ','PUTUMAYO MOCOA CRA 9 # 20 - 27  ','1.1585263','-76.64791079999990',' 4206282 - 3164467445','1','\0\0\0\0\0\0\0���R��?c.�^w)S�'),
(346,'DIRECTO','SUR',12,26,'PITALITO CALLE 9','CLL 9 # 1B - 20','HUILA PITALITO CLL 9 # 1B - 20 ','1.8520175','-76.04302659999990',' 8362734 - 8363792','1','\0\0\0\0\0\0\06�!\Zݡ�?�����S�'),
(347,'DIRECTO','SUR',12,25,'QUIRINAL II','CRA 7 # 15 - 52 LC1','HUILA NEIVA CRA 7 # 15 - 52 LC1 ','2.93357','-75.28851','8740455','1','\0\0\0\0\0\0\0�6T��w@j���v�R�'),
(348,'DIRECTO','SUR',12,25,'OFC PPAL ZONA INDUSTRIAL','CRA 8 NO 22 -21 SUR','HUILA NEIVA CRA 8 NO 22 -21 SUR ','2.9216732','-75.28413499999990','8633332','1','\0\0\0\0\0\0\0��2�_@�)�D/�R�'),
(349,'DIRECTO','SUR',12,25,'GRANJAS','CRA 6 # 26A - 26 LOCAL 101','HUILA NEIVA CRA 6 # 26A - 26 LOCAL 101 ','2.94241','-75.29194',' 8620000 - 8740385','1','\0\0\0\0\0\0\0cA�@;%��R�'),
(350,'DIRECTO','SUR',12,25,'AVENIDA 26','CRA 8A  # 40 - 41','HUILA NEIVA CRA 8A  # 40 - 41 ','2.9246094','-75.2848094','8642299','1','\0\0\0\0\0\0\0����e@��4Q:�R�'),
(351,'DIRECTO','SUR',12,25,'CRA 5  LOS REPUESTOS','CRA 5 # 2 - 47','HUILA NEIVA CRA 5 # 2 - 47 ','2.919221','-75.2859509','8642626','1','\0\0\0\0\0\0\0_&��Z@��\0M�R�'),
(352,'DIRECTO','SUR',12,25,'CL 6 PALACIO DE JUSTICIA','CALLE 6 NO 3 - 40 LC 3','HUILA NEIVA CALLE 6 NO 3 - 40 LC 3 ','2.92475','-75.28989',' 3223558722 - 8632260','1','\0\0\0\0\0\0\0���S�e@�[Ɏ��R�');

/*Table structure for table `states` */

DROP TABLE IF EXISTS `states`;

CREATE TABLE `states` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `states` */

insert  into `states`(`id`,`name`,`created_at`,`updated_at`) values 
(1,'ANTIOQUIA',NULL,NULL),
(2,'ARCHIPIELAGO',NULL,NULL),
(3,'ATLANTICO',NULL,NULL),
(4,'BOLIVAR',NULL,NULL),
(5,'BOYACA',NULL,NULL),
(6,'CALDAS',NULL,NULL),
(7,'CASANARE',NULL,NULL),
(8,'CAUCA',NULL,NULL),
(9,'CESAR',NULL,NULL),
(10,'CORDOBA',NULL,NULL),
(11,'CUNDINAMARCA',NULL,NULL),
(12,'HUILA',NULL,NULL),
(13,'LA GUAJIRA',NULL,NULL),
(14,'MAGDALENA',NULL,NULL),
(15,'META',NULL,NULL),
(16,'NARIÑO',NULL,NULL),
(17,'NORTE DE SAN',NULL,NULL),
(18,'PUTUMAYO',NULL,NULL),
(19,'QUINDIO',NULL,NULL),
(20,'RISARALDA',NULL,NULL),
(21,'SANTANDER',NULL,NULL),
(22,'SUCRE',NULL,NULL),
(23,'TOLIMA',NULL,NULL),
(24,'VALLE',NULL,NULL),
(25,'CAQUETA',NULL,NULL);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `document` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_phone_unique` (`phone`),
  UNIQUE KEY `users_document_unique` (`document`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`lastname`,`phone`,`document`,`password`,`remember_token`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'Carlos Andres','Beltran Franco','+573202273910','11111','$2y$10$vJS2Y9t3M8RPl4aLXmJ/9O1uQjKPWEi.jK3Gm4P2W3J7Kdsfg8jJy',NULL,NULL,'2018-11-01 10:40:35',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
