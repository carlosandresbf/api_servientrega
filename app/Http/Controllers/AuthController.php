<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User as User;;
use Illuminate\Http\Request;
use JWTAuth;
use Carbon\Carbon;


class AuthController extends Controller
{


    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = request(['document', 'password']);
        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized. Check Credentials']);
        }

        $Auth_user = auth()->user();
        $MyUser = User::find($Auth_user->id)->toArray();
        $response = array();
        $response = $MyUser;

        $response["expires_in"] = auth()->factory()->getTTL() * 20160;
        $response["accessToken"] = $token;
        $response["refreshToken"] = $token;

        return response()->json($response);

    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me(Request $request)
    {
        $Auth_user = auth()->user();
        $token = $request->bearerToken();
        $MyUser = User::find($Auth_user->id)->toArray();
        $response = array();
        $response['user']=$MyUser;
        $response['token'] = $token;

        return response()->json($response);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh(Request $request)
    {
        $new_token = auth()->refresh();
        $response = $this->respondWithToken($new_token);

        return $response;
    }

    public function invalidate()
    {
        #auth()->invalidate();
        // Pass true as the first param to force the token to be blacklisted "forever".
        auth()->invalidate(true);

        return response()->json(['message' => 'Token Invalidated successully.']);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 20160
        ]);
    }
}
