<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Resources\GlobalCollection;
use Propaganistas\LaravelPhone\PhoneNumber;
Use GuzzleHttp\Client as Guzzle;
use DB;



class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->input("filterColumn");
        $filterValue = $request->input("filterValue");
        $pageSize = $request->input("pageSize");
        $sortField = $request->input("sortField");
        $sortOrder = ($request->input("sortOrder") == "asc") ? "asc" : "desc";

        if (empty($sortField)) {
            $sortField = "name";
        }

        $item = Customer::orderBy($sortField, $sortOrder);

        if (empty($filter) || $filter == "*") {

            $item->filterValue($filterValue);

        } else {

            $item->where($filter, 'like', "%$filterValue%");

        }

        if (empty($pageSize)) {
            $pageSize = 10;
        }

        return new GlobalCollection($item->paginate($pageSize));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            "name" => "nullable|max:255",
            "lastname" => "nullable|max:255",
            "adress" => "nullable|max:255",
            "document" => "nullable|max:100|unique:customers",
            "mobile" => "required|max:100|min:10|phone:CO",
            "cities_id" => "nullable|integer"
        ]);
        $data["users_id"] = auth()->user()->id;

        $data["mobile"] = trim(PhoneNumber::make($data["mobile"])->ofCountry('CO'));
        $first_char= substr($data["mobile"], 0, 1);
        if($first_char != "+"){
            $data["mobile"] = "+".$data["mobile"];
            }
        $data = request()->only('name', 'lastname', 'mobile','adress', 'cities_id', 'document');
        $data = array_filter($data);
        $inserted = Customer::firstOrNew($data);

        return response()->json($inserted);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        $customer->City;
        return response()->json($customer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        $data = $request->validate([
            "name" => "nullable|max:255",
            "lastname" => "nullable|max:255",
            "document" => "nullable|max:100|unique:customers,document,".$customer->id,
            "mobile" => "required|max:100|min:10|phone:CO|unique:customers,mobile,".$customer->id,
            "cities_id" => "nullable|integer",
            "adress" => "nullable|max:255"
        ]);


        $adress =  $data["adress"];
        $city =  $data["cities_id"];

            // Se obtiene la información del municipio y departamento reportado por el cliente
        $munDep = DB::table('municipios as ct')
        ->join('departamentos as dp', 'dp.id', '=', 'ct.departamento_id')
        ->select('ct.name as municipio', 'dp.name as departamento')
        ->where('ct.id', $city)
        ->get();


        $google_maps_key = "API-KEY-GOOGLE-MAPS";

        $direccion = urlencode($adress.', '.$munDep[0]->municipio.', '.$munDep[0]->departamento.', Colombia');
        $url = "https://maps.google.com/maps/api/geocode/json?address=".$direccion."&key=".$google_maps_key;
        $client = new Guzzle();
        $response = $client->request('GET', $url);
        $response = json_decode($response->getBody());

        // Se envia petición a la API de Google para obtener las coordenadas de la dirección.
        // El formato ideal  de la dirección debería ser Cll 123 #123 - 123, Ibague, Tolima
        // Se debe procurar enviar solo la nomenclatura de la dirección, preferiblemente sin barrios, apto, o información que pueda inducir al error.
        // Google hace una aproximación a la dirección en caso de no haber coincidencias totales, y dicha información puede inducir a errores

        $result = DB::table('servientrega as s')
        ->join('cities as ct', 'ct.id', '=', 's.cities_id')
        ->select('s.id as id', 's.nombre as nombre', 's.direccion as direccion', 'ct.name as city', DB::raw("concat(s.lat, ' ', s.lon) as coordenadas "),
        DB::raw("(
                   GLENGTH(
                           LINESTRINGFROMWKB(
                                              LINESTRING(
                                                          `point`,
                                                               GEOMFROMTEXT('POINT(".$response->results[0]->geometry->location->lat." ".$response->results[0]->geometry->location->lng.")')
                                                         )
                                            )
                            )
                    )
                      AS distance")

         )->orderBy('distance', 'asc')
         ->limit(1)->get();


//        $output["cdc"]= $result;
//      $output["coords"]=$response->results[0]->geometry->location->lat." ".$response->results[0]->geometry->location->lng;
//        $output["maps_object"]=$response;
//        $output["direccion"]=$direccion;


//Consumo de  la API de generación de PIN de servientrega

        $url = "https://admin.dataella.com/Plataformas/Desarrollos/Servientrega/DT_SVRLT/V1/mensaje/msjmt";

        $Serv = new Guzzle();

        $to_send = array();
        $to_send["cedula"]=$data["document"];
        $to_send["nombre"]=$data["name"].' '.$data["lastname"];
        $to_send["correo"]="noaplica@noaplica.com";
        $to_send["celular"]=$data["mobile"];
        $to_send["planchas"]=$r->quantity;
        $to_send["valor"]=$r->quantity * 50000;
        $to_send["ciudad_sucursal"]=$result[0]->city;
        $to_send["nombre_sucursal"]=$result[0]->nombre;
        $to_send["direccion_sucursal"]=$result[0]->direccion;

        $Serv_r = $Serv->request('POST', $url, [
                                    'headers' => [
                                        'Content-Type' => 'application/json',
                                        'Accept'     => 'application/json',
                                        'Authorization' => 'Basic R05TcnZMVlQkMTgvd3M6U2UqJGRpMjUqcnQvVw=='
                                    ],
                                    'json' => $to_send
                                  ], $to_send);

        $Serv_r = json_decode($Serv_r->getBody());

        // Respuesta de la api
        $return["servientrega"]["Guia"] = $Serv_r->messages[0]->PIN_EFECTY;
        $return["servientrega"]["Sede"] = $Serv_r->messages[0]->CDS;
        $return["servientrega"]["Precio"] = $r->quantity * 50000;
        $return["servientrega"]["Direccion"] = $Serv_r->messages[0]->DIRECCION;
        $return["servientrega"]["Descripcion"] = $Serv_r->messages[0]->description;
        $return["servientrega"]["Fecha_Limite"] =  $random = Carbon::parse($Serv_r->messages[0]->FECHA_VIGENCIA);
        $return["servientrega"]["Fecha_Inicio"] =  $random = Carbon::parse($Serv_r->messages[0]->FECHA_INICIAL);



        $data["users_id"] = auth()->user()->id;
        $data["mobile"] = PhoneNumber::make($data["mobile"])->ofCountry('CO');


        $data = request()->only('name', 'lastname', 'mobile','adress', 'cities_id', 'document');
        $data = array_filter($data);


        $customer->update($data);
        $customer->City;
        $return["customer"]= $customer;
        return response()->json($return);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        $item = $customer->delete();

        $response["deleted"] = $item;
        if ($item) {
            $response["status"] = 200;
        } else {
            $response["status"] = 401;
        }

        return response()->json($response);
    }
}
