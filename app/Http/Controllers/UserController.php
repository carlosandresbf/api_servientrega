<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserCollection;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use PhpParser\ErrorHandler\Collecting;
use Propaganistas\LaravelPhone\PhoneNumber;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->input("filterColumn");
        $filterValue = $request->input("filterValue");
        $pageSize = $request->input("pageSize");
        $sortField = $request->input("sortField");
        $sortOrder = ($request->input("sortOrder") == "asc") ? "asc" : "desc";

        if (empty($sortField)) {
            $sortField = "name";
        }

        $item = User::orderBy($sortField, $sortOrder);

        if (empty($filter) || $filter == "*") {

            $item->filterValue($filterValue);

        } else {

            $item->where($filter, 'like', "%$filterValue%");

        }

        if (empty($pageSize)) {
            $pageSize = 10;
        }

        return new UserCollection($item->paginate($pageSize));

    }

    public function store(Request $request)
    {
        $data = $request->validate([
            "name" => "required|max:255",
            "lastname" => "required|max:255",
            "password" => "required|confirmed",
            "phone" => "required|max:100|min:10|phone:CO",
            "document" => "required|unique:users|max:100",
            #"pic" => "required|file|max:5120|mimes:jpg,gif,jpeg,png"
        ]);
        /* if (!empty($request->file("pic"))) {
        $new_name = time() . '.' . $request->pic->extension();
        $path = $request->pi$data["phone"]c->storeAs('images/profile_pic', $new_name, 'public');
        $data["pic"] = $path;
        } */
        $data["password"] = bcrypt($data["password"]);
        $data["phone"] = PhoneNumber::make($data["phone"])->ofCountry('CO');
        $InsertId = User::insertGetId($data);
        $inserted = User::where("id", $InsertId)->get();

        return response()->json($inserted);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $_user = current($user->get()->toArray());
        #$_user['pic'] = asset('/images/profile_pic/' . $_user['pic']);
        return response()->json($_user);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {

        $data = $request->validate([
            "name" => "nullable|max:255",
            "lastname" => "nullable|max:255",
            "password" => "nullable|confirmed",
            "phone" => "nullable|max:100|min:10|phone:CO",
            "document" => "nullable|max:100|unique:users,document,". $user->id,
            #"pic" => "nullable|file|max:5120|mimes:jpg,gif,jpeg,png"
        ]);

        $data["phone"] = PhoneNumber::make($data["phone"])->ofCountry('CO');
        $user->update($data);

        $data = request()->only('name', 'lastname', 'password', 'phone', 'document');
        $data = array_filter($data);

        if ($request->post('password') == '') {
            unset($data['password']);
         }else{
            $data['password']==bcrypt($data['password']);
         }

        /* if (!empty($request->file("pic"))) {
            $new_name = time() . '.' . $request->pic->extension();
            $path = $request->pic->storeAs('images/profile_pic', $new_name, 'public');
            $data["pic"] = $path;
        } */


        return response()->json($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $item = $user->delete();

        $response["deleted"] = $item;
        if ($item) {
            $response["status"] = 200;
        } else {
            $response["status"] = 401;
        }

        return response()->json($response);
    }


}
