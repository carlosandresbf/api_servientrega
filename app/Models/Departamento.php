<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Departamento extends Model
{

    use SoftDeletes;
    protected $table = "departamentos";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $dates = ['deleted_at'];
    protected $fillable = ['name'];

    public function Municipios()
    {
        return $this->hasMany('App\Models\Municipio', 'departamento_id', 'id');
    }
}
