<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    use SoftDeletes;
    protected $table = "users";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $dates = ['deleted_at'];
    protected $fillable = ['name', 'lastname',  'password','password_confirmation', 'phone', 'document'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     *//*
    protected $hidden = [
        'UserPassword'
    ];
*/
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getAuthPassword()
    {
        return $this->password;
    }


    /* MODEL RELATIONS */

    /* Atomatic User Foreign Key */
    public function create()
    {
          Auth::user()->Assignments()->create($article);
    }



    public function Assignments()
    {
        return $this->hasMany('App\Models\Assignment', 'users_id', 'id');

    }

    public function Customers()
    {
        return $this->hasMany('App\Models\Customers', 'users_id', 'id');

    }

    public function SessionInfos()
    {
        return $this->hasMany('App\Models\SessionInfo', 'users_id', 'id');

    }

    public function Redemptions()
    {
        return $this->hasMany('App\Models\Redemption', 'users_id', 'id');

    }




    /*  CUSTOM SCOPES */


    public function scopefilterValue($query, $param)
    {
        $query->orwhere($this->table. ".name", 'like', "%$param%");
        $query->orWhere($this->table. ".lastName", 'like', "%$param%");
        $query->orWhere($this->table. ".phone", 'like', "%$param%");
        $query->orWhere($this->table. ".document", 'like', "%$param%");
    }

    public static function scopethisUser(){
        $$query->user_id = auth()->user()->id;
    }


}
