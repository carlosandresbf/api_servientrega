<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Auth
Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::get('logout', 'AuthController@logout');
});

Route::middleware(['api', 'jwt'])->group(function () {
    Route::group([
        'prefix' => 'auth'
    ], function () {
        Route::get('me', 'AuthController@me');
        Route::get('refresh', 'AuthController@refresh');
        Route::get('invalidate', 'AuthController@invalidate');
    });

//Admin
    Route::group([
        'prefix' => 'admin'
    ], function () {
        Route::get('city', 'CityController@index');
        Route::get('store', 'StoreController@index');
        Route::post('store', 'StoreController@store');
        Route::apiResource('user', 'UserController');
        Route::apiResource('customer', 'CustomerController');
        Route::apiResource('assignment', 'AssignmentController');
        Route::apiResource('redemption', 'RedemptionController');
        Route::post('redemption/validate', 'RedemptionController@valid');
        Route::post('redemption/terms/{id}', 'RedemptionController@uploadTerms');
        Route::post('redemption/invoice/photo/{id}', 'RedemptionController@uploadInvoicePhoto');
        Route::post('redemption/{id}', 'RedemptionController@uploadTerms');
        Route::get('session/active', 'SessionInfoController@index');
        Route::get('session/set/role/{id}', 'SessionInfoController@store');
        Route::apiResource('stock', 'StockController');
        Route::post('servientrega', 'StoreController@maps');
    });
});

